/*  (c) Copyright:  2017  Patrn ESS, Confidential Data
 *
 *  Workfile:           mail_client.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi mail handler
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include <common.h>
#include "../config.h"
#include "../globals.h"
#include "encode.h"
#include "crypto.h"
#include "mail_func.h"
#include "mail_client.h"

//#define USE_PRINTF
#include <printf.h>

//
// Local prototypes
//
static bool   client_Init                    (void);
static char  *client_EncryptPassword         (char *);

// Client tests : Dump Configfile ===================================== Start ======
#ifdef  FEATURE_ENABLE_CRYPTO_TEST
static void   client_DumpData               (char *, u_int8 *, int);
#define CLIENT_DUMP_DATA(x,y,z)             client_DumpData(x,y,z)
#else
#define CLIENT_DUMP_DATA(x,y,z)
#endif  //FEATURE_ENABLE_CRYPTO_TEST
// Client tests : Dump Configfile ===================================== End ========

//
// The structures for communicating with the world
//
static CINFO *pstNetMail[NUM_CLIENT_COMMS] =
{
   NULL,
   NULL,
   NULL
};
//
static CRYPTH tHandle = CRYPT_ILLEGAL_HANDLE;

/* ======   Local Functions separator ===========================================
void ___main_functions(){}
==============================================================================*/

/**
 **  Name:         CLIENT_Init
 **  Description:  Init the client email module
 **  Arguments:    
 **
 **  Returns:      TRUE if data is OKee
 **/
bool CLIENT_Init(void)
{
   bool  fCc;

#if defined(FEATURE_ENABLE_CRYPTO_TEST)
   const char *pcPlaintext = "[*** This is NOT a very long plaintext ***]";
   int         iPlainLength;
   u_int8     *pubBuffer;
   u_int32     ulSize;
#endif //FEATURE_ENABLE_CRYPTO_TEST

   if( (fCc = client_Init()) )
   {
      tHandle = Crypto_Open(CRYPT_MODE_INTERNAL_KEY, CRYPT_LEVEL_FILE, 64);
      if(tHandle == CRYPT_ILLEGAL_HANDLE)
      {
         LOG_Report(0, "EML", "CLIENT_Init():ERROR: Crypto Open");
         fCc = FALSE;
      }
      else
      {
         #if defined(FEATURE_ENABLE_CRYPTO_TEST)
         //
         // Allocate sufficient room
         // Should be : Crypto_GetEncryptSize(ulSize);
         //
         iPlainLength = GEN_STRLEN(pcPlaintext)+1;
         pubBuffer    = safemalloc(4*iPlainLength);
         //
         GEN_STRCPY((char *)pubBuffer, pcPlaintext);
         //
         LOG_printf("CLIENT_Init:Plaintext=%s" CRLF, pubBuffer);
         CLIENT_DUMP_DATA("Plain", pubBuffer, iPlainLength);
         ulSize = Crypto_Encrypt(tHandle, pubBuffer, iPlainLength);
         CLIENT_DUMP_DATA("Crypt", pubBuffer, ulSize);
         Crypto_Decrypt(tHandle, pubBuffer, ulSize);
         CLIENT_DUMP_DATA("Plain", pubBuffer, iPlainLength);
         LOG_printf("CLIENT_Init:Plaintext=%s" CRLF, pubBuffer);
         safefree(pubBuffer);
         #endif //FEATURE_ENABLE_CRYPTO_TEST
      }
   }
   else
   {
      LOG_Report(0, "EML", "CLIENT_Init():ERROR !");
   }
   return(fCc);
}

/**
 **  Name:         CLIENT_Exit
 **  Description:  Exit the client module
 **  Arguments:    
 **
 **  Returns:      
 **/
void CLIENT_Exit(void)
{
   //
   // Free struct for incoming mail (POP3)
   //                 outgoing mail (SMTP)
   //                 router        (telnet)
   //
   if(pstNetMail[CL_COMM_MAIL_IN])  safefree(pstNetMail[CL_COMM_MAIL_IN]);
   if(pstNetMail[CL_COMM_MAIL_OUT]) safefree(pstNetMail[CL_COMM_MAIL_OUT]);
   if(pstNetMail[CL_COMM_ROUTER])   safefree(pstNetMail[CL_COMM_ROUTER]);
   //
   Crypto_Close(tHandle);
}

// 
//  Name:         CLIENT_GetServer
//  Description:  
//  Arguments:    CL_xxx type
//                  
// 
//  Returns:      Ptr to Server name
// 
char *CLIENT_GetServer(CTYPE tType)
{
   char *pcServer=NULL;

   if(tType < NUM_CLIENT_COMMS)
   {
      pcServer = pstNetMail[tType]->pcServer;
      PRINTF1("CLIENT_GetServer=%s" CRLF, pcServer);
   }
   return(pcServer);
}

// 
//  Name:         CLIENT_GetPort
//  Description:  
//  Arguments:    CL_xxx type
//                  
// 
//  Returns:      Port nr (default = 110)
// 
int CLIENT_GetPort(CTYPE tType)
{
   int   iPort=110;

   if(tType < NUM_CLIENT_COMMS)
   {
      iPort = pstNetMail[tType]->iPort;
   }
   PRINTF1("CLIENT_GetPort=%d" CRLF, iPort);
   return(iPort);
}

// 
//  Name:         CLIENT_GetUser
//  Description:  
//  Arguments:    CL_xxx type
// 
//  Returns:      Ptr to user name
// 
char *CLIENT_GetUser(CTYPE tType)
{
   char *pcUser=NULL;

   if(tType<NUM_CLIENT_COMMS)
   {
      pcUser = pstNetMail[tType]->pcUser;
   }
   return(pcUser);
}

// 
// Name:          CLIENT_GetPassword
// Description:  
// Arguments:     CL_xxx type, buffer, buffer size
// 
// Returns:       TRUE if OKee
// Note:          The password will be in plaintext in the buffer !
//
// 
bool CLIENT_GetPassword(CTYPE tType, char *pcBuffer, int iSize)
{
   bool     fCc=FALSE;
   CINFO   *pstMail;
   char    *pcCrypt, *pcTemp;
   int      iLen;

   PRINTF1("CLIENT_GetPassword:L=%d" CRLF, iSize);
   if(tType < NUM_CLIENT_COMMS)
   {
      GEN_MEMSET(pcBuffer, 0x00, iSize);
      pstMail = pstNetMail[tType];
      //
      // Get the ASCIIz password array and size
      //
      pcCrypt = pstMail->pcPass;
      iLen    = GEN_STRLEN(pcCrypt);
      PRINTF2("CLIENT_GetPassword:<%s>[%d]" CRLF, pcCrypt, iLen);
      if(iLen)
      {
         pcTemp = safemalloc(iLen);
         //
         // Decode back to binary form
         //
         MIME_Ascii2Bin(pcTemp, pcCrypt, iLen);
         //
         // Decrypt back to ASCIIz
         //
         Crypto_Decrypt(tHandle, (u_int8 *)pcTemp, iLen);
         //
         // Copy to the user, if it fits
         //
         if(iSize >= GEN_STRLEN(pcTemp))
         {
            GEN_MEMCPY(pcBuffer, pcTemp, iLen);
            fCc = TRUE;
         }
         else
         {
            PRINTF2("CLIENT_GetPassword:ERROR:BufL=%d<ActL=%d" CRLF, iSize, iLen);
         }
         safefree(pcTemp);
      }
      else
      {
         PRINTF("CLIENT_GetPassword:No password" CRLF);
      }
   }
   return(fCc);
}

// 
// Name:          CLIENT_EncryptPassword
// Description:  
// Arguments:     CL_xxx type
// 
// Returns:       TRUE if OKee
// Note:          The password is for a moment in the clear at the global
//                area: encrypt, mime and store back
// 
bool CLIENT_EncryptPassword(CTYPE tType)
{
   bool     fCc=FALSE;
   CINFO   *pstMail;
   char    *pcPass;

   if(tType < NUM_CLIENT_COMMS)
   {
      pstMail = pstNetMail[tType];
      if(pstMail)
      {
         pcPass  = client_EncryptPassword(pstMail->pcPass);
         GEN_MEMSET( pstMail->pcPass, 0x00,   MAX_PASS_LEN);
         GEN_STRNCPY(pstMail->pcPass, pcPass, MAX_PASS_LEN);
         safefree(pcPass);
         fCc = TRUE;
      }
   }
   return(fCc);
}

// Name:          CLIENT_ParameterChanged
// Description:  
// Arguments:     Parameter Enum
// 
// Returns:       TRUE if OKee
// Note:          Enum depicts what mail parameter has changed:
//                   PAR_POP3_USER
//                   PAR_POP3_PASS
//                   PAR_POP3_SRVR
//                   PAR_POP3_PORT
//                   PAR_POP3_SSL
//
//                   PAR_SMTP_USER
//                   PAR_SMTP_PASS
//                   PAR_SMTP_SRVR
//                   PAR_SMTP_PORT
//                   PAR_SMTP_SSL
// 
bool CLIENT_ParameterChanged(int iId)
{
   bool     fCc=FALSE;
   int      iTemp;
   char    *pcVar1;
   char    *pcVar2;
   CINFO   *pstMail;

   PRINTF1("CLIENT_ParameterChanged():Idx=%d" CRLF, iId);
   //
   switch(iId)
   {
      default:
         // These do not need action
         fCc = TRUE;
         break;

      case PAR_POP3_PORT:
         pstMail = pstNetMail[CL_COMM_MAIL_IN];
         if(pstMail)
         {
            pcVar1 = GLOBAL_GetParameter(PAR_POP3_PORT);
            iTemp = atoi(pcVar1);
            if(iTemp && (iTemp < 0x7fff) )
            {
               pstMail->iPort = iTemp;
               fCc = TRUE;
            }
         }
         break;

      case PAR_SMTP_PORT:
         pstMail = pstNetMail[CL_COMM_MAIL_OUT];
         if(pstMail)
         {
            pcVar1 = GLOBAL_GetParameter(PAR_SMTP_PORT);
            iTemp = atoi(pcVar1);
            if(iTemp && (iTemp < 0x7fff) )
            {
               pstMail->iPort = iTemp;
               fCc = TRUE;
            }
         }
         break;

      case PAR_POP3_USER:
         pcVar1 = GLOBAL_GetParameter(PAR_SMTP_USER);
         if(GEN_STRLEN(pcVar1) == 0)
         {
            // Duplicate user if needed
            pcVar2 = GLOBAL_GetParameter(PAR_POP3_USER);
            GEN_STRNCPY(pcVar1, pcVar2, MAX_MAIL_LEN);
         }
         break;

      case PAR_SMTP_USER:
         pcVar1 = GLOBAL_GetParameter(PAR_POP3_USER);
         if(GEN_STRLEN(pcVar1) == 0)
         {
            // Duplicate user if needed
            pcVar2 = GLOBAL_GetParameter(PAR_SMTP_USER);
            GEN_STRNCPY(pcVar1, pcVar2, MAX_MAIL_LEN);
         }
         break;

      case PAR_POP3_PASS:
         fCc = CLIENT_EncryptPassword(CL_COMM_MAIL_IN);
         if(fCc)
         {
            pcVar1 = GLOBAL_GetParameter(PAR_SMTP_PASS);
            if(GEN_STRLEN(pcVar1) == 0)
            {
               // Duplicate passwords if needed
               pcVar2 = GLOBAL_GetParameter(PAR_POP3_PASS);
               GEN_STRNCPY(pcVar1, pcVar2, MAX_PASS_LEN);
            }
         }
         else 
         {
            pcVar1 = GLOBAL_GetParameter(PAR_POP3_PASS);
            GEN_MEMSET(pcVar1, 0x00, MAX_PASS_LEN);
            PRINTF("CLIENT_ParameterChanged():ERROR:Encrypt POP3 Psw !" CRLF);
         }
         break;

      case PAR_SMTP_PASS:
         fCc = CLIENT_EncryptPassword(CL_COMM_MAIL_OUT);
         if(fCc)
         {
            pcVar1 = GLOBAL_GetParameter(PAR_POP3_PASS);
            if(GEN_STRLEN(pcVar1) == 0)
            {
               // Duplicate passwords if needed
               pcVar2 = GLOBAL_GetParameter(PAR_SMTP_PASS);
               GEN_STRNCPY(pcVar1, pcVar2, MAX_PASS_LEN);
            }
         }
         else 
         {
            pcVar1 = GLOBAL_GetParameter(PAR_SMTP_PASS);
            GEN_MEMSET(pcVar1, 0x00, MAX_PASS_LEN);
            PRINTF("CLIENT_ParameterChanged():ERROR:Encrypt SMTP Psw !" CRLF);
         }
         break;
   }
   return(fCc);
}

/**
 **  Name:         CLIENT_Setup
 **  Description:  Setup the client email module again after changes to the settings
 **  Arguments:    IN/OUT going mail
 **
 **  Returns:      ATYPE MAIL_SETUP_xxx
 **/
ATYPE CLIENT_Setup(CTYPE tType)
{
   ATYPE tAuth=MAIL_SETUP_NO_AUTH;

   PRINTF("CLIENT_Setup()" CRLF);
   //
   if(tType < NUM_CLIENT_COMMS)
   {
      client_Init();
      tAuth = pstNetMail[tType]->tAuth;
   }
   return(tAuth);
}

/* ======   Local Functions separator ===========================================
void ___local_functions(){}
==============================================================================*/

/**
 **  Name:         client_Init
 **  Description:  Init the client comm structures
 **  Arguments:    void
 **
 **  Returns:      TRUE if OKee
 **/
static bool client_Init(void)
{
   bool     fCc=TRUE;
   CINFO   *pstMail;

   PRINTF("client_Init()" CRLF);
   //
   // Setup struct for incoming mail (POP3)
   //
   if( (pstMail = pstNetMail[CL_COMM_MAIL_IN]) == NULL)
   {
      pstMail = safemalloc(sizeof(CINFO));
      pstNetMail[CL_COMM_MAIL_IN] = pstMail;
      PRINTF("client_Init(): POP3 Created." CRLF);
   }
   pstMail->pcServer = GLOBAL_GetParameter(PAR_POP3_SRVR);
   pstMail->pcUser   = GLOBAL_GetParameter(PAR_POP3_USER);
   pstMail->pcPass   = GLOBAL_GetParameter(PAR_POP3_PASS);
   pstMail->iPort    = atoi(GLOBAL_GetParameter(PAR_POP3_PORT));
   GEN_TrimRight(pstMail->pcUser);
   if( (GEN_STRLEN(pstMail->pcUser) == 0) || (GEN_STRLEN(pstMail->pcPass) == 0) )
   {
      //
      // NO Username/Pass authentication possible: do NOT attempt to login
      //
      pstMail->tAuth = MAIL_SETUP_NO_AUTH;
      PRINTF2("client_Init(): POP3 cannot authenticate with User=[%s], Pass=[%s]" CRLF, pstMail->pcUser, pstMail->pcPass);
   }
   else 
   {
      //
      // Username/Pass authentication possible: assume secure SSL/TSL login if we have a POP3 server
      //
      if(GEN_STRLEN(pstMail->pcServer) == 0) 
      {
         pstMail->tAuth = MAIL_SETUP_NO_AUTH;
         PRINTF("client_Init(): POP3 cannot authenticate without server !" CRLF);
         fCc = FALSE;
      }
      else
      {
         pstMail->tAuth = MAIL_SETUP_SSL_AUTH;
         PRINTF2("client_Init(): POP3 Server=%s, User=%s" CRLF, pstMail->pcServer, pstMail->pcUser);
      }
   }
   //
   // Setup struct for outgoing mail (SMTP)
   //
   if( (pstMail = pstNetMail[CL_COMM_MAIL_OUT]) == NULL)
   {
      pstMail = safemalloc(sizeof(CINFO));
      pstNetMail[CL_COMM_MAIL_OUT] = pstMail;
      PRINTF("client_Init(): SMTP Created." CRLF);
   }
   pstMail->pcServer = GLOBAL_GetParameter(PAR_SMTP_SRVR);
   pstMail->pcUser   = GLOBAL_GetParameter(PAR_SMTP_USER);
   pstMail->pcPass   = GLOBAL_GetParameter(PAR_SMTP_PASS);
   pstMail->iPort    = atoi(GLOBAL_GetParameter(PAR_SMTP_PORT));
   //
   GEN_TrimRight(pstMail->pcUser);
   if( (GEN_STRLEN(pstMail->pcUser) == 0) || (GEN_STRLEN(pstMail->pcPass) == 0) )
   {
      //
      // NO Username/Pass authentication possible: try to send mail anonymous (plaintext !)
      //
      PRINTF( "client_Init(): SMTP NO User:send messages in plaintext !" CRLF);
      pstMail->tAuth = MAIL_SETUP_NO_AUTH;
   }
   else 
   {
      if(GEN_STRLEN(pstMail->pcServer) == 0) 
      {
         PRINTF("client_Init(): SMTP cannot send without server !" CRLF);
         fCc = FALSE;
      }
      else
      {
         //
         // Username/Pass authentication possible: assume secure SSL/TSL login
         //
         pstMail->tAuth = MAIL_SETUP_SSL_AUTH;
         PRINTF2("client_Init(): SMTP Server=%s, User=%s" CRLF, pstMail->pcServer, pstMail->pcUser);
      }
   }
   return(fCc);
}

/**
 **  Name:        client_EncryptPassword
 **  Description: Encrypt the password
 **  Arguments:   pcPlainPassword
 **
 **  Returns:     New buffer ptr
 **  Note:        The returned buffer needs to be free'ed by the caller
 **/
static char *client_EncryptPassword(char *pcPlainText)
{
   int      iCryptSize, iPassSize, iPlainSize;
   char    *pcPass;
   u_int8  *pubCryptText;

   iPlainSize = GEN_STRLEN(pcPlainText) + 1;
   PRINTF2("client_EncryptPassword:%s[%d]" CRLF, pcPlainText, iPlainSize);
   //
   // Find out the necessary cryptlength and encrypt the plaintext
   //
   iCryptSize = Crypto_GetEncryptSize(iPlainSize);
   PRINTF2("client_EncryptPassword:plainsize=%d, cryptsize=%d" CRLF, iPlainSize, iCryptSize);
   pubCryptText = safemalloc(iCryptSize);
   //
   // Encrypt and erase the plaintext
   //
   GEN_MEMSET(pubCryptText, 0x00,        iCryptSize);
   GEN_MEMCPY(pubCryptText, pcPlainText, iPlainSize);
   GEN_MEMSET(pcPlainText,  0x00,        iPlainSize);
   //
   iPassSize = (int) Crypto_Encrypt(tHandle, pubCryptText, iCryptSize);
   PRINTF2("client_EncryptPassword:cryptsize=%d, act cryptsize=%d" CRLF, iCryptSize, iPassSize);
   //
   // The encrypted password is expanded to ASCIIz. 
   // The original passwordlength then becomes GEN_STRLEN(Asciiz_passw)
   //
   // The real crypto length := (iCryptSize*2)+1;
   //
   iPassSize  = (iCryptSize * 4);
   pcPass     = safemalloc(iPassSize);
   GEN_MEMSET(pcPass, 0x00, iPassSize);
   MIME_Bin2Ascii(pcPass, (char *)pubCryptText, iCryptSize);
   safefree(pubCryptText);
   return(pcPass);
}


#if defined(FEATURE_ENABLE_CRYPTO_TEST)
/**
 **  Name:         client_DumpData
 **  Description:  
 **  Arguments:    Headertext, pcRec, size
 **
 **  Returns:      size of result
 **/
static void client_DumpData(char *pcHeader, u_int8 *pubBuffer, int iLength)
{
   int i;
  
   LOG_printf("%s (%d)" CRLF, pcHeader, iLength);

   for(i=0; i<iLength; i++)
   {
      GEN_Printf("%02X", pubBuffer[i]);
      if(i%8 == 7) GEN_Printf("\n");
      else         GEN_Printf(" ");
   }
   GEN_Printf("\n");
}

#endif  //FEATURE_ENABLE_CRYPTO_TEST
