/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           mail_pop3.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi pop3 mail handler
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <libgen.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include <common.h>
#include "../config.h"
#include "../globals.h"
#include "mail_func.h"
#include "mail_ssl.h"
#include "mail_pop3.h"

//#define USE_PRINTF
#include <printf.h>

//
// APOP uses MD5 digest for secure authentication to prevent Username/Password transmitted
// in plaintext.
// Ziggo does not support APOP authentication.
// #define FEATURE_POP3_APOP
//

//
// Static functions
//
static int     pop3_Connect              (MAILO *, char *, int);
static bool    pop3_DataAvailable        (MAILO *);
static bool    pop3_DeleteMessage        (MAILO *, int);
static bool    pop3_Login                (MAILO *);
static int     pop3_Gets                 (MAILO *, char *, int);
static bool    pop3_GetMessage           (MAILO *, char *, int *);
static bool    pop3_PutMessage           (MAILO *, char *, char *);
static bool    pop3_DoMessage            (MAILO *, char *, char *);
static bool    pop3_RetrieveHeader       (MAILO *, int, int, MAILRCB, void *);
static bool    pop3_RetrieveMessage      (MAILO *, int, MAILRCB, void *);
static bool    pop3_RetrieveNrOfMessages (MAILO *, int *);
static int     pop3_Read                 (MAILO *, char *, int);
static int     pop3_Write                (MAILO *, char *, int);
static bool    pop3_LogOff               (MAILO *);
static void    pop3_Close                (MAILO *);

//
// Function:   POP3_LogIn
// Purpose:    Login the POP3 mail server
// Parms:      MailOpt^
//
// Returns:    TRUE if OKee
// Note:       Use APOP for MD5 secure authentication:
//             Example:
//             S: +OK POP3 server ready <1896.697170952@dbc.mtview.ca.us>
//             C: APOP mrose c4c9334bac560ecc979e58001b3e22fb
//             S: +OK maildrop has 1 message (369 octets)
//
bool POP3_LogIn(MAILO *pstOpt)
{
   bool  fCc=FALSE;
   char *pcIp;

   PRINTF2("POP3_LogIn():<%s:%d>" CRLF, pstOpt->pcHost, pstOpt->iPort);
   //
   if( (pcIp = NET_ResolveHostName(pstOpt->pcHost)) == NULL) 
   {
      LOG_Report(errno, "POP", "POP3_LogIn():Resolve %s failed", pstOpt->pcHost);
      return(fCc);
   }
   //
   // Connect to server
   //
   if(pop3_Connect(pstOpt, pcIp, pstOpt->iPort))
   {
      fCc = pop3_Login(pstOpt);
   }
   else
   {
      LOG_Report(errno, "POP", "ERROR:Connect to %s <%s:%d>",  pstOpt->pcHost, pcIp, pstOpt->iPort);
      PRINTF3("POP3_LogIn():ERROR:Connect to %s <%s:%d>" CRLF, pstOpt->pcHost, pcIp, pstOpt->iPort);
   }
   PRINTF3("POP3_LogIn():%s <%s:%d> OKee" CRLF, pstOpt->pcHost, pcIp, pstOpt->iPort);
   safefree(pcIp);
   return(fCc);
}

//
//  Function:   POP3_RetrieveAccount
//  Purpose:    Get account info from the POP3 mail server
//  Parms:      MailOpt^
//
//  Returns:    For now, just the number of emails on the server
//
int POP3_RetrieveAccount(MAILO *pstOpt)
{
   int   iNr;

   if(!pop3_RetrieveNrOfMessages(pstOpt, &iNr) ) iNr = -1;
   
   return(iNr);
}

//
//  Function:   POP3_RetrieveHeader
//  Purpose:    Get a mail header from the POP3 mail server
//  Parms:      MailOpt^, MailNr, Lines, Callback, Buffer ptr
//
//  Returns:    Retrieve a mail header plus some lines from the body
//
bool POP3_RetrieveHeader(MAILO *pstOpt, int iIdx, int iLines, MAILRCB pfnStore, void *pvData)
{
   return( pop3_RetrieveHeader(pstOpt, iIdx, iLines, pfnStore, pvData));
}

//
//  Function:   POP3_DeleteMessage
//  Purpose:    Delete mail from the POP3 mail server
//  Parms:      MailOpt^, Mail Nr
//
//  Returns:    TRUE if OKee
//
bool POP3_DeleteMessage(MAILO *pstOpt, int iNr)
{
   return(pop3_DeleteMessage(pstOpt, iNr));
}

//
//  Function:   POP3_ReceiveMail
//  Purpose:    Read mail from the POP3 mail server
//  Parms:      MailOpt^, Mail Nr, Callback, data
//
//  Returns:    TRUE if OKee
//
bool POP3_ReceiveMail(MAILO *pstOpt, int iNr, MAILRCB pfnSave, void *pvData)
{
   PRINTF("POP3_ReceiveMail()" CRLF);
   return( pop3_RetrieveMessage(pstOpt, iNr, pfnSave, pvData) );
}

//
//  Function:   POP3_LogOff
//  Purpose:    Logout the POP3 mailaccount
//  Parms:      MailOpt^
//
//  Returns:    TRUE if OKee
//
bool POP3_LogOff(MAILO *pstOpt)
{
   PRINTF("POP3_LogOff()" CRLF);
   return(pop3_LogOff(pstOpt));
}


/* ======   Local Functions separator ===========================================
___POP3_MAIL_FUNCTIONS(){}
==============================================================================*/

//  
//  Name:         pop3_DataAvailable
//  Description:  Check if data is available from the pop3 port
//  Arguments:    MAILO struct
//  
//  Returns:      TRUE if socket has data
//  
static bool pop3_DataAvailable(MAILO *pstOpt)
{
   fd_set         rfds;
   struct timeval tv;
   int            iCc, iSocket;
   bool           fResult = FALSE;

   iSocket = pstOpt->iSocket;
   // 
   FD_ZERO(&rfds);
   FD_SET(iSocket, &rfds);
   tv.tv_sec  = POP3_TIMEOUT;
   tv.tv_usec = 0;
   //
   iCc = select(iSocket+1, &rfds, NULL, NULL, &tv);
   // 
   if(iCc) fResult = TRUE;
   //
   return(fResult);
}

//  
//  Name:         pop3_Gets
//  Description:  Read the data from the mail server
//  Arguments:    Socket, Buffer, buffersize
//  
//  Returns:      Number of bytes not used in the buffer
//                
//   Responses to certain commands are multi-line.  In these cases, which
//   are clearly indicated below, after sending the first line of the
//   response and a CRLF, any additional lines are sent, each terminated
//   by a CRLF pair.  When all lines of the response have been sent, a
//   final line is sent, consisting of a termination octet (decimal code
//   046, ".") and a CRLF pair.  If any line of the multi-line response
//   begins with the termination octet, the line is "byte-stuffed" by
//   pre-pending the termination octet to that line of the response.
//   Hence a multi-line response is terminated with the five octets
//   " CRLF.CRLF".  When examining a multi-line response, the client checks
//   to see if the line begins with the termination octet.  If so and if
//   octets other than CRLF follow, the first octet of the line (the
//   termination octet) is stripped away.  If so and if CRLF immediately
//   follows the termination character, then the response from the POP
//   server is ended and the line containing ".CRLF" is not considered
//   part of the multi-line response.
//  
static int pop3_Gets(MAILO *pstOpt, char *pcBuffer, int iSize)
{
   static int iMailTermIndex = 0;
   const char cMailTerm[]    = {"\r\n.\r\n"};

   char  cChar;
   bool  fMore      = TRUE;
   bool  fFirstChar = TRUE;
   bool  fPeriod    = FALSE;

   PRINTF("pop3_Gets()" CRLF);
   //
   if(!pstOpt->fSsl)
   {
      while( fMore && pop3_DataAvailable(pstOpt) )
      {
         pop3_Read(pstOpt, &cChar, 1);
         switch(cChar)
         {
            case '.':
               if(fFirstChar) 
               {
                  if(fPeriod == FALSE)
                  {
                     *pcBuffer++ = cChar;
                     iSize--;
                     fPeriod = TRUE;
                  }
                  else
                  {
                     fFirstChar = FALSE;
                  }
               }
               else
               {
                  *pcBuffer++ = cChar;
                  iSize--;
               }
               break;

            case '\n':
               *pcBuffer++ = cChar;
               iSize--;
               fMore = FALSE;
               break;

            default:
               fFirstChar = FALSE;
               *pcBuffer++ = cChar;
               iSize--;
               break;
         }
         if(iSize == 0) fMore = FALSE;
         //
         // Keep checking if the server reports End-of-mail "CrLf.CrLf"
         //
         if(cChar == cMailTerm[iMailTermIndex]) 
         {
            //PRINTF1("pop3_Gets:%d" CRLF, iMailTermIndex);
            iMailTermIndex++;
         }
         else 
         {
            iMailTermIndex = 0;
            if(cChar == cMailTerm[iMailTermIndex]) 
            {
               //PRINTF1("pop3_Gets:%d" CRLF, iMailTermIndex);
               iMailTermIndex++;
            }
         }
         //
         if(iMailTermIndex > 4)
         {
            // end of mail
            //PRINTF1("pop3_Gets:End of mail sequence = %ld" CRLF, iSize);
            iSize += 3;       // ignore last sequence
            fMore     = FALSE;
         }
      }
   }
   PRINTF(CRLF);
   PRINTF1("pop3_Gets:End of mail = %ld" CRLF, iSize);
   return(iSize);
}

//  
// Name:          pop3_GetMessage
// Description:   Read a message from the POP3 mail server
// Arguments:     MAILO struct, Buffer, Size ptr
//                
// Returns:       TRUE if OKee
// Note:          pbBuffer = NULL : We will allocate and free a temp buffer and
//                                  just pass the result back (TRUE on "+OK" reply)
//                Else, the caller can supply his own Buffer/Size.
//  
static bool pop3_GetMessage(MAILO *pstOpt, char *pcBuffer, int *piSize)
{   
   bool  fCc=FALSE;
   int   iResult, iSize=0;
   char *pcTempBuffer=NULL;
   
   PRINTF("pop3_GetMessage" CRLF);
   //
   if(!pstOpt->fSsl)
   {
      if(pcBuffer == NULL)
      {
         pcTempBuffer = safemalloc(MAIL_BUFLEN);
         pcBuffer     = pcTempBuffer;
         iSize        = MAIL_BUFLEN;
      }
      else
      {
         if(piSize) iSize = *piSize;
      }
      iResult = pop3_Gets(pstOpt, pcBuffer, iSize);
      if(iResult != iSize)
      {
         switch(pcBuffer[0])
         {
            case '+': 
               fCc = TRUE;
               break;

            case '-': 
               PRINTF1("pop3_GetMessage:nOK:%s" CRLF, pcBuffer);
               fCc = FALSE;
               break;

            default:  
               PRINTF("pop3_GetMessage:Error" CRLF);
               fCc = FALSE;
               break;
         }
         iSize = iResult;
      }
      else
      {
         PRINTF("pop3_GetMessage:pending" CRLF);
         fCc = FALSE;
      }
      if(pcTempBuffer) safefree(pcTempBuffer);  
      if(piSize)       *piSize = iSize;
   }
   return(fCc);
}

//  
//  Name:         pop3_PutMessage
//  Description:  Write a message to the POP3 mail server
//  Arguments:    MAILO struct, Message, data
//  
//  Returns:      TRUE if OKee
//  
static bool pop3_PutMessage(MAILO *pstOpt, char *pcMessage, char *pcData)
{  
   bool  fCc=FALSE;  
   char *pcBuffer;

   pcBuffer = safemalloc(MAIL_BUFLEN);
   //
   if (pcData) GEN_SPRINTF(pcBuffer, pcMessage, pcData);
   else        GEN_STRNCPY(pcBuffer, pcMessage, MAIL_BUFLEN);
   //
   //PRINTF1("pop3_PutMessage %s", pcBuffer);
   //
   if(pop3_Write(pstOpt, pcBuffer, GEN_STRLEN(pcBuffer)) >= 0)
   {
      fCc = TRUE;
   }
   else
   {
      PRINTF("pop3_PutMessage:Failed" CRLF);
   }
   //
   return(fCc);
}

//  
//  Name:         pop3_DoMessage
//  Description:  
//  Arguments:    MAILO struct, Message, Data
//  
//  Returns:      TRUE if OKee
//  
static bool pop3_DoMessage(MAILO *pstOpt, char *pcMessage, char *pcData)
{
   bool  fCc=FALSE;
   
   PRINTF("pop3_DoMessage" CRLF);
   //
   if(pop3_PutMessage(pstOpt, pcMessage, pcData))
   {
      fCc = pop3_GetMessage(pstOpt, NULL, NULL);
   }
   //
   return(fCc);
}

//  
// Name:          pop3_Login
// Description:   Log into the POP3 mail server
// Arguments:     MailOpt^
// 
// Returns:       TRUE if OKee
// Note:          Use APOP for MD5 secure authentication:
//                Example:
//                S: +OK POP3 server ready <1896.697170952@dbc.mtview.ca.us>
//                C: APOP mrose c4c9334bac560ecc979e58001b3e22fb
//                S: +OK maildrop has 1 message (369 octets)
//
//=========================================================================================
//                In this example, the shared secret is the string "tanstaaf".
//                Hence, the MD5 algorithm is applied to the string:
//                   "<1896.697170952@dbc.mtview.ca.us>tanstaaf"
//                which produces a digest value of
//                   "c4c9334bac560ecc979e58001b3e22fb"
//
//                TestCode (tested OKee)
//
//                const char *pcActDigest = "c4c9334bac560ecc979e58001b3e22fb";
//                const char *pcBuffer    = "<1896.697170952@dbc.mtview.ca.us>tanstaaf";
//    
//                if( SSL_RpiDigestMD5(pcCurDigest, 34, (char *)pcBuffer, GEN_STRLEN(pcBuffer)))
//                {
//                   PRINTF1("cmd_SendMail():MD5 Cur digest :<%s>" CRLF, pcCurDigest);
//                   PRINTF1("cmd_SendMail():MD5 Act digest :<%s>" CRLF, pcActDigest);
//                }
//                else PRINTF("cmd_SendMail():ERROR: MD5 digest!" CRLF);
//                return(FALSE);
//=========================================================================================
//
//  
static bool pop3_Login(MAILO *pstOpt)
{
   bool  fCc=FALSE;

#ifdef FEATURE_POP3_APOP

   int   iLength=BUF_MISC;
   char  pcDigest[34];
   char *pcBuffer=safemalloc(BUF_MISC);;

   // Ziggo replies:
   // 43-Do|23-11-2017|09:21:00 26313| [TRC] POP3_LogIn():<pop.ziggo.nl:110>
   // 44-Do|23-11-2017|09:21:05 26313| [TRC] pop3_GetMessage
   // 45-Do|23-11-2017|09:21:05 26313| [TRC] pop3_Gets:End of mail = 236
   // 46-Do|23-11-2017|09:21:05 26313| [TRC] SSL_RpiDigestMD5():Size=32
   // 47-Do|23-11-2017|09:21:05 26313| [TRC] pop3_Login():Connected :+OK Dovecot ready.
   // 48-Do|23-11-2017|09:21:05 26313| [TRC] pop3_Login():MD5-Digest:f9c62c212b5f07a50a5326681b50404b
   // 49-Do|23-11-2017|09:21:05 26313| [TRC] rpi_ReceiveMail():ERROR: Login failed !

   if(pop3_GetMessage(pstOpt, pcBuffer, &iLength))
   {
      //
      // Build APOP command: "APOP user md5_digest"
      //
      if( SSL_RpiDigestMD5(pcDigest, 34, pcBuffer, GEN_STRLEN(pcBuffer)))
      {
         PRINTF1("pop3_Login():Connected :%s" CRLF, pcBuffer);
         PRINTF1("pop3_Login():MD5-Digest:%s" CRLF, pcDigest);
         GEN_SNPRINTF(pcBuffer, BUF_MISC, "%s %s", pstOpt->pcUser, pcDigest);
         //
         if(pop3_DoMessage(pstOpt, "APOP %s\n", pcBuffer))
         {
            LOG_Report(0, "POP", "Authentication successfull");
            PRINTF("pop3_Login(): Authentication successfull" CRLF);
            fCc = TRUE;
         }
         else
         {
            LOG_Report(0, "POP", "ERROR:Authentication failed !");
            PRINTF("pop3_Login(): ERROR:Authentication failed !" CRLF);
         }
      }
      else
      {
         PRINTF1("pop3_Login():ERROR:MD5 digest:%s" CRLF, pcBuffer);
      }
   }
   else
   {
      LOG_Report(0, "POP", "ERROR:GetMessage");
      PRINTF("pop3_Login(): ERROR:GetMessage" CRLF);
   }
   safefree(pcBuffer);

#else    //FEATURE_POP3_APOP

   // Ziggo replies:
   // 43-Do|23-11-2017|09:24:43 26595| [TRC] POP3_LogIn():<pop.ziggo.nl:110>
   // 44-Do|23-11-2017|09:24:48 26595| [TRC] pop3_GetMessage
   // 45-Do|23-11-2017|09:24:48 26595| [TRC] pop3_Gets:End of mail = 2028
   // 46-Do|23-11-2017|09:24:48 26595| [TRC] pop3_DoMessage
   // 47-Do|23-11-2017|09:24:48 26595| [TRC] pop3_GetMessage
   // 48-Do|23-11-2017|09:24:48 26595| [TRC] pop3_Gets:End of mail = 2043
   // 49-Do|23-11-2017|09:24:48 26595| [TRC] pop3_DoMessage
   // 50-Do|23-11-2017|09:24:48 26595| [TRC] pop3_GetMessage
   // 51-Do|23-11-2017|09:24:48 26595| [TRC] pop3_Gets:End of mail = 2032
   // 53-Do|23-11-2017|09:24:48 26595| [TRC] pop3_Login(): Authentication successfull
   // 54-Do|23-11-2017|09:24:48 26595| [TRC] pop3_RetrieveNrOfMessages
   // 55-Do|23-11-2017|09:24:48 26595| [TRC] pop3_GetMessage
   // 56-Do|23-11-2017|09:24:48 26595| [TRC] pop3_Gets:End of mail = 2036
   // 57-Do|23-11-2017|09:24:48 26595| [TRC] rpi_ReceiveMail():2 new emails for nemsi@ziggo.nl

   if(pop3_GetMessage(pstOpt, NULL, NULL))
   {
      //
      // Username
      //
      if(pop3_DoMessage(pstOpt, "USER %s\n", pstOpt->pcUser))
      {
         //
         // Password
         //
         if(pop3_DoMessage(pstOpt, "PASS %s\n", pstOpt->pcPass))
         {
            LOG_Report(0, "POP", "Authentication successfull");
            PRINTF("pop3_Login(): Authentication successfull" CRLF);
            fCc = TRUE;
         }
         else
         {
            LOG_Report(0, "POP", "ERROR:Authentication failed !");
            PRINTF("pop3_Login(): ERROR:Authentication failed !" CRLF);
         }
      }   
      else
      {
         LOG_Report(0, "POP", "ERROR:Username");
         PRINTF("pop3_Login(): ERROR:Username" CRLF);
      }
   }                  
   else
   {
      LOG_Report(0, "POP", "ERROR:GetMessage");
      PRINTF("pop3_Login(): ERROR:GetMessage" CRLF);
   }
#endif   //FEATURE_POP3_APOP
   return(fCc);  
}

//  
//  Name:         pop3_Connect
//  Description:  Connect to the POP3 mail server
//  Arguments:    MAILO struct, Server, Port
//  
//  Returns:      TRUE if OKee
//  Note:         The socket id is stored in the MAILO struct
//  
static bool pop3_Connect(MAILO *pstOpt, char *pcServer, int iPort)
{
   bool        fCc=FALSE;
   const char *pcProtocol = "tcp";

   if(pstOpt->fSsl)
   {
      if( (pstOpt->iSocket = NET_ClientConnect(pcServer, iPort, pcProtocol)) > 0)
      {
         if( SSL_RpiConnect(pstOpt) ) fCc = TRUE;
      }
   }
   else
   {
      if( (pstOpt->iSocket = NET_ClientConnect(pcServer, iPort, pcProtocol)) > 0) fCc = TRUE;
   }
   return(fCc);
}

//  
//  Name:         pop3_Read
//  Description:  Read data from the mail server
//  Arguments:    MAILO struct, buffer, size
//  
//  Returns:      Number read
//  
static int pop3_Read(MAILO *pstOpt, char *pcBuffer, int iSize)
{
   int   x;

   if(pstOpt->fSsl)
   {
      x = SSL_RpiRead(pstOpt, pcBuffer, iSize);
   }
   else
   {
      x = NET_Read(pstOpt->iSocket, pcBuffer, 1);
   }
   return(x);
}

//  
//  Name:         pop3_Write
//  Description:  Write data to the POP3 mail server
//  Arguments:    MAILO struct, buffer, size
//  
//  Returns:      Nr written
//  
static int pop3_Write(MAILO *pstOpt, char *pcBuffer, int iSize)
{
   int x;

   if(pstOpt->fSsl)
   {
      x = SSL_RpiRead(pstOpt, pcBuffer, iSize);
   }
   else
   {
      x = NET_Write(pstOpt->iSocket, pcBuffer, iSize);
   }
   return(x);
}

//  
//  Name:         pop3_LogOff
//  Description:  Log off of the mail server
//  Arguments:    MAILO struct
//  
//  Returns:      TRUE if OKee
//  
static bool pop3_LogOff(MAILO *pstOpt)
{
   bool  fCc=FALSE;
   
   PRINTF("pop3_LogOff()" CRLF);
   fCc = pop3_DoMessage(pstOpt, "QUIT\n", NULL);
   pop3_Close(pstOpt);
   //
   return(fCc);
}

//  
//  Name:         pop3_Close
//  Description:  Close the connection to the POP3 mail server
//  Arguments:    MAILO struct
//  
//  Returns:      
//  
static void pop3_Close(MAILO *pstOpt)
{
   if(pstOpt->fSsl)  SSL_RpiClose(pstOpt);
   safeclose(pstOpt->iSocket);     
}
    
//  
//  Name:         pop3_RetrieveNrOfMessages
//  Description:  Get the number of unread messages from the server
//  Arguments:    MAILO struct, MsgNr ptr
//  
//  Returns:      TRUE if OKee
//  
static bool pop3_RetrieveNrOfMessages(MAILO *pstOpt, int *piMessages)
{ 
   bool     fCc=FALSE;
   int      iNum=-1;
   char    *pcBuffer;
   int      iSize=MAIL_BUFLEN;

   PRINTF("pop3_RetrieveNrOfMessages" CRLF);
   //
   pcBuffer = safemalloc(iSize);
   if(pop3_PutMessage(pstOpt, "STAT\n", NULL))
   { 
      if(pop3_GetMessage(pstOpt, pcBuffer, &iSize))
      {
        iNum = atoi(&pcBuffer[4]);
        fCc  = TRUE;
      }
   }     
   *piMessages = iNum;
   safefree(pcBuffer);
   //
   return(fCc);
}

//  
//  Name:         pop3_RetrieveHeader
//  Description:  Receive the mail header plus iNumLines lines of the body
//  Arguments:    MAILO struct, Msg, NumLines, NfyFunction, void ptr data
//  
//  Returns:      TRUE if OKee
//  
static bool pop3_RetrieveHeader(MAILO *pstOpt, int iMessageNr, int iNumLines, MAILRCB pfnSaveData, void *pvData)
{ 
   bool     fCc=FALSE;
   bool     fKeepReading=TRUE;
   char    *pcBuffer;
   int      iSize=MAIL_BUFLEN;   
  
   PRINTF("pop3_RetrieveHeader" CRLF);
   pcBuffer = safemalloc(MAIL_BUFLEN+2);
   GEN_SPRINTF(pcBuffer, "%d %d", iMessageNr, iNumLines); 
   //
   if(pop3_PutMessage(pstOpt, "TOP %s\n", pcBuffer))
   { 
      if(pop3_GetMessage(pstOpt, pcBuffer, &iSize))
      {
         do
         {
            iSize = pop3_Gets(pstOpt, pcBuffer, MAIL_BUFLEN);
            if(iSize != MAIL_BUFLEN)
            {
               // Make sure the mailbuffer is \0 terminated
               if(pfnSaveData) 
               {
                  pcBuffer[MAIL_BUFLEN-iSize] = '\0';
                  pfnSaveData(iMessageNr, pcBuffer, MAIL_BUFLEN - iSize, pvData);
               }
            }  
            else fKeepReading = FALSE;  
         } 
         while(fKeepReading == TRUE);
         fCc = TRUE;
      }
   }     
   safefree(pcBuffer);
   //
   return(fCc);
}

//  
//  Name:         pop3_RetrieveMessage
//  Description:  Retrieve a complete message from the POP3 server
//  Arguments:    MAILO struct, MsgNr, Callback, DataPtr
//  
//  Returns:      TRUE if OKee
//  
static bool pop3_RetrieveMessage(MAILO *pstOpt, int iMessageNr, MAILRCB pfnSaveData, void *pvData)
{ 
   bool     fCc=FALSE;
   bool     fKeepReading=TRUE;
   int      iMailSize=0;
   char    *pcBuffer;
   int      iSize=MAIL_BUFLEN;   
  
   PRINTF("pop3_RetrieveMessage" CRLF);
   //
   if(!pstOpt->fSsl)
   {
      pcBuffer = safemalloc(MAIL_BUFLEN+2);
      GEN_SPRINTF(pcBuffer, "%d", iMessageNr); 
      //
      if(pop3_PutMessage(pstOpt, "RETR %s\n", pcBuffer))
      { 
         if(pop3_GetMessage(pstOpt, pcBuffer, &iSize))
         {
            fCc = TRUE;
            iMailSize = atoi(&pcBuffer[4]);
            PRINTF1("pop3_RetrieveMessage:Msg=%d bytes" CRLF, iMailSize);
            do
            {
               iSize = pop3_Gets(pstOpt, pcBuffer, MAIL_BUFLEN);
               if(iSize != MAIL_BUFLEN)
               {
                  // Make sure the mailbuffer is \0 terminated
                  if(pfnSaveData) 
                  {
                     pcBuffer[MAIL_BUFLEN-iSize] = '\0';
                     pfnSaveData(iMessageNr, pcBuffer, MAIL_BUFLEN - iSize, pvData);
                  }
                  iMailSize += iSize-MAIL_BUFLEN;  // Continue with the rest of the mail message
               }    
               else fKeepReading = FALSE;  
            } 
            while(fKeepReading == TRUE);
            PRINTF1("pop3_RetrieveMessage:Now=%d bytes" CRLF, iMailSize);
         }
      }     
      safefree(pcBuffer);
   }
   return(fCc);
}

//  
//  Name:         pop3_DeleteMessage
//  Description:  Delete this message
//  Arguments:    MAILO struct, MsgNr
//  
//  Returns:      TRUE if OKee
//  
static bool pop3_DeleteMessage(MAILO *pstOpt, int iMessageNr)
{ 
   bool  fCc;
   char *pcBuffer;

   PRINTF1("pop3_DeleteMessage():Delete msg=%d" CRLF, iMessageNr);
   //
   pcBuffer = safemalloc(MAIL_BUFLEN);
   GEN_SPRINTF(pcBuffer, "%d", iMessageNr);
   fCc = pop3_DoMessage(pstOpt, "DELE %s\n", pcBuffer);
   safefree(pcBuffer);
   //
   return(fCc);
}


