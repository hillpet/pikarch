/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           mail_ssl.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi SSL mail handler
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include <common.h>
#include "../config.h"
#include "../globals.h"

#ifdef   FEATURE_USE_SSL
//
// OpenSSL headers
//
#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/conf.h>
#include <openssl/x509.h>
#include <openssl/buffer.h>
#include <openssl/x509v3.h>
#include <openssl/opensslconf.h>
#include <openssl/md5.h>
#endif   //FEATURE_USE_SSL
// 
#include "mail_func.h"
#include "mail_ssl.h"

//#define USE_PRINTF
#include <printf.h>

//
// Static prototypes
//
#ifdef   FEATURE_USE_SSL
//
static SSL_CTX   *ssl_InitOpenSsl         (void);
static void       ssl_ReportError         (SSL *, const char *, int);
static void       ssl_ShowCerts           (SSL *);
//
#ifdef FEATURE_SSL_VERIFY_CERTS
static int        ssl_VerifyCallback      (int, X509_STORE_CTX *);
static void       ssl_PrintCnName         (const char *, X509_NAME *);
static void       ssl_PrintSanName        (const char *, X509 *);
#endif //FEATURE_SSL_VERIFY_CERTS
//
#endif //FEATURE_USE_SSL

// 
//   Name:         SSL_RpiInit
//   Description:  If SSL enabled, init SSL data
//   Arguments:    MAILO struct
//  
//   Returns:      TRUE if OKee
//  
bool SSL_RpiInit(MAILO *pstOpt)
{
   bool  fCc=FALSE;

#ifdef   FEATURE_USE_SSL
   SSL_CTX    *pstCtx;

   PRINTF("SSL_RpiInit()"CRLF);
   //
   // Initializing OpenSSL
   //
   if( (pstCtx = ssl_InitOpenSsl()) ) 
   {
      pstOpt->pstCtx = pstCtx;

      #ifdef FEATURE_SSL_VERIFY_CERTS
      long        lRes;   
      const long  lFlags = SSL_OP_ALL | SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION;
      //
      // Verify server certificate
      //
      SSL_CTX_set_verify(pstCtx, SSL_VERIFY_PEER, ssl_VerifyCallback);
      //
      SSL_CTX_set_verify_depth(pstCtx, 5);
      lRes = SSL_CTX_set_options(pstCtx, lFlags);
      PRINTF1("SSL_RpiInit():SSL_CTX_set_options()=%ld" CRLF, lRes);
      //
      LOG_Report(0, "SSL", "SSL_RpiInit():ssl_InitOpenSsl()=OKee");
      #endif   //FEATURE_SSL_VERIFY_CERTS

      fCc = TRUE;
   }
   else
   {
      PRINTF("SSL_RpiInit():ERROR:ssl_InitOpenSsl() failed" CRLF);
      LOG_Report(0, "SSL", "SSL_RpiInit():ERROR:ssl_InitOpenSsl() failed");
   }
#endif //FEATURE_USE_SSL
   return(fCc);
}

// 
//   Name:         SSL_RpiConnect
//   Description:  Connect to SSL Server
//   Arguments:    MAILO struct
//  
//   Returns:      TRUE if OKee
//  
bool SSL_RpiConnect(MAILO *pstOpt)
{
   bool  fCc=FALSE;

#ifdef   FEATURE_USE_SSL
   int   iRet=0;
   SSL  *pstSsl=NULL;
   
   if(pstOpt->pstCtx)
   {
      //LOG_Report(0, "SSL", "SSL_RpiConnect()");
      //
      if( (pstSsl = SSL_new(pstOpt->pstCtx)) )
      {
         //LOG_Report(0, "SSL", "SSL_RpiConnect():SSL_new()=OKee");
         //
         // Connect socket to SSL stack
         //
         if( (iRet = SSL_set_fd(pstSsl, pstOpt->iSocket)) )
         {
            //
            // SSL_set_fd() sets the file descriptor fd as the input/output facility for the TLS/SSL
            // (encrypted) side of ssl. fd will typically be the socket file descriptor of a network connection.
            // When performing the operation, a socket BIO is automatically created to interface between
            // the ssl and fd. The BIO and hence the SSL engine inherit the behaviour of fd. If fd is
            // non-blocking, the ssl will also have non-blocking behaviour. If there was already a BIO 
            // connected to ssl, BIO_free() will be called (for both the reading and writing side, if different). 
            //
            //LOG_Report(0, "SSL", "SSL_RpiConnect():SSL_set_fd()=OKee");
            //
            // SSL_connect(): Negotiate the SSL/TLS protocol:
            //     0:   The TLS/SSL handshake was not successful but was shut down controlled and by the 
            //          specifications of the TLS/SSL protocol. Call SSL_get_error() with the return value 
            //          ret to find out the reason.
            //     1:   The TLS/SSL handshake was successfully completed, a TLS/SSL connection has been established.
            //    <0:   The TLS/SSL handshake was not successful, because a fatal error occurred either at 
            //          the protocol level or a connection failure occurred. The shutdown was not clean. 
            //          It can also occur of action is need to continue the operation for non-blocking BIOs. 
            //          Call SSL_get_error() with the return value ret to find out the reason.
            //
            if( (iRet = SSL_connect(pstSsl)) > 0)
            {
               //LOG_Report(0, "SSL", "Successfully enabled SSL/TLS session, cc=%d", iRet);
               ssl_ShowCerts(pstSsl);
               pstOpt->pstSsl = pstSsl;
               fCc = TRUE;
            }
            else
            {
               LOG_Report(0, "SSL", "SSL_RpiConnect():Error: Could not build a SSL session, cc=%d", iRet);
               ssl_ReportError(pstSsl, "SSL_connect", iRet);
            }
         }
         else
         {
            LOG_Report(0, "SSL", "SSL_RpiConnect():ERROR: SSL_set_fd() failed, cc=%d", iRet);
            ssl_ReportError(pstSsl, "SSL_set_fd", iRet);
         }
      }
      else
      {
         LOG_Report(0, "SSL", "SSL_RpiConnect():ERROR:SSL_new() failed");
         ssl_ReportError(pstSsl, "SSL_new", 0);
      }
   }
   else
   {
      LOG_Report(0, "SSL", "SSL_RpiConnect():ERROR: No SSL-CTX");
   }
#endif //FEATURE_USE_SSL
   return(fCc);
}

// 
//   Name:         SSL_RpiRead
//   Description:  Read data through SSL socket
//   Arguments:    MAILO ptr, buffer, nr
//  
//   Returns:      Nr read
//  
int SSL_RpiRead(MAILO *pstOpt, char *pcBuffer, int iLen)
{
   int   iNr=0;

#ifdef   FEATURE_USE_SSL
   if(pstOpt->pstSsl) 
   {
      iNr = SSL_read(pstOpt->pstSsl, pcBuffer, iLen);
      if(iNr < 0) ssl_ReportError(pstOpt->pstSsl, "SSL_RpiRead", iNr);
   }
#endif //FEATURE_USE_SSL
  return(iNr);
}

// 
//   Name:         SSL_RpiWrite
//   Description:  
//   Arguments:    
//  
//   Returns:      
//  
int SSL_RpiWrite(MAILO *pstOpt, char *pcBuffer, int iLen)
{
  int   iNr=0; 

#ifdef   FEATURE_USE_SSL
   if(pstOpt->pstSsl) 
   {
      iNr = SSL_write(pstOpt->pstSsl, pcBuffer, iLen);
      if(iNr < 0) ssl_ReportError(pstOpt->pstSsl, "SSL_RpiWrite", iNr);
   }
#endif //FEATURE_USE_SSL
  return(iNr);
}

// 
//   Name:         SSL_RpiClose
//   Description:  Close the connection, free the memory
//   Arguments:    MAILO struct
//  
//   Returns:      TRUE if OKee
//  
bool SSL_RpiClose(MAILO *pstOpt)
{
   PRINTF("SSL_RpiClose"CRLF);

#ifdef   FEATURE_USE_SSL
   if(pstOpt->pstSsl)      SSL_free(pstOpt->pstSsl);
   if(pstOpt->pstCtx)      SSL_CTX_free(pstOpt->pstCtx);
   //
   pstOpt->pstSsl  = NULL;
   pstOpt->pstCtx  = NULL;
#endif //FEATURE_USE_SSL

  return(TRUE);
}

// 
//   Name:         SSL_RpiDigestMD5
//   Description:  Encode buffer to MD5
//   Arguments:    Dest ptr, dest length, Source, source length
//  
//   Returns:      TRUE if OKee
//  
bool SSL_RpiDigestMD5(char *pcMd5, int iMd5Len, char *pcBuffer, int iLen)
{
   bool     fCc=FALSE;
   int      i;
   MD5_CTX  stCtx;
   u_int8   ubDigest[MD5_DIGEST_LENGTH];

   if(iMd5Len > (2 * MD5_DIGEST_LENGTH))
   {
      PRINTF1("SSL_RpiDigestMD5():Size=%d" CRLF, 2 * sizeof(ubDigest));
      MD5_Init(&stCtx);
      MD5_Update(&stCtx, pcBuffer, iLen);
      MD5_Final(ubDigest, &stCtx);
      //
      // ubDigest now holds the binary value of the MD5 digest of the buffer
      // Rework to ascii
      //
      GEN_MEMSET(pcMd5, 0x00, iMd5Len);
      for(i=0; i<MD5_DIGEST_LENGTH; ++i)
      {
         GEN_SPRINTF(&pcMd5[i*2], "%02x", (unsigned int)ubDigest[i]);
      }
      fCc = TRUE;
   }
   else PRINTF2("SSL_RpiDigestMD5():MD5 buffer too small: MD5=%d, Act=%d" CRLF, (2*sizeof(ubDigest))+1, iMd5Len);
   return(fCc);
}

/* ======   Local Functions separator ===========================================
___LOCAL_FUNCTIONS(){}
==============================================================================*/

#ifdef   FEATURE_USE_SSL
//
// Function:   ssl_InitOpenSsl
// Purpose:    Init OpenSsl library
// Parms:      
//
// Returns:    SSL_CTX struct
// Note:       
//             
//
static SSL_CTX *ssl_InitOpenSsl(void)
{   
   const SSL_METHOD *pstMethod;
   SSL_CTX          *pstCtx=NULL;

   //
   // Init Openssl library
   //
   SSL_library_init();
   //LOG_Report(0, "SSL", "ssl_InitOpenSsl():Called SSL_library_init()");
   //
   // Bring in and register error messages
   //
   SSL_load_error_strings();
   //LOG_Report(0, "SSL", "ssl_InitOpenSsl():Called SSL_load_error_strings()");
   //
   // Load cryptos, et.al
   //
   OpenSSL_add_all_algorithms();
   //LOG_Report(0, "SSL", "ssl_InitOpenSsl():Called OpenSSL_add_all_algorithms()");
   //
   OPENSSL_config(NULL);
   //LOG_Report(0, "SSL", "ssl_InitOpenSsl():Called OPENSSL_config(NULL)");
   //
   // Create new client-method instance
   //
   // pstMethod = (SSL_METHOD *)TLSv1_2_client_method();
   if( (pstMethod = SSLv23_method()) == NULL)
   {
      ssl_ReportError(NULL, "SSLv23_method", 0);
   }
   else
   {
      //LOG_Report(0, "SSL", "ssl_InitOpenSsl():Called SSLv23_method()=OKee");
      //
      // Create new context
      //
      if( (pstCtx = SSL_CTX_new(pstMethod)) == NULL)
      {
         ssl_ReportError(NULL, "SSL_CTX_new", 0);
      }
      else
      {
         //LOG_Report(0, "SSL", "ssl_InitOpenSsl():Called SSL_CTX_new(meth)=OK");
      }
   }
   return(pstCtx);
}

//
// Function:   ssl_ShowCerts
// Purpose:    Show SSL certificates
// Parms:      SSL struct
//
// Returns:    
// Note:       
//             
//
static void ssl_ShowCerts(SSL *pstSsl)
{   
   X509 *pstCert;
   char *pcLine;

   //
   // get the server's certificate
   //
   if( (pstCert = SSL_get_peer_certificate(pstSsl)) )
   {
      //LOG_Report(0, "SSL", "ssl_ShowCerts():Called SSL_get_peer_certificate(ssl): OKee");
      PRINTF1("ssl_ShowCerts():Connected with %s encryption" CRLF, SSL_get_cipher(pstSsl));
      PRINTF("ssl_ShowCerts():Server certificates:" CRLF);
      pcLine = X509_NAME_oneline(X509_get_subject_name(pstCert), 0, 0);
      PRINTF1("ssl_ShowCerts():Subject: %s" CRLF, pcLine);
      free(pcLine);
      pcLine = X509_NAME_oneline(X509_get_issuer_name(pstCert), 0, 0);
      PRINTF1("ssl_ShowCerts():Issuer: %s" CRLF, pcLine);
      free(pcLine);
      X509_free(pstCert);
   }
   else 
   {
      LOG_Report(0, "SSL", "ssl_ShowCerts():Called SSL_get_peer_certificate(ssl): No certs");
      PRINTF("ssl_ShowCerts():Info: No client certificates configured" CRLF);
   }
}

//
// Function:   ssl_ReportError
// Purpose:    Report SSL error
// Parms:      SSL struct, Function string, return code
//
// Returns:    
// Note:       
//             
//
static void ssl_ReportError(SSL *pstSsl, const char *pcFunc, int iRet)
{
   bool        fCheckSslStack=FALSE;
   const char *pcErrStr;
   long        lSslErr;
   int         iSslErr;
        
   if(pstSsl) 
   {
      iSslErr = SSL_get_error(pstSsl, iRet);
      //
      switch(iSslErr)
      {
         case SSL_ERROR_NONE:
            // The TLS/SSL I/O operation completed. This result code is returned if and only if ret > 0. 
            LOG_Report(0, "SSL", "ssl_ReportError():%s()-SSL_ERROR_NONE", pcFunc);
            fCheckSslStack = TRUE;
            break;

         case SSL_ERROR_ZERO_RETURN:
            // The TLS/SSL connection has been closed. 
            // If the protocol version is SSL 3.0 or TLS 1.0, this result code is returned only if a 
            // closure alert has occurred in the protocol, i.e. if the connection has been closed cleanly. 
            // Note that in this case SSL_ERROR_ZERO_RETURN does not necessarily indicate that the 
            // underlying transport has been closed. 
            LOG_Report(0, "SSL", "ssl_ReportError():%s()-SSL_ERROR_ZERO_RETURN", pcFunc);
            break;

         case SSL_ERROR_WANT_READ:
            // The operation did not complete; the same TLS/SSL I/O function should be called again later. 
            // If, by then, the underlying BIO has data available for reading (if the result code is 
            // SSL_ERROR_WANT_READ) or allows writing data (SSL_ERROR_WANT_WRITE), then some TLS/SSL 
            // protocol progress will take place, i.e. at least part of an TLS/SSL record will be read 
            // or written. Note that the retry may again lead to a SSL_ERROR_WANT_READ or 
            // SSL_ERROR_WANT_WRITE condition. There is no fixed upper limit for the number of iterations 
            // that may be necessary until progress becomes visible at application protocol level. 
            // For socket BIOs (e.g. when SSL_set_fd() was used), select() or poll() on the underlying 
            // socket can be used to find out when the TLS/SSL I/O function should be retried. 
            // Caveat: Any TLS/SSL I/O function can lead to either of SSL_ERROR_WANT_READ and 
            // SSL_ERROR_WANT_WRITE. In particular, SSL_read() or SSL_peek() may want to write data and 
            // SSL_write() may want to read data. This is mainly because TLS/SSL handshakes may occur at 
            // any time during the protocol (initiated by either the client or the server); 
            // SSL_read(), SSL_peek(), and SSL_write() will handle any pending handshakes. 
            LOG_Report(0, "SSL", "ssl_ReportError():%s()-SSL_ERROR_WANT_READ", pcFunc);
            break;

         case SSL_ERROR_WANT_WRITE:
            // See comments at SSL_ERROR_WANT_READ
            LOG_Report(0, "SSL", "ssl_ReportError():%s()-SSL_ERROR_WANT_WRITE", pcFunc);
            break;

         case SSL_ERROR_WANT_CONNECT:
            // The operation did not complete; the same TLS/SSL I/O function should be called again later. 
            // The underlying BIO was not connected yet to the peer and the call would block in connect()/accept(). 
            // The SSL function should be called again when the connection is established. 
            // These messages can only appear with a BIO_s_connect() or BIO_s_accept() BIO, respectively. 
            // In order to find out, when the connection has been successfully established, on many platforms 
            // select() or poll() for writing on the socket file descriptor can be used. 
            LOG_Report(0, "SSL", "ssl_ReportError():%s()-SSL_ERROR_WANT_CONNECT", pcFunc);
            break;

         case SSL_ERROR_WANT_ACCEPT:
            // See comments at SSL_ERROR_WANT_CONNECT
            LOG_Report(0, "SSL", "ssl_ReportError():%s()-SSL_ERROR_WANT_ACCEPT", pcFunc);
            break;

         case SSL_ERROR_WANT_X509_LOOKUP:
            // The operation did not complete because an application callback set by SSL_CTX_set_client_cert_cb() 
            // has asked to be called again. The TLS/SSL I/O function should be called again later. 
            // Details depend on the application. 
            LOG_Report(0, "SSL", "ssl_ReportError():%s()-SSL_ERROR_WANT_X509_LOOKUP", pcFunc);
            break;

         case SSL_ERROR_SYSCALL:
            // Some I/O error occurred. The OpenSSL error queue may contain more information on the error. 
            // If the error queue is empty (i.e. ERR_get_error() returns 0), ret can be used to find out 
            // more about the error: If ret == 0, an EOF was observed that violates the protocol. 
            // If ret == -1, the underlying BIO reported an I/O error (for socket I/O on Unix systems, 
            // consult errno for details). 
            LOG_Report(0, "SSL", "ssl_ReportError():%s()-SSL_ERROR_SYSCALL", pcFunc);
            break;

         case SSL_ERROR_SSL:
            // A failure in the SSL library occurred, usually a protocol error. 
            // The OpenSSL error queue contains more information on the error. 
            LOG_Report(0, "SSL", "ssl_ReportError():%s()-SSL_ERROR_SSL, check error stack:", pcFunc);
            fCheckSslStack = TRUE;
            break;

         default:
            LOG_Report(0, "SSL", "ssl_ReportError():%s()-Unspecified error, check error stack:", pcFunc);
            fCheckSslStack = TRUE;
            break;
      }
   }
   //
   // Check the complete SSl error stack
   //
   if(fCheckSslStack)
   {
      do
      {
         if( (lSslErr = ERR_get_error()) )
         {
            pcErrStr = ERR_reason_error_string(lSslErr);
            //
            if(pcErrStr) LOG_Report(0, "SSL", "ssl_ReportError():%s()-No Error string, code=%ld", pcFunc, lSslErr);
            else         LOG_Report(0, "SSL", "ssl_ReportError():%s()-%s", pcFunc, pcErrStr);
         }
      }
      while(lSslErr);
      LOG_Report(0, "SSL", "ssl_ReportError():%s():-Error stack is empty.", pcFunc);
   }
}

#ifdef FEATURE_SSL_VERIFY_CERTS
//
// Function:   ssl_VerifyCallback
// Purpose:    Callback function to verify server certificate
// Parms:      Code, Certificate struct
//
// Returns:    
// Note:       
//
static int ssl_VerifyCallback(int iPreVerify, X509_STORE_CTX* pstX509Ctx)
{
   // For error codes, see http://www.openssl.org/docs/apps/verify.html  */
    
   int iDepth = X509_STORE_CTX_get_error_depth(pstX509Ctx);
   int iError = X509_STORE_CTX_get_error(pstX509Ctx);
   
   X509       *pstCert  = X509_STORE_CTX_get_current_cert(pstX509Ctx);
   X509_NAME  *pstIname = pstCert ? X509_get_issuer_name(pstCert) : NULL;
   X509_NAME  *pstSname = pstCert ? X509_get_subject_name(pstCert) : NULL;
   
   LOG_Report(0, "SSL", "ssl_VerifyCallback():(depth=%d)(iPreVerify=%d)", iDepth, iPreVerify);
   
   /* Issuer is the authority we trust that warrants nothing useful */
   ssl_PrintCnName("Issuer (cn)", pstIname);
   
   /* Subject is who the certificate is issued to by the authority  */
   ssl_PrintCnName("Subject (cn)", pstSname);
   
   if(iDepth == 0) 
   {
       /* If iDepth is 0, its the server's certificate. Print the SANs */
       ssl_PrintSanName("Subject (san)", pstCert);
   }
   if(iPreVerify == 0)
   {
       if(iError == X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY)
           LOG_Report(0, "SSL", "ssl_VerifyCallback():Error = X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY");
       else if(iError == X509_V_ERR_CERT_UNTRUSTED)
           LOG_Report(0, "SSL", "ssl_VerifyCallback():Error = X509_V_ERR_CERT_UNTRUSTED");
       else if(iError == X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN)
           LOG_Report(0, "SSL", "ssl_VerifyCallback():Error = X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN");
       else if(iError == X509_V_ERR_CERT_NOT_YET_VALID)
           LOG_Report(0, "SSL", "ssl_VerifyCallback():Error = X509_V_ERR_CERT_NOT_YET_VALID");
       else if(iError == X509_V_ERR_CERT_HAS_EXPIRED)
           LOG_Report(0, "SSL", "ssl_VerifyCallback():Error = X509_V_ERR_CERT_HAS_EXPIRED");
       else if(iError == X509_V_OK)
           LOG_Report(0, "SSL", "ssl_VerifyCallback():Error = X509_V_OK");
       else
           LOG_Report(0, "SSL", "ssl_VerifyCallback():Error = %d", iError);
   }
   return(iPreVerify);
}

//
// Function:   ssl_PrintCnName
// Purpose:    
// Parms:      
//
// Returns:    
// Note:       
//
static void ssl_PrintCnName(const char *pcLabel, X509_NAME *pstName)
{
   int            iIdx=-1, iCc=0;
   unsigned char *pcUtf8=NULL;
   
   do
   {
      if(!pstName) break;
      //
      iIdx = X509_NAME_get_index_by_NID(pstName, NID_commonName, -1);
      if(!(iIdx > -1))  break;
      //
      X509_NAME_ENTRY* pstEntry = X509_NAME_get_entry(pstName, iIdx);
      if(!pstEntry) break;
      //
      ASN1_STRING* pstData = X509_NAME_ENTRY_get_data(pstEntry);
      if(!pstData) break;
      //
      int iLen = ASN1_STRING_to_UTF8(&pcUtf8, pstData);
      if(!pcUtf8 || !(iLen > 0)) break;
      //
      LOG_Report(0,"SSL", "ssl_PrintCnName():%s:%s", pcLabel, pcUtf8);
      iCc = 1;
       
   } 
   while(0);
   //
   if(pcUtf8) OPENSSL_free(pcUtf8);
   
   if(!iCc) LOG_Report(0,"SSL", "ssl_PrintCnName():%s: <not available>\n", pcLabel);
}

//
// Function:   ssl_PrintSanName
// Purpose:    
// Parms:      
//
// Returns:    
// Note:       
//             
static void ssl_PrintSanName(const char* pcLabel, X509 *pstCert)
{
   int              iCc=0;
   GENERAL_NAMES   *pstNames=NULL;
   unsigned char   *pcUtf8=NULL;
   
   do
   {
      if(!pstCert) break;
      
      pstNames = X509_get_ext_d2i(pstCert, NID_subject_alt_name, 0, 0 );
      if(!pstNames) break;
      
      int i=0, iCount=sk_GENERAL_NAME_num(pstNames);
      if(!iCount) break;
      //
      for(i=0; i<iCount; ++i)
      {
         GENERAL_NAME* pstEntry = sk_GENERAL_NAME_value(pstNames, i);
         if(!pstEntry) continue;
         if(GEN_DNS == pstEntry->type)
         {
            int iLen1=0, iLen2=-1;
            
            iLen1 = ASN1_STRING_to_UTF8(&pcUtf8, pstEntry->d.dNSName);
            if(pcUtf8) 
            {
               iLen2 = (int)strlen((const char*)pcUtf8);
            }
            if(iLen1 != iLen2) 
            {
               LOG_Report(0, "SSL", "ssl_PrintSanName():Strlen and ASN1_STRING size do not match (embedded null?): %d vs %d\n", iLen2, iLen1);
            }
            //
            // If there's a problem with string lengths, then
            // we skip the candidate and move on to the next.
            // Another policy would be to fails since it probably
            // indicates the client is under attack.
            //
            if(pcUtf8 && iLen1 && iLen2 && (iLen1 == iLen2)) 
            {
               LOG_Report(0, "SSL", "ssl_PrintSanName():%s: %s\n", pcLabel, pcUtf8);
               iCc = 1;
            }
            if(pcUtf8) 
            {
               OPENSSL_free(pcUtf8), pcUtf8 = NULL;
            }
         }
         else
         {
             LOG_Report(0, "SSL", "ssl_PrintSanName():Unknown GENERAL_NAME type: %d\n", pstEntry->type);
         }
      }
   } 
   while(0);
   //
   if(pstNames) GENERAL_NAMES_free(pstNames);
   if(pcUtf8)   OPENSSL_free(pcUtf8);
   if(!iCc)     LOG_Report(0, "SSL", "ssl_PrintSanName():%s: <not available>\n", pcLabel);
}
#endif   //FEATURE_SSL_VERIFY_CERTS


#endif   //FEATURE_USE_SSL

/* ======   Local Functions separator ===========================================
___COMMENT(){}
==============================================================================*/
#ifdef COMMENT

main()
{
   #define HOST_NAME "www.random.org"
   #define HOST_PORT "443"
   #define HOST_RESOURCE "/cgi-bin/randbyte?nbytes=32&format=h"

   long res = 1;

   SSL_CTX* ctx = NULL;
   BIO *web = NULL, *out = NULL;
   SSL *ssl = NULL;

   init_openssl_library();

   const SSL_METHOD* method = SSLv23_method();
   if(!(NULL != method)) handleFailure();

   ctx = SSL_CTX_new(method);
   if(!(ctx != NULL)) handleFailure();

   /* Cannot fail ??? */
   SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, verify_callback);

   /* Cannot fail ??? */
   SSL_CTX_set_verify_depth(ctx, 4);

   /* Cannot fail ??? */
   const long flags = SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION;
   SSL_CTX_set_options(ctx, flags);

   res = SSL_CTX_load_verify_locations(ctx, "random-org-chain.pem", NULL);
   if(!(1 == res)) handleFailure();

   web = BIO_new_ssl_connect(ctx);
   if(!(web != NULL)) handleFailure();

   res = BIO_set_conn_hostname(web, HOST_NAME ":" HOST_PORT);
   if(!(1 == res)) handleFailure();

   BIO_get_ssl(web, &ssl);
   if(!(ssl != NULL)) handleFailure();

   const char* const PREFERRED_CIPHERS = "HIGH:!aNULL:!kRSA:!PSK:!SRP:!MD5:!RC4";
   res = SSL_set_cipher_list(ssl, PREFERRED_CIPHERS);
   if(!(1 == res)) handleFailure();

   res = SSL_set_tlsext_host_name(ssl, HOST_NAME);
   if(!(1 == res)) handleFailure();

   out = BIO_new_fp(stdout, BIO_NOCLOSE);
   if(!(NULL != out)) handleFailure();

   res = BIO_do_connect(web);
   if(!(1 == res)) handleFailure();

   res = BIO_do_handshake(web);
   if(!(1 == res)) handleFailure();

   /* Step 1: verify a server certificate was presented during the negotiation */
   X509* pstCert = SSL_get_peer_certificate(ssl);
   if(pstCert) { X509_free(pstCert); } /* Free immediately */
   if(NULL == pstCert) handleFailure();

   /* Step 2: verify the result of chain verification */
   /* Verification performed according to RFC 4158    */
   res = SSL_get_verify_result(ssl);
   if(!(X509_V_OK == res)) handleFailure();

   /* Step 3: hostname verification */
   /* An exercise left to the reader */

   BIO_puts(web, "GET " HOST_RESOURCE " HTTP/1.1\r\n"
                 "Host: " HOST_NAME "\r\n"
                 "Connection: close\r\n\r\n");
   BIO_puts(out, "\n");

   int len = 0;
   do
   {
     char buff[1536] = {};
     len = BIO_read(web, buff, sizeof(buff));
            
     if(len > 0)
       BIO_write(out, buff, len);

   } while (len > 0 || BIO_should_retry(web));

   if(out)
     BIO_free(out);

   if(web != NULL)
     BIO_free_all(web);

   if(NULL != ctx)
     SSL_CTX_free(ctx);
}


void init_openssl_library(void)
{
  SSL_library_init();
  SSL_load_error_strings();
  OPENSSL_config(NULL);
}

// 
//   Name:         SSL_RpiConnect
//   Description:  Connect to SSL Server
//   Arguments:    MAILO struct, mailserver, port
//  
//   Returns:      TRUE if OKee
//  
bool SSL_RpiConnect(MAILO *pstOpt, char *pcServer, int iPort)
{
   bool  fCc=FALSE;

   SSL  *pstSsl=NULL;
   int   iSocket; 
   
   if(pstOpt->pstCtx)
   {
      iSocket = NET_ClientConnect(pcServer, iPort, "tcp"); 
      if(iSocket == -1)
      {
         LOG_Report(errno, "SSL", "SSL_RpiConnect():ERROR: %s:%d failed", pcServer, iPort);
      }
      else
      {
         LOG_Report(0, "SSL", "SSL_RpiConnect():NET_ClientConnect(%s:%d)=OKee", pcServer, iPort);
         pstOpt->iSocket = iSocket;
         //
         if( (pstSsl = SSL_new(pstOpt->pstCtx)) )
         {
            LOG_Report(0, "SSL", "SSL_RpiConnect():SSL_new()=OKee");
            //
            // Connect socket to SSL stack
            //
            if(SSL_set_fd(pstSsl, iSocket))
            {
               //
               // SSL_set_fd() sets the file descriptor fd as the input/output facility for the TLS/SSL
               // (encrypted) side of ssl. fd will typically be the socket file descriptor of a network connection.
               // When performing the operation, a socket BIO is automatically created to interface between
               // the ssl and fd. The BIO and hence the SSL engine inherit the behaviour of fd. If fd is
               // non-blocking, the ssl will also have non-blocking behaviour. If there was already a BIO 
               // connected to ssl, BIO_free() will be called (for both the reading and writing side, if different). 
               //
               LOG_Report(0, "SSL", "SSL_RpiConnect():SSL_set_fd()=OKee");
               //
               // SSL_connect(): Negotiate the SSL/TLS protocol:
               //     0:   The TLS/SSL handshake was not successful but was shut down controlled and by the 
               //          specifications of the TLS/SSL protocol. Call SSL_get_error() with the return value 
               //          ret to find out the reason.
               //     1:   The TLS/SSL handshake was successfully completed, a TLS/SSL connection has been established.
               //    <0:   The TLS/SSL handshake was not successful, because a fatal error occurred either at 
               //          the protocol level or a connection failure occurred. The shutdown was not clean. 
               //          It can also occur of action is need to continue the operation for non-blocking BIOs. 
               //          Call SSL_get_error() with the return value ret to find out the reason.
               //
               if( SSL_connect(pstSsl) > 0)
               {
                  LOG_Report(0, "SSL", "Successfully enabled SSL/TLS session");
                  ssl_ShowCerts(pstSsl);
                  pstOpt->pstSsl = pstSsl;
                  fCc = TRUE;
               }
               else
               {
                  LOG_Report(0, "SSL", "Error: Could not build a SSL session");
                  ssl_ReportError("SSL_connect");
               }
            }
            else
            {
               LOG_Report(0, "SSL", "SSL_RpiConnect():ERROR: SSL_set_fd() failed!");
               ssl_ReportError("SSL_set_fd");
            }
         }
         else
         {
            LOG_Report(0, "SSL", "SSL_RpiConnect():ERROR:SSL_new() failed");
         }
      }
   }
   else
   {
      LOG_Report(0, "SSL", "SSL_RpiConnect():ERROR: No SSL-CTX");
   }
   return(fCc);
}

#endif   //COMMENT