/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           crypto.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            crypto.c headerfile
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CRYPT_H_
#define _CRYPT_H_

#define CRYPT_MAX_HANDLES           8     // MAx number of crypto module handles
#define CRYPT_ILLEGAL_HANDLE        0xFF

#define CRYPT_INTERNAL_KEY_LENGTH   8     // Length of internal key
#define CRYPT_EXTERNAL_KEY_LENGTH   16    //           external key
#define CRYPT_STB_KEY_LENGTH        16    //           stb specific key
#define CRYPT_STATIC_KEY_LENGTH     16    //           static key
#define CRYPT_MAX_KEY_LENGTH        16    // Max key length

#define CRYPT_MAX_APDU_LENGTH       100   // Max ECM CA Apdu size

#define CRYPT_DEFAULT_DEPTH         0     // Use default depth

typedef u_int32 CRYPTH;

typedef enum _crypt_commands
{
   CRYPT_CMD_NONE = 0,                   // 
   CRYPT_CMD_SET_KEY,                    // Set   external key
   CRYPT_CMD_CLEAR_KEY,                  // Clear external key

   NUM_CRYPT_CMDS                        // Number of CMDs
}  CRYPT_CMD;

typedef enum _crypt_modes
{
   //
   // Crypto Modes Shall be backwards compatible, so retain original values
   //
   CRYPT_MODE_NONE = 0,                   // No encryption
   CRYPT_MODE_INTERNAL_KEY,               // Use internal key
   CRYPT_MODE_EXTERNAL_KEY,               // Use external key
   CRYPT_MODE_STATIC_KEY,                 // Use static   key
   CRYPT_MODE_USER_KEY,                   // Use user     key
   CRYPT_MODE_STB_KEY,                    // STB specific key

   NUM_CRYPT_MODES                        // Number of modes
}  CRYPT_MODE;

typedef enum _crypt_level
{
   CRYPT_LEVEL_NONE = 0,                  // No encryption
   CRYPT_LEVEL_DISK,                      // Use Disk    level scrambling
   CRYPT_LEVEL_FILE,                      // Use File    level encryption
   CRYPT_LEVEL_STREAM,                    // Use Stream  level encryption

   NUM_CRYPT_LEVELS                       // Number of modes
}  CRYPT_LEVEL;

typedef struct _crypto_handles
{
   CRYPT_LEVEL tLevel;                    // Crypto level
   CRYPT_MODE  tMode;                     // Crypto mode
   u_int32     ulDepth;                   // Default number of bytes to encrypt
   void       *pBox;                      // Specific info
}  CRYPTO;

typedef struct _crypto_info
{                                         // Total size is DVR_CRYPT_INFO_LENGTH = 128
   CRYPT_LEVEL tLevel;                          // 4
   CRYPT_MODE  tMode;                           // 4
   u_int16     usSpare[10];                     // 20
   u_int8      pubApdu[CRYPT_MAX_APDU_LENGTH];  // 100
}  CRYPTI;

//
// Generic En/Decryption prototypes
//
bool     Crypto_Control             (CRYPT_CMD, void *, u_int32);
CRYPTH   Crypto_Open                (CRYPT_MODE, CRYPT_LEVEL, u_int32);
void     Crypto_Close               (CRYPTH);
u_int32  Crypto_Encrypt             (CRYPTH, u_int8 *, u_int32);
u_int32  Crypto_GetEncryptSize      (u_int32);
u_int32  Crypto_Decrypt             (CRYPTH, u_int8 *, u_int32);

//
// Low level prototypes
//
bool     CryptoModule_FileSetup     (void *, u_int8 *, u_int16);
bool     CryptoModule_StreamSetup   (void *, u_int8 *, u_int16);
u_int32  CryptoModule_GetBlockSize  (u_int32);
u_int32  CryptoModule_Encrypt       (void *, u_int8 *, u_int32);
u_int32  CryptoModule_Decrypt       (void *, u_int8 *, u_int32);
u_int32  CryptoModule_GetBoxSize    (void);
bool     CryptoModule_Validate      (void *);

u_int32  DetermineXORKey(u_int8* pulData, u_int32 ulDataLen);

#endif   //_CRYPT_H_
