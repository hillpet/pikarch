/*  (c) Copyright:  2017  Patrn, Confidential Data 
 *
 *  Workfile:           mail_smtp.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi mail handler
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <common.h>
#include "../config.h"
#include "../globals.h"
#include "mail_ssl.h"
#include "mail_func.h"
#include "mail_smtp.h"

//#define USE_PRINTF
#include <printf.h>

#ifdef   FEATURE_LOG_MAIL_TO_FILE
   static int iFdLog=-1;
   static void smtp_WriteComms(const char *, char *, int);
   #define  LOG_MAIL(a,b,c)                  smtp_WriteComms(a,b,c)
#else    // FEATURE_LOG_MAIL_TO_FILE
   #define  LOG_MAIL(a,b,c)
#endif   // FEATURE_LOG_MAIL_TO_FILE

//
// Static data
//

//
// Static prototypes
//
static bool  smtp_NormalLogIn             (MAILO *);
static bool  smtp_SecureLogIn             (MAILO *);
static char *smtp_ExtractAddr             (MAILO *, int, MAILDT);
static bool  smtp_SendWait                (MAILO *, bool, char *, const char *);

//
// Function:   SMTP_LogIn
// Purpose:    Log in the SMTP server
//
// Parms:      Mail struct
// Returns:    TRUE if OKee
// Note:       
//
bool SMTP_LogIn(MAILO *pstOpt)
{
   bool  fCc=TRUE;
   char *pcIp;

   //LOG_Report(0, "SML", "SMTP_LogIn():Host=%s", pstOpt->pcHost);
   //
   if( (pcIp = NET_ResolveHostName(pstOpt->pcHost)) == NULL) 
   {
      LOG_Report(errno, "SML", "SMTP_LogIn():Resolve %s failed", pstOpt->pcHost);
      return(FALSE);
   }
   //LOG_Report(0, "SML", "SMTP_LogIn():Resolve host %s to %s", pstOpt->pcHost, pcIp);
   //
#ifdef   FEATURE_TEST_SEND_MAIL
   PRINTF3("SMTP_LogIn():MAIL-TEST!  (Mail Server %s <%s:%d>=OKee)" CRLF, pstOpt->pcHost, pcIp, pstOpt->iPort);
#else  //FEATURE_TEST_SEND_MAIL
   //
   PRINTF3("SMTP_LogIn():Connect to mail Server %s <%s:%d>" CRLF, pstOpt->pcHost, pcIp, pstOpt->iPort);
   pstOpt->iSocket = NET_ClientConnect(pcIp, pstOpt->iPort, "tcp");
   safefree(pcIp);
   //
   if(pstOpt->iSocket == BAD_SOCKET)
   {
      LOG_Report(errno, "SML", "SMTP_LogIn():<%s:%d> failed", pstOpt->pcHost, pstOpt->iPort);
      return(FALSE);
   }
   //
   // Initial negotiations are always plaintext (until SSL STARTTLS is chosen)
   //
   LOG_Report(0, "SML", "SMTP_LogIn():Mail Server %s:%d=OKee", pstOpt->pcHost, pstOpt->iPort);
   PRINTF2("SMTP_LogIn():Mail Server %s:%d OKee" CRLF, pstOpt->pcHost, pstOpt->iPort);
   //
   //
   // Log in procedure (plaintext)
   //
   // Negotiate authentication
   //    S: 250 HELP
   //    C: EHLO PatrnRpi10
   //    S: 250 SIZE xxx
   //    S: 250 BIT8MIME
   //    S: 250 ...
   //    S: 250 AUTH PLAIN LOGIN
   //    S: 250 ...
   //    S: 250 HELP
   //    C: AUTH PLAIN <base64 "\0Username\0Password"
   //    S: 235 Authentication succeeded
   //
   if(!smtp_SendWait(pstOpt, FALSE, NULL, "220")) 
   {
      LOG_Report(0, "SML", "SMTP_LogIn():No initial 220 received!");
      PRINTF("SMTP_LogIn():No initial 220 received!" CRLF);
      return(FALSE);
   }
   //
   // Received smtp.ziggo.nl:
   //    Sp:220-smtp7.tb.mail.iss.as9143.net ESMTP Mon, 04 Dec 2017 11:08:20 +0100
   //    220-Sending spam or unsolicited commercial e-mail to this server is strictly
   //    220-prohibited by our NO UBE / NO UCE policy. Abuse will be prosecuted and/or
   //    220 charged per attempted recipient at international postal rates.
   //
   switch(pstOpt->tAuth)
   {
      case MAIL_SETUP_NO_AUTH:
         // We want to use no authentication
         //LOG_Report(0, "SML", "SMTP_LogIn():No User/Psw login");
         PRINTF("SMTP_LogIn():No User/Psw login" CRLF);
         fCc = TRUE;
         break;

      case MAIL_SETUP_PLAIN_AUTH:
         // We want to use plaintext authentication
         fCc = smtp_NormalLogIn(pstOpt);
         break;

      default:
      case MAIL_SETUP_SSL_AUTH:
         // Do not allow plaintext authentication
         if(pstOpt->fSsl) fCc = smtp_SecureLogIn(pstOpt);
         else             fCc = FALSE;
         break;
   }
#endif //FEATURE_TEST_SEND_MAIL

   return(fCc);
}

//
// Function:   SMTP_SendMail
// Purpose:    Send out mail actually.
//
// Parms:      Mail struct
// Returns:    TRUE if OKee
// Note:       
//
bool SMTP_SendMail(MAILO *pstOpt)
{
   int      i;
   char    *pcUrl;
   char    *pcMailComms;

   pcMailComms = safemalloc(1024);
   //
   // MAIL FROM: and search for 250
   // Received smtp.ziggo.nl:
   // Port 25:    250 OK
   // port 587:   530 Je moet eerst inloggen als je poort 587 gebruikt
   //
   if( (pcUrl = smtp_ExtractAddr(pstOpt, 0, MAIL_DT_FROM)) )
   {
      GEN_SNPRINTF(pcMailComms, 1024, "MAIL FROM: <%s>\r\n", pcUrl);
      if(!smtp_SendWait(pstOpt, pstOpt->fSsl, pcMailComms, "250"))
      {
         LOG_Report(errno, "SML", "SMTP_SendMail():MAIL FROM failed");
         PRINTF("SMTP_SendMail():Mail From failed" CRLF);
         safefree(pcMailComms);
         return(FALSE);
      }
   }
   // 
   // RCPT TO: and search for 250
   // Received smtp.ziggo.nl:
   // Port 25:    250 Accepted
   // port 587:   503 sender not yet given
   //
   i=0;
   while( (pcUrl = smtp_ExtractAddr(pstOpt, i++, MAIL_DT_TO)) )
   {
      GEN_SNPRINTF(pcMailComms, 1024, "RCPT TO: <%s>\r\n", pcUrl);
      if(!smtp_SendWait(pstOpt, pstOpt->fSsl, pcMailComms, "250"))
      {
         LOG_Report(errno, "SML", "SMTP_SendMail():%s failed", pcMailComms);
         PRINTF1("SMTP_SendMail():%s failed" CRLF, pcMailComms);
         safefree(pcMailComms);
         return(FALSE);
      }
   }
   //
   i=0;
   while( (pcUrl = smtp_ExtractAddr(pstOpt, i++, MAIL_DT_CC)) )
   {
      GEN_SNPRINTF(pcMailComms, 1024, "RCPT TO: <%s>\r\n", pcUrl);
      if(!smtp_SendWait(pstOpt, pstOpt->fSsl, pcMailComms, "250"))
      {
         LOG_Report(errno, "SML", "SMTP_SendMail():%s failed", pcMailComms);
         PRINTF1("SMTP_SendMail():%s failed" CRLF, pcMailComms);
         safefree(pcMailComms);
         return(FALSE);
      }
   }
   //
   i=0;
   while( (pcUrl = smtp_ExtractAddr(pstOpt, i++, MAIL_DT_BCC)) )
   {
      GEN_SNPRINTF(pcMailComms, 1024, "RCPT TO: <%s>\r\n", pcUrl);
      if(!smtp_SendWait(pstOpt, pstOpt->fSsl, pcMailComms, "250"))
      {
         LOG_Report(errno, "SML", "SMTP_SendMail():%s failed", pcMailComms);
         PRINTF1("SMTP_SendMail():%s failed" CRLF, pcMailComms);
         safefree(pcMailComms);
         return(FALSE);
      }
   }
   //
   // Mail commands send: continue with the mail itself
   //
   safefree(pcMailComms);
   //
   // DATA and search for 354
   // Received smtp.ziggo.nl:
   // Port 25:    354 Enter message, ending with "." on a line by itself
   // port 587:   503-All RCPT commands were rejected with this error:
   //             503-503 sender not yet given
   //             503 Valid RCPT command must precede DATA
   //
   if(!smtp_SendWait(pstOpt, pstOpt->fSsl, "DATA\r\n", "354"))
   {
      LOG_Report(errno, "SML", "SMTP_SendMail():DATA 354 failed");
      PRINTF("SMTP_SendMail():DATA failed" CRLF);
      return(FALSE);
   }
   //
   // send the mail
   //
   if(!smtp_SendWait(pstOpt, pstOpt->fSsl, pstOpt->pcMail, NULL))
   {
      LOG_Report(errno, "SML", "SMTP_SendMail():ERROR:Sending BODY failed");
      PRINTF("SMTP_SendMail():ERROR:Sending BODY failed" CRLF);
      return(FALSE);
   }
   PRINTF("SMTP_SendMail():Sending BODY OKee" CRLF);
   //
   // end the mail with '.' and search for 250
   // Received smtp.ziggo.nl:
   // Port 25:    250 OK id=1eGoxJ-0003ju-TJ
   // port 587:   554 SMTP synchronization error
   //
   if(!smtp_SendWait(pstOpt, pstOpt->fSsl, "\r\n.\r\n", "250"))
   {
      LOG_Report(errno, "SML", "SMTP_SendMail():Unexpected End sequence reply (but continue)");
      PRINTF("SMTP_SendMail():Unexpected End sequence reply (but continue)" CRLF);
   }
   else PRINTF("SMTP_SendMail():Sending End-Of_Mail OKee" CRLF);
   return(TRUE);
}

//
// Function:   SMTP_LogOff
// Purpose:    Log off the smtp mail server
//
// Parms:      MailOpt ptr
// Returns:    TRUE if OKee
// Note:       
//
bool SMTP_LogOff(MAILO *pstOpt)
{
   bool  fCc;

   //
   // Log off the mail server
   //
   // Received smtp.ziggo.nl:
   // Port 25:    221 smtp11.tb.mail.iss.as9143.net closing connection
   // port 587:   
   //
   fCc = smtp_SendWait(pstOpt, pstOpt->fSsl, "QUIT\r\n", "221");

#ifdef   FEATURE_LOG_MAIL_TO_FILE
   safeclose(iFdLog);
   iFdLog = -1;
#endif   //FEATURE_LOG_MAIL_TO_FILE

   return(fCc);
}


/* ======   Local Functions separator ===========================================
___LOCAL_FUNCTIONS(){}
==============================================================================*/

//
// Function:   smtp_NormalLogIn
// Purpose:    Log in the SMTP server using plaintext authentication
//
// Parms:      Mail struct
// Returns:    TRUE if OKee
// Note:       
//
static bool smtp_NormalLogIn(MAILO *pstOpt)
{
   bool  fCc=FALSE;
   char *pcBuffer=NULL;
   char *pcCredit=NULL;
   int   iBlen, iClen=0;

   PRINTF2("smtp_NormalLogIn():U=<%s>-P=<%s>" CRLF, pstOpt->pcUser, pstOpt->pcPass);
   do
   {
      //==============================
      // Allocate buffers:
      //
      pcBuffer = safemalloc(1024);
      //
      GEN_SNPRINTF(pcBuffer, 1024, "EHLO %s\r\n", GLOBAL_GetHostName());
      if(!smtp_SendWait(pstOpt, FALSE, pcBuffer, "AUTH PLAIN"))
      {
         if(pstOpt->tAuth == MAIL_SETUP_SSL_AUTH)
         {
            //
            // PLAIN AUTH LOGIN not allowed now
            //
            LOG_Report(0, "SML", "smtp_NormalLogIn():ERROR: AUTH PLAIN LOGIN not allowed!");
            PRINTF("smtp_NormalLogIn():ERROR: AUTH PLAIN LOGIN not allowed!" CRLF);
            //
            break;
         } 
         else
         {
            //
            // MAIL_SETUP_NO_AUTH:
            // MAIL_SETUP_PLAIN_AUTH:
            // Although the mailserver does not publish the possibility to send mail
            // unencrypted, it will probably do it anyway
            //
            // Assume OKee!
            //
            LOG_Report(0, "SML", "smtp_NormalLogIn():Warning: AUTH PLAIN LOGIN not advertised !");
            PRINTF("smtp_NormalLogIn():Warning: AUTH PLAIN LOGIN not advertised !" CRLF);
         }
      }
      if(pstOpt->pcPass)
      {
         //
         // Received smtp.ziggo.nl:
         // 250 smtp6.mnd.mail.iss.as9143.net Hello 524a88ed.cm-4-3c.dynamic.ziggo.nl [82.74.136.237]
         // 250 SIZE 31457280
         // 250 8BITMIME
         // 250 PIPELINING
         // 250 STARTTLS
         // 250 HELP
         //
         // Build credentials using MIME user+password:
         //    "\0UserName\0Password"
         //
         GEN_MEMSET(pcBuffer, 0x00, 1024);
         GEN_SNPRINTF(&pcBuffer[1], 1023, "%s.%s", pstOpt->pcUser, pstOpt->pcPass);
         pcBuffer[GEN_STRLEN(pstOpt->pcUser)+1] = 0;
         //
         iBlen    = GEN_STRLEN(pstOpt->pcUser) + GEN_STRLEN(pstOpt->pcPass) + 2;
         pcCredit = MIME_Encode64((u_int8 *)pcBuffer, iBlen, NULL, &iClen);
         //
         GEN_SNPRINTF(pcBuffer, 1023, "AUTH PLAIN %s\r\n", pcCredit);
         PRINTF3("smtp_NormalLogIn():Credit Lorig=%d, Lmime=%d:<%s>" CRLF, iBlen, iClen, pcCredit);
         //
         if(!smtp_SendWait(pstOpt, FALSE, pcBuffer, "235")) 
         {
            // Received smtp.ziggo.nl:
            // 503 AUTH command used when not advertised
            //
            if(pstOpt->tAuth == MAIL_SETUP_SSL_AUTH)
            {
               //
               // We explicitly requested secure SSL AUTH, which is not offered here.
               //
               LOG_Report(0, "SML", "smtp_NormalLogIn():ERROR: Authentication failed !");
               PRINTF("smtp_NormalLogIn():ERROR: Authentication failed !" CRLF);
            }
            else
            {
               //
               // NOT all went well: we may have reluctantly logged into the SMTP server. 
               // Continue untill we get dropped out at some other point.
               //
               LOG_Report(0, "SML", "smtp_NormalLogIn():Warning: Authentication unclear ?");
               PRINTF("smtp_NormalLogIn():Warning: Authentication unclear ?" CRLF);
               fCc = TRUE;
            }
         }
         else
         {
            // Received smtp.ziggo.nl:
            // 235 Authentication succeeded
            // All went well: we have successfully logged into the SMTP server
            //
            //LOG_Report(0, "SML", "smtp_NormalLogIn():Authentication successfull!");
            PRINTF("smtp_NormalLogIn():Authentication successfull!" CRLF);
            //
            fCc = TRUE;
         }
      }
      else
      {
            LOG_Report(0, "SML", "smtp_NormalLogIn():ERROR: No password for Authentication !");
            PRINTF("smtp_NormalLogIn():ERROR: No password for Authentication !" CRLF);
      }
   }
   while(0);
   //
   if(pcCredit) safefree(pcCredit);
   if(pcBuffer) safefree(pcBuffer);
   //
   // Free buffers:
   //==============================
   return(fCc);
}

//
// Function:   smtp_SecureLogIn
// Purpose:    Log in the SMTP server using SSL
//
// Parms:      Mail struct
// Returns:    TRUE if OKee
// Note:       
//
static bool smtp_SecureLogIn(MAILO *pstOpt)
{
   bool  fCc=FALSE;
   char *pcBuffer=NULL;
   char *pcCredit=NULL;
   int   iBlen, iClen=0;

   PRINTF2("smtp_SecureLogIn():U=<%s>-P=<%s>" CRLF, pstOpt->pcUser, pstOpt->pcPass);
   do
   {
      //==============================
      // Allocate buffers:
      //
      pcBuffer = safemalloc(1024);
      //
      GEN_SNPRINTF(pcBuffer, 1024, "EHLO %s\r\n", GLOBAL_GetHostName());
      if(!smtp_SendWait(pstOpt, FALSE, pcBuffer, "STARTTLS"))
      {
         LOG_Report(0, "SML", "smtp_SecureLogIn():STARTTLS not supported !");
         PRINTF("smtp_SecureLogIn():STARTTLS not supported !" CRLF);
         break;
      }
      //
      // Received smtp.ziggo.nl:
      //    Sp:250-smtp7.tb.mail.iss.as9143.net Hello 524a88ed.cm-4-3c.dynamic.ziggo.nl [82.74.136.237]
      //    250-SIZE 31457280
      //    250-8BITMIME
      //    250-PIPELINING
      //    250-STARTTLS
      //    250 HELP
      //
      GEN_SNPRINTF(pcBuffer, 1024, "STARTTLS\r\n");
      if(!smtp_SendWait(pstOpt, FALSE, pcBuffer, "220"))
      {
         LOG_Report(0, "SML", "smtp_SecureLogIn():No 220 found on STARTTLS !");
         PRINTF("smtp_SecureLogIn():No 220 found on STARTTLS !" CRLF);
         break;
      }
      //
      // Received smtp.ziggo.nl:
      //    Sp:220 TLS go ahead
      //
      LOG_Report(0, "SML", "smtp_SecureLogIn():Authenticate using STARTTLS");
      PRINTF("smtp_SecureLogIn():Authenticate using STARTTLS" CRLF);
      //
      // Reconnect using SSL
      //
      if(!SSL_RpiConnect(pstOpt) )
      {
         LOG_Report(0, "SML", "smtp_SecureLogIn():ERROR:SSL Connect to %s:%d failed !", pstOpt->pcHost, pstOpt->iPort);
         PRINTF2("smtp_SecureLogIn():ERROR:SSL Connect to %s:%d failed !" CRLF, pstOpt->pcHost, pstOpt->iPort);
         break;
      }
      //
      // Connected to server using SSL 
      //
      GEN_SNPRINTF(pcBuffer, 1024, "EHLO %s\r\n", GLOBAL_GetHostName());
      if(!smtp_SendWait(pstOpt, pstOpt->fSsl, pcBuffer, "PLAIN"))
      {
         LOG_Report(0, "SML", "smtp_SecureLogIn():AUTH PLAIN not supported !");
         PRINTF("smtp_SecureLogIn():AUTH PLAIN not supported !" CRLF);
         break;
      }
      //
      // Received smtp.ziggo.nl:
      //    Se:250-smtp7.tb.mail.iss.as9143.net Hello 524a88ed.cm-4-3c.dynamic.ziggo.nl [82.74.136.237]
      //    250-SIZE 31457280
      //    250-8BITMIME
      //    250-PIPELINING
      //    250-AUTH PLAIN LOGIN
      //    250 HELP
      //
      // Supply login credentials
      //    Ce:AUTH PLAIN AG5lbXNpQHppZ2dvLm5sAFdvcm0wMDBIb2xl
      //
      if(pstOpt->pcPass)
      {
         //
         // AUTH PLAIN encapsulates the user+pass in a single base64 string
         //
         GEN_MEMSET(pcBuffer, 0x00, 1024);
         GEN_SNPRINTF(&pcBuffer[1], 1023, "%s.%s", pstOpt->pcUser, pstOpt->pcPass);
         pcBuffer[GEN_STRLEN(pstOpt->pcUser)+1] = 0;
         //
         iBlen    = GEN_STRLEN(pstOpt->pcUser) + GEN_STRLEN(pstOpt->pcPass) + 2;
         pcCredit = MIME_Encode64((u_int8 *)pcBuffer, iBlen, NULL, &iClen);
         //
         GEN_SNPRINTF(pcBuffer, 1023, "AUTH PLAIN %s\r\n", pcCredit);
         PRINTF3("smtp_SecureLogIn():Credit Lorig=%d, Lmime=%d : <%s>" CRLF, iBlen, iClen, pcCredit);
         //
         if(!smtp_SendWait(pstOpt, pstOpt->fSsl, pcBuffer, "235")) 
         {
            // Received smtp.ziggo.nl:
            //
            LOG_Report(0, "SML", "smtp_SecureLogIn():ERROR: Authentication failed !");
            PRINTF("smtp_SecureLogIn():ERROR: Authentication failed !" CRLF);
         }
         else
         {
            // Received smtp.ziggo.nl:
            //    Se:235 Authentication succeeded
            //
            //LOG_Report(0, "SML", "smtp_SecureLogIn():Authentication succeeded!");
            PRINTF("smtp_SecureLogIn():Authentication succeeded!" CRLF);
            //
            fCc = TRUE;
         }
      }
      else
      {
            LOG_Report(0, "SML", "smtp_SecureLogIn():Authentication error!");
            PRINTF("smtp_SecureLogIn():Authentication error!" CRLF);
      }
   }
   while(0);
   //
   if(pcCredit) safefree(pcCredit);
   if(pcBuffer) safefree(pcBuffer);
   //
   // Free buffers:
   //==============================
   return(fCc);
}

//
// Function:   smtp_ExtractAddr
// Purpose:    Extract the URL from the OPT struct for a data-type
//
// Parms:      MAILO *, URL Index, data-type 
// Returns:    URL ptr -> "aap.noot@mies.nl" or NULL
// Note:       MAILO struct contains bracketted URL -> "<aap.noot@mies.nl>"
//
static char *smtp_ExtractAddr(MAILO *pstOpt, int iIdx, MAILDT tDt)
{
   int      i;
   char    *pcRet=NULL;
   MAILL   *pstList=pstOpt->pstList;

   while(pstList)
   {
      if(pstList->tDataType == tDt)
      {
         if(iIdx ==0)
         {
            if((pcRet = index(pstList->pcData, '<')) == NULL) 
            {
               pcRet = pstList->pcData;
               break;
            }
            else
            {
               pcRet++;
               for(i = 0; i < GEN_STRLEN(pcRet); i++)
               {
                  if(pcRet[i] == '>')
                  {
                     pcRet[i] = '\0';
                     break;
                  }
               }
            }
         }
         else iIdx--;
      }
      pstList = pstList->pstNext;
   }
   return(pcRet);
}

//
// Function:   smtp_SendWait
// Purpose:    Send out mail data to socket and wait for reply
//
// Parms:      MailOpt ptr, fSSL, data to send, reply to wait for
// Returns:    TRUE if OKee
// Note:       
//
static bool smtp_SendWait(MAILO *pstOpt, bool fSsl, char *pcSend, const char *pcSearch)
{
#ifdef   FEATURE_TEST_SEND_MAIL
   bool  fCc=TRUE;

   //
   // Just show what data we were about to send to the mailserver
   //
   if(pcSend)     LOG_printf("smtp_SendWait():FAKE: Send %s" CRLF, pcSend);
   else           LOG_printf("smtp_SendWait():FAKE: Send NOTHING" CRLF);
   if(pcSearch)   LOG_printf("smtp_SendWait():FAKE: Wait %s" CRLF, pcSearch);
   else           LOG_printf("smtp_SendWait():FAKE: NO WAIT" CRLF);   

#else    //FEATURE_TEST_SEND_MAIL

   bool  fCc=FALSE;
   int   iNrWr, iNr;
   char *pcBuffer;

   //
   // If we have something to send to the server:
   //
   if(pcSend)
   {           
      //
      // send the data
      //
      //PRINTF1("smtp_SendWait():Send:" CRLF "%s" CRLF, pcSend);
      iNr = GEN_STRLEN(pcSend);
      PRINTF1("smtp_SendWait():Send %d bytes" CRLF, iNr);
      //
      if(fSsl) 
      {
         LOG_MAIL("Ce:", pcSend, iNr);
         iNrWr = SSL_RpiWrite(pstOpt, pcSend, iNr);
      }
      else
      {
         LOG_MAIL("Cp:", pcSend, iNr);
         iNrWr = NET_Write(pstOpt->iSocket, pcSend, iNr);
      }
      //
      if(iNrWr != iNr) 
      {
         PRINTF2("smtp_SendWait(): ERROR written %d instead of %d" CRLF, iNrWr, iNr);
      }
   }
   //
   // If we expect something in return to verify :
   //
   if(pcSearch)
   {
      //
      // read the server reply
      //
      pcBuffer = safemalloc(1024);
      //
      if(fSsl) 
      {
         PRINTF("smtp_SendWait():Going to read the encrypted reply...." CRLF);
         iNr = SSL_RpiRead(pstOpt, pcBuffer, 1000);
         pcBuffer[iNr] = '\0';
         PRINTF2("smtp_SendWait():Read %d bytes: %s" CRLF, iNr, pcBuffer);
         LOG_MAIL("Se:", pcBuffer, iNr);
      }
      else
      {
         PRINTF("smtp_SendWait():Going to read the plaintext reply...." CRLF);
         iNr = NET_Read(pstOpt->iSocket, pcBuffer, 1000);
         pcBuffer[iNr] = '\0';
         PRINTF2("smtp_SendWait():Read %d bytes: %s" CRLF, iNr, pcBuffer);
         LOG_MAIL("Sp:", pcBuffer, iNr);
      }
      PRINTF1("smtp_SendWait():Need:%s" CRLF, pcSearch);
      PRINTF1("smtp_SendWait():Received:" CRLF "%s" CRLF, pcBuffer);
      //
      if(GEN_STRSTRI(pcBuffer, pcSearch)) 
      {
         PRINTF("smtp_SendWait():Match" CRLF);
         fCc = TRUE;
      }
      else
      {
         PRINTF("smtp_SendWait():NO Match" CRLF);
      }
      safefree(pcBuffer);
   }
   else fCc = TRUE;
#endif   //FEATURE_TEST_SEND_MAIL

   return(fCc);
}

#ifdef   FEATURE_LOG_MAIL_TO_FILE
//
// Function:   smtp_WriteComms
// Purpose:    Log data to/from mail server
//
// Parms:      Id, Data, size
// Returns:    
// Note:       Log: C: caller
//                  S: server reply
//
static void smtp_WriteComms(const char *pcId, char *pcData, int iLen)
{
   switch(iFdLog)
   {
      case 0:
         break;

      case -1:
         if( (iFdLog = safeopen(RPI_MAIL_PATH, O_RDWR|O_CREAT|O_APPEND)) == -1)
         {
            iFdLog = 0;
            LOG_Report(errno, "EML", "ERROR: Open mail log");
            return;
         }
         //
         // Fallthrough
         //
      default:
         safewrite(iFdLog, pcId, GEN_STRLEN(pcId));
         safewrite(iFdLog, pcData, iLen);
         safewrite(iFdLog, "\r\n", 2);
         break;
   }
}
#endif   //FEATURE_LOG_MAIL_TO_FILE


/* ======   Local Functions separator ===========================================
___CLIENT_SERVER_LOG(){}
==============================================================================*/

//    Sp:220-smtp7.tb.mail.iss.as9143.net ESMTP Mon, 04 Dec 2017 11:08:20 +0100
//    220-Sending spam or unsolicited commercial e-mail to this server is strictly
//    220-prohibited by our NO UBE / NO UCE policy. Abuse will be prosecuted and/or
//    220 charged per attempted recipient at international postal rates.
//    
//    Cp:EHLO PatrnRpi10
//    
//    Sp:250-smtp7.tb.mail.iss.as9143.net Hello 524a88ed.cm-4-3c.dynamic.ziggo.nl [82.74.136.237]
//    250-SIZE 31457280
//    250-8BITMIME
//    250-PIPELINING
//    250-STARTTLS
//    250 HELP
//    
//    Cp:STARTTLS
//    
//    Sp:220 TLS go ahead
//    
//    Ce:EHLO PatrnRpi10
//    
//    Se:250-smtp7.tb.mail.iss.as9143.net Hello 524a88ed.cm-4-3c.dynamic.ziggo.nl [82.74.136.237]
//    250-SIZE 31457280
//    250-8BITMIME
//    250-PIPELINING
//    250-AUTH PLAIN LOGIN
//    250 HELP
//    
//    Ce:AUTH PLAIN AG5lbXNpQHppZ2dvLm5sAFdvcm0wMDBIb2xl
//    
//    Se:235 Authentication succeeded
//    
//    Ce:MAIL FROM: nemsi@ziggo.nl
//    
//    Se:250 OK
//    
//    Ce:RCPT TO: peter.hillen@ziggo.nl
//    
//    Se:250 Accepted
//    
//    Ce:DATA
//    
//    Se:354 Enter message, ending with "." on a line by itself
//    
//    Ce:From: nemsi@ziggo.nl
//    To: peter.hillen@ziggo.nl
//    Subject: Report from Ma|04-12-2017|11:08:15
//    MIME-Version: 1.0
//    Content-Type: multipart/mixed;
//    boundary="=_1512385701_="
//    
//    --=_1512385701_=
//    Content-Type: text/plain; charset="ISO-8859-1"
//    Content-Transfer-Encoding: base64
//    Content-Disposition: inline
//    
//    SGkgQWxsLAoKRm90byBkZXRlY3Rpb24gYW5kIFNtYXJ0LUUgcmVzdWx0cy4KCgoKLVBldGV
//    yLQo=
//    
//    --=_1512385701_=
//    Content-Type: application/octed-stream; name="info.txt"
//    Content-Transfer-Encoding: base64
//    Content-Disposition: attachment; filename="info.txt"
//    
//    W0RvfDAzLTA4LTIwMTd8MTA6MDU6MjZdIFZFQ19Xcml0ZU1vdGlvblZlY3RvcnMoKToKICA
//    gICAgICAgICAgICAgICAgICAgIEF2Zy1CZ25kLVNBRD0gICAgMCBNYXgtQmduZC1TQUQ9IC
//    AgIDAKICAgTWluLUZnbmQtU0FEPSAgICAwIEF2Zy1GZ25kLVNBRD0gICAgMCBNYXgtRmduZ
//    C1TQUQ9ICAgIDAKICAgSG9yLVZlY3RvcnMgPSAgMjA2IFZlci1WZWN0b3JzID0gIDE1NAog
//    ICBDYWwtVmVjdG9ycyA9ICAgIDAgVG90LVZlY3RvcnMgPTMxNzI0CiAgIFh5LUNvdW50ICA
//    gID0gICAgMCBYeS1XZWlnaHQgICA9ICAgIDAKCgo=
//    
//    
//    --=_1512385701_=--
//    
//    Ce:
//    .
//    
//    Se:550-Message refused by spam filter (node=smtp7.tb.mail.iss.as9143.net
//    
//    Ce:QUIT
//    
//    Se:550 msg-id=1eLnfh-0002UE-Kr)
//