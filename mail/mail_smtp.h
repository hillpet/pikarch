/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           mail_smtp.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi mail handler header file
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _MAIL_SMTP_H_
#define _MAIL_SMTP_H_

bool SMTP_LogIn            (MAILO *);
bool SMTP_SendMail         (MAILO *);
bool SMTP_LogOff           (MAILO *);

#endif /* _MAIL_SMTP_H_ */
