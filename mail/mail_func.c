/*  (c) Copyright:  2017  Patrn ESS, Confidential Data
 *
 *  Workfile:           cmd_mail.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi mail handler
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <libgen.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include <common.h>
#include "../config.h"
#include "../globals.h"
#include "mail_func.h"
#include "mail_ssl.h"
#include "mail_smtp.h"
#include "mail_pop3.h"

//#define USE_PRINTF
#include <printf.h>

//
// Debug sleep msecs
//
#define PSL    1000
//
typedef struct exttype
{
   const char *pcExt;
   const char *pcType;
   const char *(*pfExt)(MAILO *, const struct exttype *);
}  EXTTYPE;
//
typedef const char * (*PFEXT)(MAILO *, const EXTTYPE *);
//
// Static functions
//
static void          mail_Free                  (MAILO *);
static char         *mail_ReadResultsFromFile   (char *, MAILCT, int *);
//
static char         *mail_CreateMimeBody        (MAILO *, int*);
static void          mail_AddListEntries        (MAILO *, MAILDT, const char *, const char *);
static char         *mail_EncodeAttachment      (char *, int *);
static const char   *mail_FileExtension         (MAILO *, char *);
static int           mail_GetListSize           (MAILO *, MAILDT);
//
// File extension handlers
//
static const char   *mail_HandleExtTypeGeneric  (MAILO *, const EXTTYPE *);
//
// Multipart/mixed MIME header to start any body
//
static const char *pcMimeMultiHdr    = "MIME-Version: 1.0\r\n"
                                       "Content-Type: multipart/mixed; "
                                       "boundary=\"=_%d_=\"\r\n\r\n";
//
// HTML markup for embedded pictures, needs 2 format parameters:
//    %s: html extension (i.e. "image/jpeg")
//    %s: base64 coded picture  
//
static const char *pcPlainBodyPicture= "<html>\r\n"
                                       "<head>\r\n"
                                       "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n"
                                       "<title>PATRN</title>\r\n"
                                       "</head>\r\n"
                                       "<body style=\"text-align:center\">\r\n"
                                       "<h2>PATRN SpiCam HTTP Server</h2>\r\n"
                                       "<br/>\r\n"
                                       "<br/>Snapshot:\r\n"
                                       "<br/>\r\n"
                                       "<br/>\r\n"
                                       "<br/>\r\n"
                                       "<img alt=\"Snapshot\" width=\"800\" height=\"450\" src=\"data:%s;base64,\r\n"
                                       "%s\"/>\r\n"
                                       "<br/>\r\n"
                                       "<br/>\r\n"
                                       "</body>\r\n"
                                       "</html>\r\n";
//
static const char *pcPlainBodyHtml    = "--=_%d_=\r\n"
                                       "Content-Type: text/html; charset=\"us-ascii\"\r\n"
                                       "Content-Transfer-Encoding: 7bit\r\n"
                                       "Content-Disposition: inline\r\n\r\n";
//
//
static const char *pcMimeBodyText    = "--=_%d_=\r\n"
                                       "Content-Type: text/plain; charset=\"us-ascii\"\r\n"
                                       "Content-Transfer-Encoding: base64\r\n"
                                       "Content-Disposition: inline\r\n\r\n";
//
static const char *pcMimeBodyHtml    = "--=_%d_=\r\n"
                                       "Content-Type: text/html; charset=\"us-ascii\"\r\n"
                                       "Content-Transfer-Encoding: base64\r\n"
                                       "Content-Disposition: inline\r\n\r\n";
//
static const char *pcMimeBodyFile    = "--=_%d_=\r\n"
                                       "Content-Type: %s\r\n"
                                       "Content-Transfer-Encoding: base64\r\n"
                                       "Content-Disposition: inline\r\n\r\n";
//
static const char *pcMimeAttmFile    = "--=_%d_=\r\n"
                                       "Content-Type: %s; name=\"%s\"\r\n"
                                       "Content-Transfer-Encoding: base64\r\n"
                                       "Content-Disposition: attachment; filename=\"%s\"\r\n\r\n";
//
static const char pcContentTypeGif[]  = "image/gif";
static const char pcContentTypeJpg[]  = "image/jpeg";
static const char pcContentTypeBmp[]  = "image/bmp";
static const char pcContentTypePng[]  = "image/png";
static const char pcContentTypeMp3[]  = "audio/mp3";
static const char pcContentTypeMp4[]  = "video/mp4";
static const char pcContentTypeH264[] = "video/h264";
static const char pcContentTypeTxt[]  = "application/doc";
static const char pcContentTypePdf[]  = "application/pdf";
static const char pcContentTypeZip[]  = "application/x-zip-compressed";

static const EXTTYPE stExtensionTypes[] =
{
//   pcExt           pcType              pfExt
   { "gif",          pcContentTypeGif,   mail_HandleExtTypeGeneric },
   { "jpg",          pcContentTypeJpg,   mail_HandleExtTypeGeneric },
   { "bmp",          pcContentTypeBmp,   mail_HandleExtTypeGeneric },
   { "png",          pcContentTypePng,   mail_HandleExtTypeGeneric },
   { "mp3",          pcContentTypeMp3,   mail_HandleExtTypeGeneric },
   { "mp4",          pcContentTypeMp4,   mail_HandleExtTypeGeneric },
   { "h264",         pcContentTypeH264,  mail_HandleExtTypeGeneric },
   { "txt",          pcContentTypeTxt,   mail_HandleExtTypeGeneric },
   { "pdf",          pcContentTypePdf,   mail_HandleExtTypeGeneric },
   { "zip",          pcContentTypeZip,   mail_HandleExtTypeGeneric }
};
static const int iNumExtTypes = sizeof(stExtensionTypes)/sizeof(EXTTYPE);
//
static const char *pcMailHeadFrom   =  "From: <";
static const char *pcMailHeadReply  =  "Reply-To: <";
static const char *pcMailHeadTo     =  "To: <";
static const char *pcMailHeadCc     =  "Cc: <";
static const char *pcMailHeadBcc    =  "Bcc: <";
static const char *pcMailHeadSubj   =  "Subject:";
static const char *pcMailHeadDate   =  "Date:";
static const char *pcMailHeadId     =  "Message-ID: <0031$";
static const char *pcMailComma      =  ",";
static const char *pcMailTail       =  ">\r\n";
static const char *pcMailCrLf       =  "\r\n";
static const char *pcMailCrLf2      =  "\r\n\r\n";
//
// Function:   MAIL_Init
// Purpose:    Init the mail forewarder
// Parms:      Setup for authentication 
//
// Returns:    Mail options
// Note:       Mail option stay with us until mail send. Then it's memory
//             is freed
//
MAILO *MAIL_Init(ATYPE tAuth)
{
   MAILO   *pstOpt;

   pstOpt = safemalloc(sizeof(MAILO));
   GEN_MEMSET(pstOpt, 0, sizeof(MAILO));
   //
   pstOpt->ulSecs = RTC_GetSystemSecs();
   //
   // Setup for plaintext authentication:
   // ===================================
   // Plain Setting          SMTP    POP3  
   // YES:  Server port  --> 25      110
   //       Ssl          --> No       No
   // NO:   Server port  --> 587     995
   //       Ssl          --> Yes     Yes
   //
   switch(tAuth)
   {
      case MAIL_SETUP_NO_AUTH:
         PRINTF("MAIL-Init():NO authentication" CRLF);
         // Setup default if none available
         if(GEN_STRLEN(pstMap->G_pcPop3Port) == 0) GEN_SNPRINTF(pstMap->G_pcPop3Port, MAX_PARM_LEN, "%d", MAIL_POP3_PORT_PLAIN);
         if(GEN_STRLEN(pstMap->G_pcSmtpPort) == 0) GEN_SNPRINTF(pstMap->G_pcSmtpPort, MAX_PARM_LEN, "%d", MAIL_SMTP_PORT_PLAIN);
         pstOpt->fSsl = FALSE;
         break;
   
      case MAIL_SETUP_PLAIN_AUTH:
         PRINTF("MAIL-Init():Plaintext authentication" CRLF);
         // Setup default if none available
         if(GEN_STRLEN(pstMap->G_pcPop3Port) == 0) GEN_SNPRINTF(pstMap->G_pcPop3Port, MAX_PARM_LEN, "%d", MAIL_POP3_PORT_PLAIN);
         if(GEN_STRLEN(pstMap->G_pcSmtpPort) == 0) GEN_SNPRINTF(pstMap->G_pcSmtpPort, MAX_PARM_LEN, "%d", MAIL_SMTP_PORT_PLAIN);
         pstOpt->fSsl = FALSE;
         break;
   
      default:
      case MAIL_SETUP_SSL_AUTH:
         PRINTF("MAIL-Init():SSL authentication" CRLF);
         // Setup default if none available
         if(GEN_STRLEN(pstMap->G_pcPop3Port) == 0) GEN_SNPRINTF(pstMap->G_pcPop3Port, MAX_PARM_LEN, "%d", MAIL_POP3_PORT_SSL);
         if(GEN_STRLEN(pstMap->G_pcSmtpPort) == 0) GEN_SNPRINTF(pstMap->G_pcSmtpPort, MAX_PARM_LEN, "%d", MAIL_SMTP_PORT_SSL);
         if(!(pstOpt->fSsl = SSL_RpiInit(pstOpt)) )
         {
            // SSL/TLS setup failed: switch to plaintext
            PRINTF("MAIL-Init():SSL authentication FAILED: use plaintext" CRLF);
            tAuth = MAIL_SETUP_PLAIN_AUTH;
         }
         break;
   }
   pstOpt->iSocket = BAD_SOCKET;
   pstOpt->tAuth   = tAuth;
   return(pstOpt);
}

//
//  Function:   MAIL_Cleanup
//  Purpose:    Cleanup
//  Parms:      MailOpt^
//
//  Returns:    TRUE if OKee
//
MAILO *MAIL_Cleanup(MAILO *pstOpt)
{
   //
   // disconnect if not SSL connected
   //
   if(pstOpt->fSsl) SSL_RpiClose(pstOpt);
   //
   if(pstOpt->iSocket != BAD_SOCKET)
   {
      NET_ClientDisconnect(pstOpt->iSocket);
   }
   //
   // Free all memory
   //
   mail_Free(pstOpt);
   return(NULL);
}

//
//  Function:   MAIL_Login
//  Purpose:    Login the mail server
//  Parms:      MailOpt^
//
//  Returns:    TRUE if OKee
//
bool MAIL_Login(MAILO *pstOpt)
{
   bool  fCc=FALSE;

   switch(pstOpt->tType)
   {
      default:
         break;

      case MAIL_POP3:
         PRINTF("MAIL-Login():Pop3" CRLF);
         fCc = POP3_LogIn(pstOpt);
         break;

      case MAIL_SMTP:
         PRINTF("MAIL-Login():Smtp" CRLF);
         fCc = SMTP_LogIn(pstOpt);
         break;
   }
   return(fCc);
}

//
//  Function:   MAIL_Logout
//  Purpose:    Logout the POP3 mailaccount
//  Parms:      MailOpt^
//
//  Returns:    void
//
void MAIL_Logout(MAILO *pstOpt)
{
   switch(pstOpt->tType)
   {
      default:
         PRINTF("MAIL-Logout():None" CRLF);
         break;

      case MAIL_POP3:
         POP3_LogOff(pstOpt);
         PRINTF("MAIL-Logout():Pop3" CRLF);
         break;

      case MAIL_SMTP:
         SMTP_LogOff(pstOpt);
         PRINTF("MAIL-Logout():Smtp" CRLF);
         break;
   }
}

//
//  Function:   MAIL_DeleteMail
//  Purpose:    Delete a mail message
//  Parms:      MAILO struct, MsgNr
//
//  Returns:    True if OKee
//
bool MAIL_DeleteMail(MAILO *pstOpt, int iMsgNr)
{
   bool  fCc=FALSE;

   PRINTF1("MAIL-DeleteMail():Delete Nr-%d" CRLF, iMsgNr);
   //
   if(!pstOpt->fSsl)
   {
      fCc = POP3_DeleteMessage(pstOpt, iMsgNr);
   }
   return(fCc);
}

//
//  Name:         MAIL_RetrieveAccount
//  Description:  Retrieve the account info from the POP3 server
//  Arguments:    MAILO struct
//
//  Returns:      Number of messages in the account or -1 on error
//
int MAIL_RetrieveAccount(MAILO *pstOpt)
{
   int   iCount=0;
  
   if(!pstOpt->fSsl)
   {
      iCount = POP3_RetrieveAccount(pstOpt);
   }
   return(iCount);
}

//
//  Name:         MAIL_RetrieveHeader
//  Description:  Retrieve a mailheader plus some lines from the body from the POP3 server
//  Arguments:    MAILO struct, Msg#, number of lines, Callback, DataPtr
//
//  Returns:      
//
bool MAIL_RetrieveHeader(MAILO *pstOpt, int iMsgNr, int iLines, MAILRCB pfnStore, void *pvData)
{
   bool  fCc=FALSE;

   if(!pstOpt->fSsl)
   {
      fCc = POP3_RetrieveHeader(pstOpt, iMsgNr, iLines, pfnStore, pvData);
   }
   return(fCc);
}

//
//  Name:         MAIL_ReceiveMail
//  Description:  Read the mail
//  Arguments:    MAILO struct, message number 1..?, Callback, DataPtr
//
//  Returns:      TRUE if OKee received
//
bool MAIL_ReceiveMail(MAILO *pstOpt, int iMsgNr, MAILRCB pfnStore, void *pvData)
{
   bool  fCc=FALSE;

   if(!pstOpt->fSsl)
   {
      fCc = POP3_ReceiveMail(pstOpt, iMsgNr, pfnStore, pvData);
   }
   return(fCc);
}

//
//  Function:   MAIL_SetMailServer
//  Purpose:    Set the mailserver who will be handling the SMTP xfer
//  Parms:      MailOpt, POP3/SMTP, Servername, port, user, password
//
//  Returns:    TRUE if OKee
//
bool MAIL_SetMailServer(MAILO *pstOpt, MAILT tType, char *pcSrvr, int iPort, char *pcUser, char *pcPass)
{
   bool     fCc=FALSE;
   int      iLen;

   if(pstOpt)
   {
      pstOpt->tType  = tType;
      pstOpt->iPort  = iPort;
      //
      iLen = GEN_STRLEN(pcSrvr);
      pstOpt->pcHost = GEN_Remalloc(pstOpt->pcHost, iLen);
      GEN_STRCPY(pstOpt->pcHost, pcSrvr);
      PRINTF2("SetMailServer():Host:%s:%d" CRLF, pstOpt->pcHost, pstOpt->iPort);
      //
      // User/Psw only if AUTH effective
      //
      if(pcUser)
      {
         iLen = GEN_STRLEN(pcUser);
         pstOpt->pcUser = GEN_Remalloc(pstOpt->pcUser, iLen);
         GEN_STRCPY(pstOpt->pcUser, pcUser);
         PRINTF1("SetMailServer():User:%s" CRLF, pstOpt->pcUser);
      }
      else
      {
         pstOpt->pcUser = NULL;
         PRINTF("SetMailServer():NO User" CRLF);
      }
      if(pcPass)
      {
         iLen = GEN_STRLEN(pcPass);
         pstOpt->pcPass = GEN_Remalloc(pstOpt->pcPass, iLen);
         GEN_STRCPY(pstOpt->pcPass, pcPass);
         PRINTF1("SetMailServer():Pass:%s" CRLF, pstOpt->pcPass);
      }
      else
      {
         pstOpt->pcPass = NULL;
         PRINTF("SetMailServer():NO Pass" CRLF);
      }
      fCc = TRUE;
   }
   return(fCc);
}

//
//  Function:   MAIL_SendMail
//  Purpose:    
//
//  Parms:      MailOpt 
//  Returns:    Total mail length
//
void MAIL_SendMail(MAILO *pstOpt)
{
   PRINTF("MAIL-SendMail()" CRLF);
   //
   mail_AddListEntries(pstOpt, MAIL_DT_FROM,    pcMailHeadFrom,  pcMailTail);
   mail_AddListEntries(pstOpt, MAIL_DT_REPLY,   pcMailHeadReply, pcMailTail);
   mail_AddListEntries(pstOpt, MAIL_DT_TO,      pcMailHeadTo,    pcMailTail);
   mail_AddListEntries(pstOpt, MAIL_DT_CC,      pcMailHeadCc,    pcMailTail);
   mail_AddListEntries(pstOpt, MAIL_DT_BCC,     pcMailHeadBcc,   pcMailTail);
   //
   mail_AddListEntries(pstOpt, MAIL_DT_SUBJECT, pcMailHeadSubj,  pcMailCrLf);
   //
   // Add SHOULD HAVE fixed headers:
   //    Date
   //    Message-ID
   // Add buffer space for it as well !
   //
   mail_AddListEntries(pstOpt, MAIL_DT_DATE,    pcMailHeadDate,  pcMailCrLf);
   mail_AddListEntries(pstOpt, MAIL_DT_ID,      pcMailHeadId,    pcMailTail);

   //
   // Add mail-bodies and attachments:
   //    "MIME-Version: 1.0\r\n"
   //    "Content-Type: multipart/mixed; "
   //    "boundary=\"=_%d_=\"\r\n\r\n";
   //
   mail_AddListEntries(pstOpt, MAIL_DT_BODY,    NULL,            pcMailCrLf);
   mail_AddListEntries(pstOpt, MAIL_DT_ATTM,    NULL,            pcMailCrLf);
   //
   PRINTF3("MAIL-SendMail(%d):Size=%d:\n\n%s" CRLF, GEN_Sleep(PSL), pstOpt->iMailSize, pstOpt->pcMail);
   //
   PRINTF1("MAIL-SendMail(%d):Done: SMTP_SendMail() now:" CRLF, GEN_Sleep(PSL));
   SMTP_SendMail(pstOpt);
}

/* ======   Local Functions separator ===========================================
___SMTP_MAIL_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   mail_AddListEntries
//  Purpose:    Add list entries to the mail 
//
//  Parms:      Mail^, Data Type, Head, Tail
//  Returns:    void
//
static void mail_AddListEntries(MAILO *pstOpt, MAILDT tDt, const char *pcHead, const char *pcTail)
{
   bool     fNeedComma=FALSE;
   int      iSize;
   int      iMailSize=pstOpt->iMailSize;
   char    *pcBuffer;
   char    *pcMail=pstOpt->pcMail;
   MAILL   *pstList=pstOpt->pstList;

   //PRINTF("mail-AddListEntries()" CRLF);
   switch(tDt)
   {
      default:
      case MAIL_DT_ATTM:
         // Attachments are base64 encoded and mixed with the mail body !
         break;

      case MAIL_DT_FROM:
      case MAIL_DT_REPLY:
      case MAIL_DT_TO:
      case MAIL_DT_CC:
      case MAIL_DT_BCC:
         iSize  = mail_GetListSize(pstOpt, tDt);
         //PRINTF2("mail-AddListEntries():DT=%d:Add %d bytes" CRLF, tDt, iSize);
         if(iSize)
         {
            iMailSize += iSize + GEN_STRLEN(pcHead) + GEN_STRLEN(pcTail);
            pcMail     = GEN_Remalloc(pcMail, iMailSize);
            GEN_STRCAT(pcMail, pcHead);
            while(pstList)
            {
               if(pstList->tDataType == tDt)
               {
                  //
                  // Add this datatype to the email buffer
                  //
                  if(fNeedComma) GEN_STRCAT(pcMail, pcMailComma);
                  fNeedComma = TRUE;
                  GEN_STRCAT(pcMail, (char *)pstList->pcData);
               }
               pstList = pstList->pstNext;
            }
            GEN_STRCAT(pcMail, pcTail);
         }
         break;

      case MAIL_DT_SUBJECT:
         iSize = mail_GetListSize(pstOpt, tDt);
         if(iSize)
         {
            while(pstList)
            {
               if(pstList->tDataType == tDt)
               {
                  PRINTF1("mail-AddListEntries():SUBJECT:Add %d bytes" CRLF, iSize);
                  //
                  // Add this datatype to the email buffer
                  //
                  iMailSize += iSize + GEN_STRLEN(pcHead) + GEN_STRLEN(pcTail);
                  pcMail     = GEN_Remalloc(pcMail, iMailSize);
                  GEN_STRCAT(pcMail, pcHead);
                  GEN_STRCAT(pcMail, (char *)pstList->pcData);
                  GEN_STRCAT(pcMail, pcTail);
                  break;
               }
               pstList = pstList->pstNext;
            }
         }
         break;

      case MAIL_DT_DATE:
         iSize    = RTC_ConvertDateTimeSize(TIME_FORMAT_MAIL) + GEN_STRLEN(pcHead) + GEN_STRLEN(pcTail);
         pcBuffer = GEN_Remalloc(NULL, iSize);
         RTC_ConvertDateTime(TIME_FORMAT_MAIL, pcBuffer, pstOpt->ulSecs);
         //
         PRINTF1("mail-AddListEntries():DATE:Add %d bytes" CRLF, iSize);
         iMailSize += iSize;
         pcMail     = GEN_Remalloc(pcMail, iMailSize);
         GEN_STRCAT(pcMail, pcHead);
         GEN_STRCAT(pcMail, pcBuffer);
         GEN_STRCAT(pcMail, pcTail);
         safefree(pcBuffer);
         break;

      case MAIL_DT_ID:
         iSize    = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS) + GEN_STRLEN(pcHead) + GEN_STRLEN(pcTail);
         pcBuffer = GEN_Remalloc(NULL, iSize);
         RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcBuffer, pstOpt->ulSecs);
         //
         PRINTF1("mail-AddListEntries():ID:Add %d bytes" CRLF, iSize);
         iMailSize += iSize;
         pcMail     = GEN_Remalloc(pcMail, iMailSize);
         GEN_STRCAT(pcMail, pcHead);
         GEN_STRCAT(pcMail, pcBuffer);
         GEN_STRCAT(pcMail, pcTail);
         safefree(pcBuffer);
         break;

      case MAIL_DT_BODY:
         pcBuffer = mail_CreateMimeBody(pstOpt, &iSize);
         PRINTF1("mail-AddListEntries():BODY:Add %d bytes MIME data" CRLF, iSize);
         iMailSize += iSize;
         pcMail     = GEN_Remalloc(pcMail, iMailSize);
         GEN_STRCAT(pcMail, pcBuffer);
         safefree(pcBuffer);
         break;
   }
   //PRINTF3("mail-AddListEntries(%d):Size=%d:\n%s" CRLF, GEN_Sleep(PSL), iMailSize, pcMail);
   pstOpt->pcMail    = pcMail;
   pstOpt->iMailSize = iMailSize;
}

//
// Function:   mail_CreateMimeBody
// Purpose:    Base64-encode mail bodies and attachments
//
// Parms:      MailOpt, size ptr
// Returns:    MIME data ptr
// Note:       MIME headers:
//
// HDR         MIME-Version: 1.0
//             Content-Type: text/plain or 
//                           text/html  or
//                           image/jpeg or
//                           audio/mp3  or
//                           video/mp4  or
//                           multipart/mixed;boundary="=_timestamp_="
//
// BODY        --=_timestamp_=
//             Content-Type:text/plain; charset=us-ascii
//             Content-Transfer-Encoding:quoted-printable (or 7bit, base64,....)
//             Content-Disposition:inline
//             
//             <message_body> (can be base64 encoded)
//
// For all attachments:
//
// FILE        --=_timestamp_=
//             Content-Type:application/octet-stream;name="picture.jpg"
//             Content-Transfer-Encoding:base64
//             Content-Disposition:attachment;filename="picture.jpg"
//
//             <base64 data>
//
//             --=_timestamp_=--
//
//
static char *mail_CreateMimeBody(MAILO *pstOpt, int *piSize)
{
   int         iSize, iBodySize, iMimeSize;
   char       *pcTemp;
   char       *pcBody;
   char       *pcMime;
   char       *pcFile;
   const char *pcExt;
   char       *pcFileName;
   MAILL      *pstList;

   PRINTF1("mail-CreateMimeBody(%d)" CRLF, GEN_Sleep(PSL));
   //==========================================================================
   // pcBody    : Future complete body plus attachments
   // iBodySize : Size of it
   //==========================================================================
   // (1)
   // The mime-boundary will be the secs timestamp
   // Create the ContType_Message_Header
   //
   pcTemp = safemalloc(1024);
   GEN_SNPRINTF(pcTemp, 1024, pcMimeMultiHdr, pstOpt->ulSecs);
   //
   // Allocate body memory
   //
   iBodySize = GEN_STRLEN(pcTemp);
   pcBody    = GEN_Remalloc(NULL, iBodySize);
   GEN_STRCPY(pcBody, pcTemp);
   PRINTF1("mail-CreateMimeBody():Hdr Size=%d" CRLF, iBodySize);
   //
   // (2) MAIL_DT_BODY
   // Handle all mail bodies (text, html, pictures) as inline base64 encoded
   //
   //    MAIL_CT_TEXT:  Just add the data to the mix.
   //    MAIL_CT_HTML:  Drop the HTTP headers to obtain the HTML part.
   //    MAIL_CT_PICT:  Drop the HTTP headers to obtain the HTML part and 
   //                   add the HTML code to embed the picture.
   //    MAIL_CT_FILE:  Add the data from file.
   //    MAIL_CT_ATTM:  Add the data as attachment.
   //
   pstList = pstOpt->pstList;
   while(pstList)
   {
      if(pstList->tDataType == MAIL_DT_BODY)
      {
         //
         // Create the content header for this mail BODY and base64 encode it
         //
         switch(pstList->tContType)
         {
            default:
            case MAIL_CT_TEXT:
               PRINTF1("mail-CreateMimeBody():(2):CT-Text-S:Size=%d bytes" CRLF, iBodySize); 
               //
               // Generic text body: put it into the multipart/mixed container
               //
               GEN_SNPRINTF(pcTemp, 1024, pcMimeBodyText, pstOpt->ulSecs);
               iBodySize += GEN_STRLEN(pcTemp)+ GEN_STRLEN(pcMailCrLf) ;
               pcMime     = MIME_Encode64((u_int8 *)pstList->pcData, pstList->iDataSize, NULL, &iMimeSize);
               iBodySize += iMimeSize + GEN_STRLEN(pcMailCrLf2);
               pcBody     = GEN_Remalloc(pcBody, iBodySize);
               GEN_STRCAT(pcBody, pcTemp);
               GEN_STRCAT(pcBody, pcMailCrLf);
               GEN_STRCAT(pcBody, pcMime);
               GEN_STRCAT(pcBody, pcMailCrLf2);
               safefree(pcMime);
               PRINTF1("mail-CreateMimeBody():(2):CT-Text-E:Size=%d bytes" CRLF, iBodySize); 
               break;

            case MAIL_CT_HTML: 
               PRINTF1("mail-CreateMimeBody():(2):CT-Html-S:Size=%d bytes" CRLF, iBodySize); 
               //
               // read it from file
               //
               if( (pcFile = mail_ReadResultsFromFile(pstList->pcData, MAIL_CT_HTML, &iSize)) )
               {
                  GEN_SNPRINTF(pcTemp, 1024, pcMimeBodyHtml, pstOpt->ulSecs); 
                  iBodySize += GEN_STRLEN(pcTemp)+ GEN_STRLEN(pcMailCrLf) ;
                  //
                  // Encode the data to base64
                  //
                  pcMime     = MIME_Encode64((u_int8 *)pcFile, iSize, NULL, &iMimeSize);
                  iBodySize += iMimeSize + GEN_STRLEN(pcMailCrLf2);
                  pcBody     = GEN_Remalloc(pcBody, iBodySize);
                  GEN_STRCAT(pcBody, pcTemp);
                  GEN_STRCAT(pcBody, pcMailCrLf);
                  GEN_STRCAT(pcBody, pcMime);
                  GEN_STRCAT(pcBody, pcMailCrLf2);
                  safefree(pcFile);
                  safefree(pcMime);
               }
               PRINTF1("mail-CreateMimeBody():(2):CT-Html-E:Size=%d bytes" CRLF, iBodySize); 
               break;

            case MAIL_CT_PICT: 
               PRINTF1("mail-CreateMimeBody():(2):CT-Pict-S:Size=%d bytes" CRLF, iBodySize); 
               //
               // read it from file
               //
               if( (pcFile = mail_ReadResultsFromFile(pstList->pcData, MAIL_CT_PICT, &iSize)) )
               {
                  char  *pcHtml;

                  //
                  // pcFile = picture bin data
                  //          Encode the data to base64
                  //
                  pcMime = MIME_Encode64((u_int8 *)pcFile, iSize, NULL, &iMimeSize);
                  //
                  // Create the boundary header
                  //
                  GEN_SNPRINTF(pcTemp, 1024, pcPlainBodyHtml, pstOpt->ulSecs); 
                  PRINTF2("mail-CreateMimeBody(%d):BoundHdr:\n%s" CRLF, GEN_Sleep(PSL), pcTemp); 
                  //
                  // Create HTML data to embed the base64 picture (pcMime):
                  // pcPlainBodyPicture = "<html><.....%s......%s....</html>"
                  //                                  pcExt   pcMime
                  //
                  pcExt  = mail_FileExtension(pstOpt, pstList->pcData);
                  iSize  = iMimeSize + GEN_STRLEN(pcExt) + GEN_STRLEN(pcPlainBodyPicture);
                  pcHtml = GEN_Remalloc(NULL, iSize);
                  GEN_SNPRINTF(pcHtml, iSize, pcPlainBodyPicture, pcExt, pcMime); 
                  PRINTF2("mail-CreateMimeBody(%d):HTML data:\n%s" CRLF, GEN_Sleep(PSL), pcHtml); 
                  //
                  // Add HTML with embedded MIME data to mail body
                  //
                  iBodySize += GEN_STRLEN(pcTemp) + GEN_STRLEN(pcHtml) + GEN_STRLEN(pcMailCrLf2);
                  PRINTF2("mail-CreateMimeBody(%d):ML-BodySize=%d bytes" CRLF, GEN_Sleep(PSL), iBodySize); 
                  pcBody     = GEN_Remalloc(pcBody, iBodySize);
                  //
                  GEN_STRCAT(pcBody, pcTemp);
                  GEN_STRCAT(pcBody, pcHtml);
                  GEN_STRCAT(pcBody, pcMailCrLf2);
                  safefree(pcFile);
                  safefree(pcMime);
                  safefree(pcHtml);
               }
               PRINTF1("mail-CreateMimeBody(%d):MAIL_CT_PICT Done." CRLF, GEN_Sleep(PSL)); 
               break;

            case MAIL_CT_ATTM: 
               PRINTF1("mail-CreateMimeBody():(2):CT-Attm-S:Size=%d bytes" CRLF, iBodySize); 
               //
               // attach it from file
               //
               if( (pcFile = mail_ReadResultsFromFile(pstList->pcData, MAIL_CT_ATTM, &iSize)) )
               {
                  pcExt = mail_FileExtension(pstOpt, pstList->pcData);
                  GEN_SNPRINTF(pcTemp, 1024, pcMimeBodyFile, pstOpt->ulSecs, pcExt); 
                  iBodySize += GEN_STRLEN(pcTemp)+ GEN_STRLEN(pcMailCrLf) ;
                  //
                  // Encode the data to base64
                  //
                  pcMime     = MIME_Encode64((u_int8 *)pcFile, iSize, NULL, &iMimeSize);
                  iBodySize += iMimeSize + GEN_STRLEN(pcMailCrLf2);
                  pcBody     = GEN_Remalloc(pcBody, iBodySize);
                  GEN_STRCAT(pcBody, pcTemp);
                  GEN_STRCAT(pcBody, pcMailCrLf);
                  GEN_STRCAT(pcBody, pcMime);
                  GEN_STRCAT(pcBody, pcMailCrLf2);
                  safefree(pcFile);
                  safefree(pcMime);
               }
               PRINTF1("mail-CreateMimeBody():(2):CT-File-E:Size=%d bytes" CRLF, iBodySize); 
               break;

            case MAIL_CT_FILE: 
               PRINTF1("mail-CreateMimeBody():(2):CT-File-S:Size=%d bytes" CRLF, iBodySize); 
               //
               // Generic text body: put it into the multipart/mixed container
               // insert it from file
               //
               if( (pcFile = mail_ReadResultsFromFile(pstList->pcData, MAIL_CT_FILE, &iSize)) )
               {
                  GEN_SNPRINTF(pcTemp, 1024, pcMimeBodyText, pstOpt->ulSecs);
                  iBodySize += GEN_STRLEN(pcTemp)+ GEN_STRLEN(pcMailCrLf) ;
                  //
                  // Encode the data to base64
                  //
                  pcMime     = MIME_Encode64((u_int8 *)pcFile, iSize, NULL, &iMimeSize);
                  iBodySize += iMimeSize + GEN_STRLEN(pcMailCrLf2);
                  pcBody     = GEN_Remalloc(pcBody, iBodySize);
                  GEN_STRCAT(pcBody, pcTemp);
                  GEN_STRCAT(pcBody, pcMailCrLf);
                  GEN_STRCAT(pcBody, pcMime);
                  GEN_STRCAT(pcBody, pcMailCrLf2);
                  safefree(pcFile);
                  safefree(pcMime);
               }
               break;
         }
      }
      pstList = pstList->pstNext;
   }
   // (3) MAIL_DT_ATTM
   // Add all attachments base64 encoded 
   //
   PRINTF1("mail-CreateMimeBody():(3):Attm-S:Size=%d bytes" CRLF, iBodySize); 
   pstList = pstOpt->pstList;
   while(pstList)
   {
      if(pstList->tDataType == MAIL_DT_ATTM)
      {
         pcFileName = basename(pstList->pcData);
         pcExt = mail_FileExtension(pstOpt, pcFileName);
         GEN_SNPRINTF(pcTemp, 1024, pcMimeAttmFile, pstOpt->ulSecs, pcExt, pcFileName, pcFileName);
         pcMime = mail_EncodeAttachment(pstList->pcData, &iMimeSize);
         if(pcMime)
         {
            iBodySize += GEN_STRLEN(pcTemp) + GEN_STRLEN(pcMailCrLf);
            iBodySize += iMimeSize + GEN_STRLEN(pcMailCrLf2);
            pcBody     = GEN_Remalloc(pcBody, iBodySize);
            GEN_STRCAT(pcBody, pcTemp);
            GEN_STRCAT(pcBody, pcMailCrLf);
            GEN_STRCAT(pcBody, pcMime);
            GEN_STRCAT(pcBody, pcMailCrLf2);
            safefree(pcMime);
         }
      }
      pstList = pstList->pstNext;
   }
   PRINTF1("mail-CreateMimeBody():(3):Attm-E:Size=%d bytes" CRLF, iBodySize); 
   // (4)
   // End of multipart/mixed
   //
   GEN_SNPRINTF(pcTemp, 1024, "\r\n--=_%ld_=--\r\n\r\n", pstOpt->ulSecs);
   iBodySize += GEN_STRLEN(pcTemp);
   pcBody     = GEN_Remalloc(pcBody, iBodySize);
   GEN_STRCAT(pcBody, pcTemp);
   //
   // MSG = Message_Subject
   //       ContType_Message_Header
   //
   //       --=_<timestamp>_=
   //       ContType_Message_Body
   //       base64(message_body)
   //
   // ATT1     --=_<timestamp>_=
   //          ContHdr(FileAtt[1])
   //          base64 (FileAtt)
   //       
   //          .....
   //       
   // ATTx     --=_<timestamp>_=
   //          ContHdr(FileAtt[x])
   //          base64 (FileAtt)
   //
   //       --=_<timestamp>_=--
   //-------------------------------------------------------------------------------------------
   PRINTF2("mail-CreateMimeBody(%d):Done, Body/Attm size=%d bytes" CRLF, GEN_Sleep(PSL), iBodySize);
   safefree(pcTemp);
   //
   *piSize = iBodySize;
   return(pcBody);
}

//
//  Function:   mail_EncodeAttachment
//  Purpose:    Mime-base64 encode the attachment
//
//  Parms:      Attachment filename, result size ptr
//  Returns:    The buffer^ to the base64 encoded attachment or NULL if not found
//
static char *mail_EncodeAttachment(char *pcFileName, int *piSize)
{
   char       *pcAttm;
   char       *pcMime=NULL;
   char        cFileSize[16]; 
   int         iFd, iRead;
   int         iAttmSize;

   //
   // Determine filesize
   //
   FINFO_GetFileInfo(pcFileName, FI_SIZE, cFileSize, 16);
   iAttmSize = (int)strtoul(cFileSize, NULL, 10);
   PRINTF2("mail-EncodeAttachment(): %s=%d bytes" CRLF, pcFileName, iAttmSize);
   //
   // Read attachment
   //
   iFd = safeopen(pcFileName, O_RDWR);
   //
   if(iFd > 0)
   {
      //
      // OKEE: proceed
      // Read the whole file into memory
      //
      pcAttm = safemalloc(iAttmSize);
      iRead  = saferead(iFd, pcAttm, iAttmSize);
      safeclose(iFd);
      //
      if(iRead == iAttmSize) pcMime = MIME_Encode64((u_int8 *)pcAttm, iAttmSize, NULL, piSize);
      else                   PRINTF2("mail-EncodeAttachment(): ERROR: Read=%d, File=%d" CRLF, iRead, iAttmSize);
      //
      safefree(pcAttm);
   }
   else
   {
      LOG_Report(errno, "CML", "mail-EncodeAttachment():ERROR ");
   }
   return(pcMime);
}

//
//  Function:   mail_Free
//  Purpose:    Free all allocated memory
//
//  Parms:      mail struct
//  Returns:    
//
static void mail_Free(MAILO *pstOpt)
{
   MAILL   *pstNext;
   MAILL   *pstList=pstOpt->pstList;

   if(pstOpt->pcHost) safefree(pstOpt->pcHost);
   if(pstOpt->pcUser) safefree(pstOpt->pcUser);
   if(pstOpt->pcPass) safefree(pstOpt->pcPass);
   if(pstOpt->pcMail) safefree(pstOpt->pcMail);
   //
   do
   {
      if(pstList) 
      {
         pstNext = pstList;
         if(pstList->pcData) safefree(pstList->pcData);
         pstList = pstList->pstNext;
         safefree(pstNext);
      }
   }
   while(pstList);
   //
   safefree(pstOpt);
}

// 
// Function:   mail_ReadResultsFromFile
// Purpose:    Read the data returned from external handlers
// 
// Parameters: 
// Returns:    MAILCT, Buffer with mail, Size
// Note:       Data to fetch has been stored in pcFileName. 
//             MAIL_CT_HTML:  Drop the HTTP headers to obtain the HTML part
//             Other:         Just read the (bin)file.
//
//             Caller needs to free the returned memory !
// 
static char *mail_ReadResultsFromFile(char *pcFileName, MAILCT tCt, int *piSize)
{
   int      iFd, iSize, iRead;
   char    *pcMail=NULL;
   char     cFileSize[16];

   //
   // Determine filesize
   //
   FINFO_GetFileInfo(pcFileName, FI_SIZE, cFileSize, 16);
   iSize = strtoul(cFileSize, NULL, 10);
   PRINTF2("mail-ReadResultsFromFile(): %s=%d bytes" CRLF, pcFileName, iSize);
   //
   // Read mail
   //
   iFd = safeopen(pcFileName, O_RDWR);
   //
   if(iFd > 0)
   {
      pcMail = safemalloc(iSize);
      switch(tCt)
      {
         case MAIL_CT_HTML:
            //
            // Proxy has HTTP headers still intact: skip data until HTML headers appear
            // Drop the HTTP headers
            //
            do
            {
               iRead = saferead(iFd, pcMail, 1);
               iSize--;
               if(*pcMail == '<')   break;
            }
            while(iSize);
            //
            iRead = saferead(iFd, &pcMail[1], iSize);
            if(iRead != iSize) PRINTF2("mail-ReadResultsFromFile(): ERROR: Read=%d, File=%d" CRLF, iRead, iSize);
            break;

         default:
            // MAIL_CT_TEXT,
            // MAIL_CT_PICT,
            // MAIL_CT_ATTM,
            // MAIL_CT_FILE,
            iRead = saferead(iFd, pcMail, iSize);
            if(iRead != iSize) PRINTF2("mail-ReadResultsFromFile(): ERROR: Read=%d, File=%d" CRLF, iRead, iSize);
            break;
            
      }
      safeclose(iFd);
      *piSize = iSize;
   }
   return(pcMail);
}

//
//  Function:   mail_FileExtension
//  Purpose:    Handle filename extension
//
//  Parms:      MAILO, filename
//  Returns:    Content-Type string
//
static const char *mail_FileExtension(MAILO *pstOpt, char *pcFile)
{
   const char *pcFileExt;
   const char *pcTypeExt;
   const char *pcType=pcContentTypeTxt;
   int         iIdx;
   PFEXT       pfExtCb;

   pcFileExt = strrchr(pcFile, '.');
   if(pcFileExt++)
   {
      PRINTF1("mail-FileExtension(): Ext=%s" CRLF, pcFileExt);
      for(iIdx=0; iIdx<iNumExtTypes; iIdx++)
      {
         pcTypeExt = stExtensionTypes[iIdx].pcExt;
         if(GEN_STRCMP(pcFileExt, pcTypeExt) == 0)
         {
            pfExtCb = stExtensionTypes[iIdx].pfExt;
            if(pfExtCb) pcType = pfExtCb(pstOpt, &stExtensionTypes[iIdx]);
         }
      }
   }
   return(pcType);
}

//
//  Function:   mail_GetListSize
//  Purpose:    Get size of this datatype in the options list
//
//  Parms:      Datatype, MAILO *
//  Returns:    Size
//
static int mail_GetListSize(MAILO *pstOpt, MAILDT tDt)
{
   int      iSize=0;
   MAILL   *pstList=pstOpt->pstList;

   while(pstList)
   {
      if(pstList->tDataType == tDt)
      {
         //
         // Add this datatype to the size
         //
         iSize  += pstList->iDataSize;
      }
      pstList = pstList->pstNext;
   }
   return(iSize);
}

//
//  Function:   mail_HandleExtTypeGeneric
//  Purpose:    Handle Generic extension
//
//  Parms:      MAILO *, EXTTYPE *
//  Returns:    Content type string ptr
//
static const char *mail_HandleExtTypeGeneric(MAILO *pstOpt, const EXTTYPE *pstExt)
{
   return(pstExt->pcType);
}


/* ======   Local Functions separator ===========================================
___COMMENTS(){}
==============================================================================*/

