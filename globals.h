/*  (c) Copyright:  2018  Patrn, Confidential Data
 *
 *  Workfile:           globals.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Define the mmap variables, common over all threads. If
 *                      the structure needs to change, roll the revision number to make sure a new mmap file is created !
 *                     
 * 
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    24 Jul 2018:      Created
 *    16 Apr 2021:      Add Verbose LOG level
 *    04 Jun 2021:      Add global pstMap; Add GLOBAL_CheckDelete()
 *
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <semaphore.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <netinet/in.h>

#ifdef DEFINE_GLOBALS
   #define EXTERN
#else
   #define EXTERN extern
#endif

//
// Enums PIDs
//
typedef enum _proc_pids_
{
   #define  EXTRACT_PID(a,b,c)   a,
   #include "par_pids.h"
   #undef EXTRACT_PID
   //
   NUM_PIDT
}  PIDT;
//
typedef struct _proc_pidlist_
{
   pid_t       tPid;
   int         iFlag;
   int         iNotify;
   const char *pcName;
   const char *pcHelp;
}  PIDL;
//
typedef struct _par_args_
{
   int            iFunction;
   const char    *pcJson;
   const char    *pcHtml;
   const char    *pcOption;
   int            iValueOffset;
   int            iValueSize;
   int            iChangedOffset;
}  PARARGS;
//
typedef struct _globals_defaults_
{
   int         iFunction;
   const char *pcDefault;
   int         iChanged;
   int         iChangedOffset;
   int         iValueOffset;
   int         iGlobalSize;
}  GLODEFS;
//
typedef enum _global_parameters_
{
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    a,
#include "par_defs.h"
#undef EXTRACT_PAR
   NUM_GLOBAL_DEFS
}  GLOPAR;
//
//
#define  PAR____        0x00000000     // No command
//
// HTTP Functions (callback through HTML or JSON)
//
#define  PAR_STI        0x00000001     // Snapshot foto
#define  PAR_TIM        0x00000002     // Timelapse foto's
#define  PAR_REC        0x00000004     // Video recording
#define  PAR_STR        0x00000008     // Video streaming
#define  PAR_PIX        0x00000010     // List of pictures
#define  PAR_VID        0x00000020     // List of Videos
#define  PAR_PRM        0x00000040     // List of Parameters
#define  PAR_RMP        0x00000080     // Remove fotos
#define  PAR_RMV        0x00000100     // Remove videos
#define  PAR_DEF        0x00000200     // Defaults
#define  PAR_STP        0x00000400     // Stop
#define  PAR_ALM        0x00000800     // Alarm timer
#define  PAR_VEC        0x00001000     // Video motion vectors
#define  PAR_ERR        0x00008000     // Error replies
#define  PAR_ALL        0x0000FFFF     // All HTTP functions
//
// Internal Functions (callback through SIGUSR1=10/SIGUSR2=12)
//
#define  PAR_UNM        0x01000000     // Unmount
#define  PAR_PAT        0x02000000     // Mode pattern changed
#define  PAR_KEY        0x04000000     // RCU IR keypress
#define  PAR_BUT        0x08000000     // Button changed
//
// JSON markers for text-style-vars ("value" = "123456")
//             or number-style-vars ("value" =  123456 )
//
#define  JSN_TXT        0x10000000     // JSON Variable is text
#define  JSN_INT        0x20000000     // JSON Variable is integer
#define  WB             0x80000000     // Warm boot var
//
#define  ALWAYS_CHANGED -1
#define  NEVER_CHANGED  -2
//
#define  ONEMILLION                       1000000L
#define  ONEBILLION                       1000000000L
//
#define  MAX_CMD_LEN                      200
#define  MAX_ARG_LEN                      1000
#define  MAX_CMDLINE_LEN                  (MAX_CMD_LEN + MAX_ARG_LEN)
//
#define  MAX_TEMP_LEN                     80
#define  MAX_NUM_ALARMS                   16
#define  MAX_NUM_REPORT_LINES             250
//
// Global area array sizes
//
#define  MAX_PATH_LEN                     499
#define  MAX_PATH_LENZ                    MAX_PATH_LEN+1
#define  MAX_URL_LEN                      31
#define  MAX_URL_LENZ                     MAX_URL_LEN+1
#define  MAX_ADDR_LEN                     INET_ADDRSTRLEN
#define  MAX_ADDR_LENZ                    INET_ADDRSTRLEN+1
#define  MAX_PARM_LEN                     15
#define  MAX_PARM_LENZ                    MAX_PARM_LEN+1
#define  MAX_MAIL_LEN                     31
#define  MAX_MAIL_LENZ                    MAX_MAIL_LEN+1
#define  MAX_PASS_LEN                     127
#define  MAX_PASS_LENZ                    MAX_PASS_LEN+1
//
// GLOBAL Notification:
// Init notifications  0x0000.0FFF
//
#define  GLOBAL_CMD_INI 0x00000001        // Ini: Cmd handler
#define  GLOBAL_XXX_INI 0x000007FF        // Init Mask
//
// GLOBAL Notification:
// Activations:
//
#define  GLOBAL_CMD_RUN 0x00001000        // CMD
#define  GLOBAL_GRD_RUN 0x00002000        // GRD
#define  GLOBAL_XXX_RUN 0x00FFF000        // Run Mask
//
// GLOBAL Notification:
// Completion notifications:
//
#define  GLOBAL_CMD_NFY 0x01000000        // CMD
#define  GLOBAL_FLT_NFY 0x02000000        // FLT
#define  GLOBAL_XXX_NFY 0xFF000000        // Notify Mask
//
typedef enum _varreset_
{
   VAR_COLD    = 0,                 // Cold reset
   VAR_WARM,                        // Warm reset
   VAR_UPDATE,                      // Update

   NUM_VARRESETS
}  VARRESET;
//
typedef enum _rpi_mapstate_
{
  RMAP_CLEAR = 0,                   // Clear all the mapped memory
  RMAP_SYNC,                        // Synchronize the map
  RMAP_RESTART,                     // Restart the counters using the last stored data

  NUM_RMAPSTATES
} RMAPSTATE;

//=============================================================================
// Global parameters mapped to a shared MMAP file 
//=============================================================================
typedef struct _rpimap_
{
   int               G_iSignature1;
   char              G_iVersionMajor;
   char              G_iVersionMinor;
   u_int32           G_ulStartTimestamp;
   //
   // Misc semaphores
   //
   sem_t             G_tSemHost;
   sem_t             G_tSemCmnd;
   pthread_mutex_t   G_tMutex;
   //
   //=============== Start WB reset area ======================================
   int               G_iResetStart;
   //
   u_int32           G_ulDebugMask;
   u_int32           G_ulSecondsCounter;
   int               G_iMallocs;
   int               G_iSegFaultLine;
   int               G_iSegFaultPid;
   //
   int               G_iTraceCode;
   int               G_iGuards;   
   char              G_pcSegFaultFile     [MAX_URL_LENZ];
   PIDL              G_stPidList          [NUM_PIDT];
   GLOG              G_stLog;
   //
   //--------------------------------------------------------------------------
   // Take new power-on reset parms from this pool:
   //--------------------------------------------------------------------------
   #define SPARE_POOL_ZERO 100
   #define EXTRA_POOL_ZERO (2*MAX_PARM_LENZ)
   //
   char              G_pcVerbose          [MAX_PARM_LENZ];
   char              G_pcDelete           [MAX_PARM_LENZ];
   char              G_pcSparePoolZero    [SPARE_POOL_ZERO-EXTRA_POOL_ZERO];
   //
   // Extra from zero-init pool
   //

   //--------------------------------------------------------------------------
   int               G_iResetEnd;
   //=============== End WB reset area ========================================
   //
   char              G_pcHostname         [MAX_URL_LENZ];
   char              G_pcVersionSw        [MAX_PARM_LENZ];
   //
   char              G_pcArchiveDir       [MAX_PATH_LENZ];
   char              G_pcMediaDir         [MAX_PATH_LENZ];
   char              G_pcFifoDir          [MAX_PATH_LENZ];
   char              G_pcLogFile          [MAX_PATH_LENZ];
   char              G_pcLastVideoFile    [MAX_PATH_LENZ];
   char              G_pcLastThumbFile    [MAX_PATH_LENZ];
   char              G_pcLastStillFile    [MAX_PATH_LENZ];
   char              G_pcMinuteNow        [MAX_PARM_LENZ];
   char              G_pcMinuteDawn       [MAX_PARM_LENZ];
   char              G_pcMinuteSunrise    [MAX_PARM_LENZ];
   char              G_pcMinuteSunset     [MAX_PARM_LENZ];
   char              G_pcMinuteDusk       [MAX_PARM_LENZ];
   char              G_pcMotionState      [MAX_PARM_LENZ];
   //
   // Email user/passw/server data
   //
   char              G_pcPop3User         [MAX_MAIL_LENZ];
   char              G_pcPop3Pass         [MAX_PASS_LENZ];
   char              G_pcPop3Server       [MAX_MAIL_LENZ];
   char              G_pcPop3Port         [MAX_PARM_LENZ];
   //
   char              G_pcSmtpUser         [MAX_MAIL_LENZ];
   char              G_pcSmtpPass         [MAX_PASS_LENZ];
   char              G_pcSmtpServer       [MAX_MAIL_LENZ];
   char              G_pcSmtpPort         [MAX_PARM_LENZ];
   char              G_pcDebugMask        [MAX_PARM_LENZ];
   //
   u_int32           G_ulMailSecsNow;
   u_int32           G_ulMailSecsMail;
   int               G_iArchiveCount;
   //
   //--------------------------------------------------------------------------
   // Take new persistent parms from this pool:
   //--------------------------------------------------------------------------
   #define SPARE_POOL_PERS 100
   #define EXTRA_POOL_PERS 0
   //
   // Extra from non-volatile pool
   //
   char              G_pcSparePool2          [SPARE_POOL_PERS-EXTRA_POOL_PERS];
   
   //--------------------------------------------------------------------------
   int               G_iSignature2;          // Signature bottom
}  RPIMAP;

//=============================================================================
// GLOBAL MMAP structure pointer
//=============================================================================
EXTERN RPIMAP *pstMap;

//
// Global functions
//
bool           GLOBAL_Init                   (void);
//
void           GLOBAL_ExpandMap              (int, int);
bool           GLOBAL_Close                  (void);
int            GLOBAL_Lock                   (void);
int            GLOBAL_Unlock                 (void);
char          *GLOBAL_GetHostName            (void);
int            GLOBAL_GetTraceCode           (void);
u_int32        GLOBAL_ConvertDebugMask       (bool);
u_int32        GLOBAL_GetDebugMask           (void);
void           GLOBAL_SetDebugMask           (u_int32);
u_int32        GLOBAL_ReadSecs               (void);
void           GLOBAL_RestoreDefaults        (void);
bool           GLOBAL_Share                  (void);
int            GLOBAL_Status                 (int);
char          *GLOBAL_GetHostName            (void);
bool           GLOBAL_Sync                   (void);
void           GLOBAL_SegmentationFault      (const char *, int);
bool           GLOBAL_PidCheckGuards         (void);
bool           GLOBAL_PidGetGuard            (PIDT);
bool           GLOBAL_PidSetGuard            (PIDT);
void           GLOBAL_PidClearGuards         (void);
void           GLOBAL_PidSaveGuard           (PIDT, int);
pid_t          GLOBAL_PidGet                 (PIDT);
const char    *GLOBAL_PidGetName             (PIDT);
const char    *GLOBAL_PidGetHelp             (PIDT);
bool           GLOBAL_PidPut                 (PIDT, pid_t);
int            GLOBAL_PidsLog                (void);
int            GLOBAL_PidsTerminate          (int);
int            GLOBAL_Notify                 (int, int, int);
int            GLOBAL_HostNotification       (int);
bool           GLOBAL_GetSignalNotification  (int, int);
int            GLOBAL_SetSignalNotification  (int, int);
void           GLOBAL_UpdateSecs             (void);
//
int            GLOBAL_GetMallocs             (void);
void           GLOBAL_PutMallocs             (int);
//
void           GLOBAL_Signal                 (PIDT, int);
void           GLOBAL_SignalPid              (pid_t, int);
//
const PARARGS *GLOBAL_GetParameters          (void);
char          *GLOBAL_GetParameter           (GLOPAR);
int            GLOBAL_GetParameterSize       (GLOPAR);
GLOG          *GLOBAL_GetLog                 (void);
bool           GLOBAL_CheckDelete            (char *);
#endif /* _GLOBALS_H_ */
