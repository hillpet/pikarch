/*  (c) Copyright:  2018  Patrn, Confidential Data
 *
 *  Workfile:           par_defs.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            All defines for extracting rpi archive data
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    24 Jul 2018:      Created
 *    14 Aug 2018:      Add $D and $o args
 *    16 Apr 2021:      Add Verbose LOG level
 *
 *
 *
 *
 *
 *
**/

//
// Macro:       a                                              b                                     c                 d        e           f                                     g                  h                                        i   j                        k
//          Cmd-Enum,         _____________________________iFunction____________________________   pcJson,           pcHtml   pcOption iValueOffset                           iValueSize        iChangedOffset                                   pcDefault                 pfHttpFun
//                               Txt/Int Snaps   Tlapse  Recs    Strm    Misc
// Misc
EXTRACT_PAR(PAR_HOSTNAME,    (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "Hostname",       "",      "",      offsetof(RPIMAP, G_pcHostname),        MAX_URL_LEN,      ALWAYS_CHANGED,                               0, "",                       NULL)
EXTRACT_PAR(PAR_VERSION,     (WB|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "Version",        "vrs=",  "",      offsetof(RPIMAP, G_pcVersionSw),       MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, VERSION,                  NULL)
// The files :
EXTRACT_PAR(PAR_ARCHIVE,     (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "ArchDir",        "",      "",      offsetof(RPIMAP, G_pcArchiveDir),      MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, "",                       NULL)
EXTRACT_PAR(PAR_MEDIA,       (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "MediaDir",       "",      "",      offsetof(RPIMAP, G_pcMediaDir),        MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, "",                       NULL)
EXTRACT_PAR(PAR_FIFO,        (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "FifoDir",        "",      "",      offsetof(RPIMAP, G_pcFifoDir),         MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, "",                       NULL)
EXTRACT_PAR(PAR_LOG,         (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "LogFile",        "",      "",      offsetof(RPIMAP, G_pcLogFile),         MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, RPI_PIKRELL_LOG,          NULL)
EXTRACT_PAR(PAR_LAST_VID,    (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "VideoFile",      "",      "",      offsetof(RPIMAP, G_pcLastVideoFile),   MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, "",                       NULL)
EXTRACT_PAR(PAR_LAST_THB,    (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "ThumbFile",      "",      "",      offsetof(RPIMAP, G_pcLastThumbFile),   MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, "",                       NULL)
EXTRACT_PAR(PAR_LAST_PIX,    (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "StillFile",      "",      "",      offsetof(RPIMAP, G_pcLastStillFile),   MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, "",                       NULL)
EXTRACT_PAR(PAR_MIN_NOW,     (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "MinNow",         "",      "",      offsetof(RPIMAP, G_pcMinuteNow),       MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(PAR_MIN_DAWN,    (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "MinDawn",        "",      "",      offsetof(RPIMAP, G_pcMinuteDawn),      MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(PAR_MIN_SUNRISE, (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "MinSunrise",     "",      "",      offsetof(RPIMAP, G_pcMinuteSunrise),   MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(PAR_MIN_SUNSET,  (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "MinSunset",      "",      "",      offsetof(RPIMAP, G_pcMinuteSunset),    MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(PAR_MIN_DUSK,    (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "MinDusk",        "",      "",      offsetof(RPIMAP, G_pcMinuteDusk),      MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(PAR_MOTION_STATE,(   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "MotState",       "",      "",      offsetof(RPIMAP, G_pcMotionState),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(PAR_VERBOSE,     (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "Verbose",        "",      "",      offsetof(RPIMAP, G_pcVerbose),         MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
// Server
EXTRACT_PAR(PAR_POP3_USER,   (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "Pop3User",       "",      "",      offsetof(RPIMAP, G_pcPop3User),        MAX_MAIL_LEN,     ALWAYS_CHANGED,                               0, "nemsi@ziggo.nl",         NULL)
EXTRACT_PAR(PAR_POP3_PASS,   (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "Pop3Pass",       "",      "",      offsetof(RPIMAP, G_pcPop3Pass),        MAX_PASS_LEN,     ALWAYS_CHANGED,                               0, "",                       NULL)
EXTRACT_PAR(PAR_POP3_SRVR,   (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "Pop3Server",     "",      "",      offsetof(RPIMAP, G_pcPop3Server),      MAX_MAIL_LEN,     ALWAYS_CHANGED,                               0, "pop.ziggo.nl",           NULL)
EXTRACT_PAR(PAR_POP3_PORT,   (   JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "Pop3Port",       "",      "",      offsetof(RPIMAP, G_pcPop3Port),        MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "110",                    NULL)
//
EXTRACT_PAR(PAR_SMTP_USER,   (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "SmtpUser",       "",      "",      offsetof(RPIMAP, G_pcSmtpUser),        MAX_MAIL_LEN,     ALWAYS_CHANGED,                               0, "nemsi@ziggo.nl",         NULL)
EXTRACT_PAR(PAR_SMTP_PASS,   (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "SmtpPass",       "",      "",      offsetof(RPIMAP, G_pcSmtpPass),        MAX_PASS_LEN,     ALWAYS_CHANGED,                               0, "",                       NULL)
EXTRACT_PAR(PAR_SMTP_SRVR,   (   JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "SmtpServer",     "",      "",      offsetof(RPIMAP, G_pcSmtpServer),      MAX_MAIL_LEN,     ALWAYS_CHANGED,                               0, "smtp.ziggo.nl",          NULL)
EXTRACT_PAR(PAR_SMTP_PORT,   (   JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "SmtpPort",       "",      "",      offsetof(RPIMAP, G_pcSmtpPort),        MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "25",                     NULL)
