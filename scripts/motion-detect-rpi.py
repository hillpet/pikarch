#!/usr/bin/env python
# tab-width: 4
# This on_motion_begin script will send a bit pattern as remote IO to the
# Remote IO server RPI #5
#
# Set this script to be a PiKrellCam on_motion_begin script by copying to
# scripts/motion-xmt-rpi5 and editing on_motion_begin in pikrellcam.conf:
#
# 	on_motion_begin $C/motion-xmt-rpi5
#

import sys
import socket

if len (sys.argv) < 2:
    bit = "256"
else:
    bit = sys.argv[1]

msg = "GET /io.json?RemIo=" + bit + " HTTP/1.1\r\n\r\n"

RPI5_IP = "192.168.178.205"
RPI5_PORT = 80
BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((RPI5_IP, RPI5_PORT))
s.send(msg)
data = s.recv(BUFFER_SIZE)
s.close()

#print "Received data:", data
