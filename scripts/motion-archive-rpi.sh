#!/bin/bash
# Script to copy a motion video file from the loop to the archive dir.
#
# If this script is configured to be run as the on_motion_end command, than
# motion videos can be immediately saved to a backup dir as long as the
# archiving utility is present (/usr/bin/pikarch).
#
# This script should be configured in ~/.pikrellcam/pikrellcam.conf and not
#   in the at-command.conf file.  Eg in pikrellcam.conf:
#   on_motion_end $C/motion-end-rpi8 $V $a $v $P $m $t $M $S $s $G
#
# Argument substitution done by PiKrellCam before running this script:
#   $C - scripts directory so this script is found.
#   $G - log file configured in ~/.pikrellcam/pikrellcam.conf.
#   $a - the full path name of the Archive.
#   $v - the full path name of the motion video just saved.
#   $A - the full path name of the last motion thumbnail file.
#   $P - the command FIFO so we can write commands to PiKrellCam.
#

LOG_FILE=$1
if [ "$LOG_FILE" == "" ]
then
    LOG_FILE=/mnt/rpicache/pikarch.err
fi

if [ -f /usr/bin/pikarch ]
then
    echo `date`:Start archive utility "$@">> $LOG_FILE
    sudo /usr/bin/pikarch "$@"
else
    echo NOT FOUND : Archive utility "$@" >> $LOG_FILE
fi

