#!/bin/bash
# Script to run at the beginning of motion-detection
#
# If this script is configured to be run as the on_motion_begin command. 
# Motion can be determined further by examining /run/pikrellcam/motion-events.
#
# This script should be configured in ~/.pikrellcam/pikrellcam.conf and not
#   in the at-command.conf file.  Eg in pikrellcam.conf:
#   on_motion_begin $C/motion-detect-rpi.sh $G $v $a $P
#
# Argument substitution done by PiKrellCam before running this script:
#   $C - scripts directory so this script is found.
#   $G - log file configured in ~/.pikrellcam/pikrellcam.conf.
#   $v - the full path name of the last motion video file.
#   $a - the full path name of the archive
#   $P - the command FIFO so we can write commands to PiKrellCam.
#
#   $A - the full path name of the last motion thumbnail file.
#

LOG_FILE=$1
if [ "$LOG_FILE" == "" ]
then
    LOG_FILE=/mnt/rpicache/pikarch.err
fi

if [ -f /usr/bin/pikmotion ]
then
    sudo /usr/bin/pikmotion "$@"
else
    echo NOT FOUND : Motion detect utility $v to $a >> $LOG_FILE
fi

