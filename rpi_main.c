/*  (c) Copyright:  2018..2019 Patrn, Confidential Data
 *
 *  Workfile:           rpi_main.c
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            pikarch archives the loop video file 
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    24 Jul 2018:      Created
 *    08 Feb 2019:      Add parms check
 *    16 Apr 2021:      Add Verbose LOG level
 *    06 Jun 2021:      Add flags to verbose arg
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_cmnd.h"
#include "rpi_main.h"
//
//#define USE_PRINTF
//#undef  FEATURE_USE_PRINTF
//#define FEATURE_USE_STDOUT
#include <printf.h>

//
// Local arguments
//
static const char *pcLogfile        = RPI_PUBLIC_LOG_PATH;
static int         iNumArgs         = 0;
//
static char        cArgs[NUM_ARGS][ARG_LENZ];
//
// Local functions
//
static int     rpi_CopyOption             (char *, char *);

/* ======   Local Functions separator ===========================================
___MAIN_FUNCTIONS(){}
==============================================================================*/

//
// Function:   main
// Purpose:    Main entry point for the Raspberry Pi PiKrellCam archive utility
//
// Parms:      Commandline options
// Returns:    Exit codes
// Note:       Called by PiKrellCam script to copy a motion video file from the loop 
//             to the archive dir.
//
//             If this script is configured to be run as the on_motion_end command, then
//             motion videos can be immediately saved to a backup dir as long as the
//             archiving utility is present (/usr/bin/pikarch).
//             
//             This script should be configured in ~/.pikrellcam/pikrellcam.conf and not
//             in the at-command.conf file.  Eg in pikrellcam.conf:
//             on_motion_end $C/motion-archive-rpi.sh $G $a $v $A $P $D $o
//             
//             Argument substitution done by PiKrellCam before running this script:
//               $C - scripts directory so this script is found.
//               $G - log file configured in ~/.pikrellcam/pikrellcam.conf.
//               $a - the full path name of the Archive.
//               $v - the full path name of the motion video just saved.
//               $A - the full path name of the last motion thumbnail file.
//               $P - the command FIFO so we can write commands to PiKrellCam.
//               $D - minutes now, dawn, sunrise, sunset, dusk
//               $o - motion state on/off
//             
//             Examples of the arguments passed to the pikarch utility are:
//             	0: Utility path: /usr/bin/pikarch
//             	1: $G - /mnt/rpicache/pikarch.log
//             	2: $a - /all/public/www/media/archive
//             	3: $v - /all/public/www/media/videos/loop_2018-07-27_18.46.42_m.mp4
//             	4: $A - /all/public/www/media/videos/loop_2018-07-27_18.46.42_m.th.jpg
//             	5: $P - /home/pi/pikrellcam/www/FIFO
//             	6: $D - 1294 339 337 1256 1294
//             	7: $o - off
//             	8: 0  - Flags/Verbose level
//
//             First have the arguments copied to a location accessible to all threads
//
int main(int argc, char **argv)
{
   int   i=0, iCc=EXIT_CC_OKEE;

   PRINTF1("main():%d args" CRLF, argc);
   //
   while( (i<argc) && (i<NUM_ARGS) )
   {
      rpi_CopyOption(cArgs[i], argv[i]);
      PRINTF2("main():Copied argument %d [%s]" CRLF, i, cArgs[i]);
      i++;
      iNumArgs++;
   }
   //
   // Make sure we have the right mmap parameters setup for us !
   //
   GLOBAL_Init();
   //
   // Truncate reports file
   //
   if(GEN_FileTruncate((char *)pcLogfile, MAX_NUM_REPORT_LINES) != 0)
   {
      PRINTF1("main():Error truncating reports file %s" CRLF, pcLogfile);
   }
   //
   // Open logfile
   //
   if( LOG_ReportOpenFile((char *)pcLogfile) == FALSE)
   {
      PRINTF1("main():Error opening log file %s" CRLF, pcLogfile);
   }
   else
   {
      PRINTF1("main():Logfile is %s" CRLF, pcLogfile);
   }
   iCc = CMD_Execute(cArgs, ARG_LENZ, iNumArgs);
   //
   // Exit 
   //
   //LOG_Report(0, "CMD", "Version=%s now EXIT", VERSION);
   LOG_ReportCloseFile();
   //==============================================================================================
   // From here on :
   // CANNOT Use functions which use MMAP anymore:
   // CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
   //==============================================================================================
   GLOBAL_Sync();
   GLOBAL_Close();
   PRINTF1("Version=%s now EXIT" CRLF, VERSION);
   exit(iCc);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   rpi_CopyOption
// Purpose:    Copy applicable option to the list
// 
// Parameters: Dest, Src
// Returns:    Option length
// 
static int rpi_CopyOption(char *pcArgs, char *pcOption)
{
   int   iLen=0;

   while( (*pcOption != ' ') && (*pcOption != 0) )
   {
      *pcArgs++ = *pcOption++;
      iLen++;
   }
   *pcArgs = 0;
   return(iLen);
}




