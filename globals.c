/*  (c) Copyright:  2018  Patrn, Confidential Data
 *
 *  Workfile:           globals.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Global variables for Raspberry pi pikarch tool
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    24 Jul 2018:      Created
 *    16 Apr 2021:      Add Verbose LOG level
 *    04 Jun 2021:      Add global pstMap; Add GLOBAL_CheckDelete()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
//
#include <common.h>
#include "config.h"
//
#define DEFINE_GLOBALS
#include "globals.h"

//#define USE_PRINTF
#include <printf.h>

//
// Local functions
//
static bool    global_CreateMap        (void);
static void    global_InitMemory       (RMAPSTATE);
static void    global_PidsInit         (void);
static void    global_Open             (const char *, int);
static void    global_SaveMapfile      (void);
static int     global_RestoreMapfile   (void);
static bool    global_GetFileInfo      (char *, char *);
//
// Static variables
//
static int        iGlobalMapSize;
static int        iFdMap;
//
static const char pcMapFile[]       = RPI_MAP_PATH;
static const char pcDirRestoreSrc[] = RPI_BACKUP_DIR;
static const char pcDirRestoreDst[] = RPI_WORK_DIR;
static const char pcShellRestore[]  = "cp " RPI_BACKUP_DIR RPI_BASE_NAME ".* " RPI_WORK_DIR;
static const char pcShellBackup[]   = "cp " RPI_WORK_DIR   RPI_BASE_NAME ".* " RPI_BACKUP_DIR;

//
// Index list to global Arguments G_xxxx
//
static const PARARGS stGlobalParameters[] =
{
//
//     ________iFunction_________      pcJson,     pcHtml   pcOption    iValueOffset                  iValueSize     iChangedOffset
//    JSN_TXT|PAR__STI|...|PAR____,     "Width",    "w=",    "-w",       offsetof(RPIMAP, G_pcWidth),  MAX_PARM_LEN,  offsetof(RPIMAP, G_ubWidthChanged)
//   
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    {b,c,d,e,f,g,h},
#include "par_defs.h"
#undef EXTRACT_PAR
   {     0,                            NULL,       NULL,    NULL,       0,                            0,             0 }
};

//
// These global settings are being initialized on EVERY RESTART of the Apps.
//
static const GLODEFS stDefaultParameters[] =
{
//    iFunction   pcDefault   iChanged iChangedOffset iValueOffset   iGlobalSize
//
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    {b,j,i,h,f,g},
#include "par_defs.h"   
#undef   EXTRACT_PAR
   {  0,          NULL,       0,       0,             0,            0  }
};

//
// PID list
//
static const PIDL stDefaultPidList[] =
{
// Pid-file:   ePid, pcPid, pcHelp
// PIDL:       tPid, iFlag, iNotify, pcName, pcHelp
//
#define  EXTRACT_PID(a,b,c)   {0,0,0,b,c},
#include "par_pids.h"
#undef   EXTRACT_PID
};

/*------  Local functions separator -----------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   GLOBAL_Init
// Purpose:    Init all global variables
//
// Parms:      
// Returns:    TRUE if mmap was found correctly
// Note:       CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
//
bool GLOBAL_Init(void)
{
   bool           fCc=FALSE;
   const GLODEFS *pstGlobals=stDefaultParameters;
   char          *pcValue;
   int            iResetLength;
   u_int8        *pubReset;
   u_int8        *pubChanged;
   
   //
   // (Try to) copy the mmap file from SD to RAM disk
   // Create the global MMAP mapping
   //
   global_RestoreMapfile();
   fCc = global_CreateMap();

   //================================================================================
   // Mutex is OKee: LOGs are permitted from here on
   //================================================================================
   //
   // Reset protected global settings every restart :
   //    G_iResetStart ... G_iResetEnd
   //
   pubReset     = (u_int8 *)pstMap + offsetof(RPIMAP, G_iResetStart);
   iResetLength = offsetof(RPIMAP, G_iResetEnd) - offsetof(RPIMAP, G_iResetStart);
   PRINTF1("GLOBAL_Init: Reset MAP %d reset-parm bytes" CRLF, iResetLength);
   GEN_MEMSET(pubReset, 0x00, iResetLength);
   // Mark start & end
   pstMap->G_iResetStart = 0x66666666;
   pstMap->G_iResetEnd   = 0x99999999;
   //
   // Load global settings every restart :
   //
   while(pstGlobals->pcDefault)
   {
      if(pstGlobals->iFunction & WB)
      {
         //
         // Vars marked WB will be initialized with its default setting on every start !
         //
         pcValue = (char *)pstMap + pstGlobals->iValueOffset;
         GEN_STRNCPY(pcValue, pstGlobals->pcDefault, pstGlobals->iGlobalSize);
         if(pstGlobals->iChangedOffset >= 0)
         {
            pubChanged  = (u_int8 *)pstMap + pstGlobals->iChangedOffset;
            *pubChanged = pstGlobals->iChanged;
            PRINTF3("GLOBAL_Init: RpiMap[%04d] Chg=%d Value = <%s>" CRLF, pstGlobals->iValueOffset, *pubChanged, pcValue);
         }
         else
         {
            PRINTF2("GLOBAL_Init: RpiMap[%04d] Chg=? Value = <%s>" CRLF, pstGlobals->iValueOffset, pcValue);
         }
      }
      pstGlobals++;
   }
   global_PidsInit();
   return(fCc);
}

//
// Function:   GLOBAL_ExpandMap
// Purpose:    Expand the current mapfile and save to new file
//
// Parms:      Expand zero-reset size, End-size
// Returns:    
// Note:       
//
void GLOBAL_ExpandMap(int iZero, int iSpare)
{
   int         i, iFd, iSize;
   u_int8     *pubSrc;
   const char  cZero[2]={0,0};

   iFd = safeopen2(RPI_MAP_NEWPATH, O_RDWR|O_CREAT|O_TRUNC, 0640);
   //
   // Copy   MMAP including reset-parms
   // Expand MMAP reset-parms (if required)
   // Copy   MMAP static parms
   // Expand MMAP spare static parms (if required)
   // Add    Signature-2 
   // Add    "EOF"

   // Copy MMAP including reset-parms
   pubSrc   = (u_int8 *)pstMap;
   iSize    = offsetof(RPIMAP, G_iResetEnd);
   safewrite(iFd, (char *)pubSrc, iSize);

   // Expand MMAP reset-parms (if required)
   if(iZero > 0)
   {
      for(i=0; i<iZero; i++) safewrite(iFd, cZero, 1);
   }
   PRINTF1("GLOBAL_Expand(): Added MMAP %d reset-parm bytes" CRLF, iZero);

   // Copy MMAP static parms
   pubSrc += iSize;
   iSize   = offsetof(RPIMAP, G_iSignature2) - iSize;
   safewrite(iFd, (char *)pubSrc, iSize);
   
   // Expand MMAP spare static parms (if required)
   if(iSpare > 0)
   {
      for(i=0; i<iSpare; i++) safewrite(iFd, cZero, 1);
   }
   PRINTF1("GLOBAL_Expand(): Added MMAP %d static-spare bytes" CRLF, iSpare);

   // Add Signature-2 
   // Add "EOF"
   safewrite(iFd, (char *)&pstMap->G_iSignature2, sizeof(int));
   safewrite(iFd, "EOF", 4);
   safeclose(iFd);
}

//
// Function:   GLOBAL_RestoreDefaults
// Purpose:    Restore global variables
//
// Parms:      
// Returns:    
// Note:       
//
void GLOBAL_RestoreDefaults(void)
{
   const GLODEFS *pstGlobals = stDefaultParameters;
   char          *pcValue;
   u_int8        *pubChanged;

   //
   // Restore global settings
   //
   while(pstGlobals->pcDefault)
   {
      pcValue = (char *)pstMap + pstGlobals->iValueOffset;
      GEN_STRNCPY(pcValue, pstGlobals->pcDefault, pstGlobals->iGlobalSize);
      if(pstGlobals->iChangedOffset >= 0)
      {
         pubChanged  = (u_int8 *)pstMap + pstGlobals->iChangedOffset;
         *pubChanged = pstGlobals->iChanged;
         //GEN_PRINTF("GLOBAL_RestoreDefaults: RpiMap[%04d] Chg=%d Value = <%s>" CRLF, pstGlobals->iValueOffset, *pubChanged, pcValue);
      }
      else
      {
         //GEN_PRINTF("GLOBAL_RestoreDefaults: RpiMap[%04d] Chg=? Value = <%s>" CRLF, pstGlobals->iValueOffset, pcValue);
      }
      pstGlobals++;
   }
}

//
// Function:   GLOBAL_Lock
// Purpose:    Lock global mutex
//
// Parms:      
// Returns:    0=OKee, else error
// Note:       
//
int GLOBAL_Lock(void)
{
   int   iCc;

   if( (iCc = pthread_mutex_lock(&pstMap->G_tMutex)) )
   {
      PRINTF2("GLOBAL_Lock(): mutex ERROR %d (%s)", iCc, strerror(iCc));
   }
   return(iCc);
}

//
// Function:   GLOBAL_Unlock
// Purpose:    Unlock global mutex
//
// Parms:      
// Returns:    0=OKee, else error
// Note:       
//
int GLOBAL_Unlock(void)
{
   int   iCc;
   
   if( (iCc = pthread_mutex_unlock(&pstMap->G_tMutex)) )
   {
      PRINTF2("GLOBAL_Unlock(): mutex ERROR %d (%s)", iCc, strerror(iCc));
   }
   return(iCc);
}

//
// Function:   GLOBAL_UpdateSecs
// Purpose:    Count seconds
//
// Parms:      
// Returns:    
// Note:       
//
void GLOBAL_UpdateSecs(void)
{
   pstMap->G_ulSecondsCounter++;
   //PRINTF1("GLOBAL_UpdateSecs: 0x%lX" CRLF, pstMap->G_ulSecondsCounter);
}

//
// Function:   GLOBAL_ReadSecs
// Purpose:    Return seconds counter
//
// Parms:      
// Returns:    Running seconds counter
// Note:       
//
u_int32 GLOBAL_ReadSecs(void)
{
   //PRINTF1("GLOBAL_ReadSecs: 0x%lX" CRLF, pstMap->G_ulSecondsCounter);
   return(pstMap->G_ulSecondsCounter);
}

//
//  Function:  GLOBAL_GetTraceCode
//  Purpose:   Retrieve and update the tracecode
//
//  Parms:     
//  Returns:   Tracecode 
//
int GLOBAL_GetTraceCode()
{
   int   iTrace=0;

   if(pstMap)
   {
      GLOBAL_Lock();
      iTrace = ++(pstMap->G_iTraceCode);
      GLOBAL_Unlock();
   }
   return(iTrace);
}

//
//  Function:  GLOBAL_ConvertDebugMask
//  Purpose:   Convert G_pcDebugMask <---> G_ulDebugMask 
//
//  Parms:     TRUE = Ascii --> u_init32
//  Returns:   Debug mask 
//
u_int32 GLOBAL_ConvertDebugMask(bool fAsciiToInt)
{
   if(fAsciiToInt)
   {
      pstMap->G_ulDebugMask = strtoul(pstMap->G_pcDebugMask, NULL, 16);
      PRINTF1("GLOBAL_ConvertDebugMask():A-->I:Mask=0x%lx" CRLF, pstMap->G_ulDebugMask);
   }
   else
   {
      GEN_SNPRINTF(pstMap->G_pcDebugMask, MAX_PARM_LEN, "%lx", pstMap->G_ulDebugMask);
      PRINTF1("GLOBAL_ConvertDebugMask():I-->A:Mask=%s" CRLF, pstMap->G_pcDebugMask);
   }
   return(pstMap->G_ulDebugMask);
}

//
//  Function:  GLOBAL_GetDebugMask
//  Purpose:   Retrieve the current debug mask
//
//  Parms:     
//  Returns:   Mask
//
u_int32 GLOBAL_GetDebugMask(void)
{
   return(pstMap->G_ulDebugMask);
}

//
//  Function:  GLOBAL_SetDebugMask
//  Purpose:   Update the current debug mask
//
//  Parms:     New mask
//  Returns:    
//
void GLOBAL_SetDebugMask(u_int32 ulMask)
{
   pstMap->G_ulDebugMask = ulMask;
   PRINTF1("GLOBAL_SetDebugMask():Mask=0x%lx" CRLF, pstMap->G_ulDebugMask);
}

//
//  Function:  GLOBAL_GetHostName
//  Purpose:   Retrieve the global host name. If not stored yet, obtain it first
//
//  Parms:     
//  Returns:   Pointer to name (Fixed name on error)
//
char *GLOBAL_GetHostName(void)
{
   const char *pcDefaultName = "PatrnRPi-Default";
   char       *pcName;
   int         iErr;

   pcName = pstMap->G_pcHostname;
   if(GEN_STRLEN(pcName) == 0)
   {
      iErr = gethostname(pcName, MAX_URL_LEN);
      if(iErr)
      {
         // gethostname failed, supply default name
         pcName = (char *)pcDefaultName;
      }
   }
   return(pcName);
}

//
// Function:   GLOBAL_Sync
// Purpose:    Sync the main file with the mapping
//
// Parms:      
// Returns:    TRUE if OKee
// Note:       
//
bool  GLOBAL_Sync(void)
{
  msync(pstMap, iGlobalMapSize, MS_SYNC);
  return(TRUE);
}

//
// Function:   GLOBAL_Close
// Purpose:    Close the main file with the mapping
//
// Parms:      
// Returns:    TRUE if OKee
// Note:       
//
bool  GLOBAL_Close(void)
{
   pthread_mutex_destroy(&pstMap->G_tMutex);
   munmap(pstMap, iGlobalMapSize); 
   safeclose(iFdMap);
   global_SaveMapfile();
   return(TRUE);
}

//
//  Function:   GLOBAL_Share
//  Purpose:    Start sharing the mmap file
//
//  Parms:      
//  Returns:    True if OKee
//
bool GLOBAL_Share(void)
{
   bool     fCc=TRUE;

   pstMap = (RPIMAP *) mmap(NULL, iGlobalMapSize, PROT_READ|PROT_WRITE, MAP_SHARED, iFdMap, 0);
   if(pstMap == MAP_FAILED)
   {
      LOG_Report(0, "MAP", "Re-mapping failed");
      PRINTF("GLOBAL_Share(): remapping failed" CRLF);
      fCc = FALSE;
   }
   else
   {
      PRINTF1("GLOBAL_Share(): Map=%p" CRLF, pstMap);
   }
   return(fCc);
}

//
//  Function:  GLOBAL_Notify
//  Purpose:   Notify threads using a signal
//
//  Parms:     Pid-Enum, Nfy flag, Signal
//  Returns:   Remainder notification
//  Note:      
//
int GLOBAL_Notify(int ePid, int iMsg, int iSignal)
{
   int   iFlag=0;
   pid_t tPid=GLOBAL_PidGet(ePid);

   if(tPid > 0)
   {
      iFlag = GLOBAL_SetSignalNotification(ePid, iMsg);
      GLOBAL_SignalPid(tPid, iSignal);
   }
   else PRINTF2("GLOBAL_Notify(): BAD PID %d=%d" CRLF, ePid, tPid);
   return(iFlag);
}

// 
// Function:   GLOBAL_Signal
// Purpose:    Signal a PID_xx
// 
// Parameters: PIDT PID_xxx, signal
// Returns:    
// Note:       
// 
void GLOBAL_Signal(PIDT ePid, int iSignal)
{
   pid_t tPid;

   tPid = GLOBAL_PidGet(ePid);
   if(tPid > 0) kill(tPid, iSignal);
}

// 
// Function:   GLOBAL_SignalPid
// Purpose:    Signal a PID
// 
// Parameters: Pid, signal
// Returns:    
// Note:       
// 
void GLOBAL_SignalPid(pid_t tPid, int iSignal)
{
   if(tPid > 0) kill(tPid, iSignal);
}

// 
// Function:   GLOBAL_SegmentationFault
// Purpose:    Store segv cause
// 
// Parameters: 
// Returns:    
// 
void GLOBAL_SegmentationFault(const char *pcFile, int iLineNr)
{
   GEN_STRNCPY(pstMap->G_pcSegFaultFile, pcFile, MAX_URL_LEN);
   pstMap->G_iSegFaultLine = iLineNr;
   pstMap->G_iSegFaultPid  = getpid();
}

//
//  Function:  GLOBAL_HostNotification
//  Purpose:   Signal completion
//
//  Parms:     Message flag
//  Returns:   New notification
//  Note:      
//
int GLOBAL_HostNotification(int iMsg)
{
   int   iFlag;
   
   iFlag = GLOBAL_SetSignalNotification(PID_HOST, iMsg);
   //LOG_Report(0, "GLB", "GLOBAL_HostNotification():0x%08X now 0x%08X", iMsg, iFlag);
   GLOBAL_Signal(PID_HOST, SIGUSR1);
   return(iFlag);
}

//
//  Function:  GLOBAL_GetSignalNotification
//  Purpose:   Get the SIGUSRx notification flag. Clears the flag after reading.
//
//  Parms:     Flag
//  Returns:   New flag
//
//
bool GLOBAL_GetSignalNotification(int ePid, int iFlag)
{
   bool  fNfy;
   int   iNotify;

   GLOBAL_Lock();
   iNotify = pstMap->G_stPidList[ePid].iNotify;
   //
   if(iNotify & iFlag) fNfy = TRUE;
   else                fNfy = FALSE;
   //
   pstMap->G_stPidList[ePid].iNotify &= ~iFlag;
   GLOBAL_Unlock();
   return(fNfy);
}

//
//  Function:  GLOBAL_SetSignalNotification
//  Purpose:   Set the SIGUSRx notification flag
//
//  Parms:     Flag
//  Returns:   New Flag
//
int GLOBAL_SetSignalNotification(int ePid, int iFlag)
{
   GLOBAL_Lock();
   pstMap->G_stPidList[ePid].iNotify |= iFlag;
   iFlag = pstMap->G_stPidList[ePid].iNotify;
   GLOBAL_Unlock();
   return(iFlag);
}

//
// Function:   GLOBAL_PidCheckGuards
// Purpose:    Check if all threads are still active
//             
// Parms:     
// Returns:    TRUE if OKee
// Note:       G_stPidList[i]->pid_t       tPid   : The PID
//                             int         iFlag  : The guard marker
//                             const char *pcName : The PID short-name
//                             const char *pcHelp : The PID long-name
//
bool GLOBAL_PidCheckGuards(void)
{
   bool     fCc=TRUE;
   int      iFlag, iGuard;
   int      ePid;
   PIDL    *pstPidl;
   //
   if(pstMap)
   {
      GLOBAL_Lock();
      iGuard = pstMap->G_iGuards;
      GLOBAL_Unlock();
      //
      for(ePid=0; ePid<NUM_PIDT; ePid++) 
      {
         pstPidl = &(pstMap->G_stPidList[ePid]);
         if( (pstPidl->tPid > 0) && (pstMap->G_stPidList[ePid].iFlag != 0) )
         {
            //
            // This thread seems to be running and has an Init-marker:
            // Verify that the flag is active again
            //
            iFlag = iGuard & pstMap->G_stPidList[ePid].iFlag;
            if(iFlag) 
            {
               PRINTF1("GLOBAL_PidCheckGuards:%s OKee" CRLF, pstPidl->pcHelp);
            }
            else
            {
               //
               // Thread did NOT respond in time: send final wakeup call
               //
               GLOBAL_SetSignalNotification(ePid, GLOBAL_GRD_RUN);
               GLOBAL_SignalPid(pstPidl->tPid, SIGUSR1);
               LOG_Report(0, "GLB", "GLOBAL_PidCheckGuards():Check Guard: ERROR %s", GLOBAL_PidGetHelp(ePid));
               PRINTF1("GLOBAL_PidCheckGuards:%s NOT set" CRLF, pstPidl->pcHelp);
               fCc = FALSE;
            }
         }
         else
         {
            PRINTF4("GLOBAL_PidCheckGuards:(G=0x%04X, F=0x%04X) : %5d-%s not guarded" CRLF, 
                        iGuard,
                        pstMap->G_stPidList[ePid].iFlag,
                        pstPidl->tPid, 
                        pstPidl->pcHelp);
         }
      }
   }
   return(fCc);
}

//
// Function:   GLOBAL_PidClearGuards
// Purpose:    Clear all guard markers
//
// Parms:      
// Returns:    
// Note:       
//
void GLOBAL_PidClearGuards(void)
{
   GLOBAL_Lock();
   pstMap->G_iGuards = 0;
   GLOBAL_Unlock();
}

//
// Function:   GLOBAL_PidSaveGuard
// Purpose:    Save the correct guard marker
//
// Parms:      PID enum, marker
// Returns:    
//
void GLOBAL_PidSaveGuard(PIDT ePid, int iMarker)
{
   GLOBAL_Lock();
   if(pstMap && (ePid < NUM_PIDT) )
   {
      pstMap->G_stPidList[ePid].iFlag = iMarker;
   }
   GLOBAL_Unlock();
}

//
// Function:   GLOBAL_PidGetGuard
// Purpose:    Get the guard marker for this thread
//             
// Parms:      Thread ID
// Returns:    TRUE if OKee
// Note:       G_stPidList[i]->pid_t       tPid   : The PID
//                             int         iFlag  : The guard marker
//                             const char *pcName : The PID short-name
//                             const char *pcHelp : The PID long-name
//
bool GLOBAL_PidGetGuard(PIDT ePid)
{
   bool  fMarker=TRUE;
   int   iFlag;

   if(pstMap && (ePid < NUM_PIDT) )
   {
      GLOBAL_Lock();
      iFlag = pstMap->G_stPidList[ePid].iFlag;
      //
      // Get the flag
      //
      if( (pstMap->G_iGuards & iFlag) == 0)
      {
         fMarker = FALSE;
      }
      GLOBAL_Unlock();
   }
   return(fMarker);
}

//
// Function:   GLOBAL_PidSetGuard
// Purpose:    Set the correct guard marker
//
// Parms:      PID enum
// Returns:    
// Note:       This function should be called regularly (at least within GUARD_SECS
//             secs) in order to SET their GLOBAL_xxx_INI flag in G_iGuard.
//             If they fail (Current time is later than the previous check) the
//             event will be counted. At the end of the day, the counts of all
//             active threads should be lower that a threshold. If not the service 
//             will be restarted by the WatchDog, who is triggered by PID_HOST 
//             through a SIGUSR1 (Restart) or SIGUSR2 (Reboot).
//
//             G_iGuards
//             G_stPidList[i]->pid_t       tPid   : The PID
//                             int         iFlag  : The guard marker
//                             const char *pcName : The PID short-name
//                             const char *pcHelp : The PID long-name
//
//
bool GLOBAL_PidSetGuard(PIDT ePid)
{
   bool  fChanged=FALSE;
   int   iFlag, iGuard;

   if(pstMap && (ePid < NUM_PIDT) )
   {
      GLOBAL_Lock();
      iGuard = pstMap->G_iGuards;
      iFlag  = pstMap->G_stPidList[ePid].iFlag;
      //
      // Set the flag only if necessary
      //
      if( iFlag && ((iGuard & iFlag) == 0) )
      {
         iGuard |= iFlag;
         pstMap->G_iGuards = iGuard;
         fChanged = TRUE;
      }
      GLOBAL_Unlock();
   }
   if(fChanged) PRINTF3("GLOBAL_PidSetGuard():%5d-0x%08X(%s)" CRLF, pstMap->G_stPidList[ePid].tPid, iGuard, pstMap->G_stPidList[ePid].pcHelp);
   return(fChanged);
}

//
//  Function:  GLOBAL_PidGet
//  Purpose:   Retrieve thread pid
//
//  Parms:     PID enum
//  Returns:   tPid
//
pid_t GLOBAL_PidGet(PIDT ePid)
{
   pid_t tPid=0;

   if(pstMap && (ePid != -1) && (ePid < NUM_PIDT) )
   {
      GLOBAL_Lock();
      tPid = pstMap->G_stPidList[ePid].tPid;
      GLOBAL_Unlock();
   }
   return(tPid);
}

//
//  Function:  GLOBAL_PidGetName
//  Purpose:   Retrieve thread pid name
//
//  Parms:     PID enum
//  Returns:   name
//
const char *GLOBAL_PidGetName(PIDT ePid)
{
   const char *pcName=NULL;

   if(pstMap && (ePid != -1) && (ePid < NUM_PIDT) )
   {
      pcName = pstMap->G_stPidList[ePid].pcName;
   }
   return(pcName);
}

//
//  Function:  GLOBAL_PidGetHelp
//  Purpose:   Retrieve thread pid help
//
//  Parms:     PID enum
//  Returns:   Help
//
const char *GLOBAL_PidGetHelp(PIDT ePid)
{
   const char *pcHelp=NULL;

   if(pstMap && (ePid != -1) && (ePid < NUM_PIDT) )
   {
      pcHelp = pstMap->G_stPidList[ePid].pcHelp;
   }
   return(pcHelp);
}

//
//  Function:  GLOBAL_PidPut
//  Purpose:   Store thread pid
//
//  Parms:     PID enum, Pid
//  Returns:   tPid
//
bool GLOBAL_PidPut(PIDT ePid, pid_t tPid)
{
   bool fCc = FALSE;

   if(pstMap && (ePid != -1) && (ePid < NUM_PIDT) )
   {
      pstMap->G_stPidList[ePid].tPid = tPid;
      PRINTF2("GLOBAL_PidPut: Pid %s=%d" CRLF, GLOBAL_PidGetName(ePid), tPid);
      fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   GLOBAL_PidsLog
// Purpose:    Log all threads
//
// Parms:      
// Returns:    Nr of threads running
// Note:       
//
int GLOBAL_PidsLog()
{
   int      ePid, iRunning=0;
   pid_t    tPid;
   PIDL    *pstPidl;

   if(pstMap)
   {
      //
      // Log PIDs
      //
      for(ePid=0; ePid<NUM_PIDT; ePid++) 
      {
         pstPidl = &pstMap->G_stPidList[ePid];
         tPid    = pstPidl->tPid;
         if(tPid > 0) iRunning++;
         //
         LOG_Report(0, "GLB", "%s = %5d (%s)", pstPidl->pcName, tPid, pstPidl->pcHelp);
      }
   }
   else
   {
      LOG_Report(0, "GLB", "PID Log: No Mapfile");
   }
   return(iRunning);
}

//
// Function:   GLOBAL_PidsTerminate
// Purpose:    Terminate all threads
//
// Parms:      Timeout in mSecs
// Returns:    Nr of threads still running
// Note:       Caller should NOT kill itself !
//
int GLOBAL_PidsTerminate(int iMsecTimeout)
{
   int      ePid, iRunning=0;
   pid_t    tPid;

   if(pstMap)
   {
      //
      // First persuade all threads (if any) to terminate peacefully
      //
      for(ePid=0; ePid<NUM_PIDT; ePid++)
      {
         tPid = pstMap->G_stPidList[ePid].tPid;
         if( (tPid > 0) && (tPid != getpid()) )
         {
            GEN_KillProcess(tPid, DO_FRIENDLY, 0);
         }
      }
      //
      // Force all threads (if any) to terminate if not voluntarily
      //
      for(ePid=0; ePid<NUM_PIDT; ePid++)
      {
         tPid = pstMap->G_stPidList[ePid].tPid;
         if( (tPid > 0) && (tPid != getpid()) )
         {
            if( GEN_WaitProcessTimeout(tPid, iMsecTimeout) == 1)
            {
               iRunning++;
               PRINTF1("GLOBAL_PidsTerminate: %s NOT terminated yet" CRLF, GLOBAL_PidGetName(ePid));
               GEN_KillProcess(tPid, NO_FRIENDLY, 500);
            }
            else
            {
               PRINTF1("GLOBAL_PidsTerminate: %s terminated" CRLF, GLOBAL_PidGetName(ePid));
            }
            pstMap->G_stPidList[ePid].tPid = 0;
         }
      }
   }
   return(iRunning);
}

//
//  Function:  GLOBAL_GetMallocs
//  Purpose:   Return the global malloc count
//  Parms:     
//
//  Returns:   Count
//
int GLOBAL_GetMallocs(void)
{
   return(pstMap->G_iMallocs);
}

//
//  Function:  GLOBAL_PutMallocs
//  Purpose:   
//  Parms:     Mallocs
//
//  Returns:
//
void GLOBAL_PutMallocs(int iMalloc)
{
   pstMap->G_iMallocs = iMalloc;
}

//
// Function:   GLOBAL_GetParameters
// Purpose:    Retrieve the camera parameter list
//
// Parms:      
// Returns:    The List
//
const PARARGS *GLOBAL_GetParameters(void)
{
   return(stGlobalParameters);
}

// 
// Function:   GLOBAL_GetParameter
// Purpose:    Retrieve a single parameter from the global list
// 
// Parameters: Parameter enum
// Returns:    Parameter^ or NULL if not found
// 
// 
char *GLOBAL_GetParameter(GLOPAR tParm)
{
   char    *pcValue=NULL;

   if(tParm < NUM_GLOBAL_DEFS)
   {
      pcValue = (char *)pstMap + stGlobalParameters[tParm].iValueOffset;
   }
   return(pcValue);
}

// 
// Function:   GLOBAL_GetParameterSize
// Purpose:    Retrieve the size of a single parameter from the global list
// 
// Parameters: Parameter enum
// Returns:    Size
// 
// 
int GLOBAL_GetParameterSize(GLOPAR tParm)
{
   int   iSize=0;

   if(tParm < NUM_GLOBAL_DEFS)
   {
      iSize = stGlobalParameters[tParm].iValueSize;
   }
   else PRINTF1("GLOBAL-GetParameterSize():Unknown parameter %d" CRLF, tParm);
   return(iSize);
}

//
//  Function:  GLOBAL_GetLog
//  Purpose:   Retrieve the LOG file struct^
//
//  Parms:     
//  Returns:   LOG struct
//
GLOG *GLOBAL_GetLog()
{
   return(&(pstMap->G_stLog));
}

// 
// Function:   GLOBAL_CheckDelete
// Purpose:    Check the status of the delete parameter
// 
// Parameters: Full pathname of file to delete
// Returns:    TRUE if delete parameter found and file deleted
// 
bool GLOBAL_CheckDelete(char *pcPath)
{
   bool  fDelete=FALSE;
   int   iCc;

   if(GEN_STRNCMPI(pstMap->G_pcDelete, "yes", 3) == 0)
   {
      iCc = remove(pcPath);
      if(iCc == 0)
      {
         fDelete = TRUE;
         GEN_STRCPY(pstMap->G_pcDelete, "no");
      }
   }
   return(fDelete);
}


/*------  Local functions separator -----------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   global_CreateMap
// Purpose:    Read the mapping from the file
// Parms:
//
// Returns:    TRUE if mmap was OKee
// Note:       Make sure to setup the GLOBAL mutex as soon as we have a valid MAP file
//             before using functions that make use of the mutex !
//
//             Mutex init types:
//                PTHREAD_MUTEX_NORMAL
//                PTHREAD_MUTEX_ERRORCHECK
//                PTHREAD_MUTEX_RECURSIVE
//                PTHREAD_MUTEX_DEFAULT
//
//=============================================================================
//             On critical errors this function with exit() theApp !
//=============================================================================
//
static bool global_CreateMap(void)
{
   pthread_mutexattr_t tMutexAttr;
   bool  fCc=FALSE;
   int   iCc;
   //
   // Open the mmap file
   //
   global_Open(pcMapFile, sizeof(RPIMAP));
   if(pstMap == NULL ) 
   {
      PRINTF1("global_CreateMap(): MMAP %s open error" CRLF, pcMapFile);
      exit(EXIT_CC_GEN_ERROR);
   }
   //
   if((pstMap->G_iSignature1 != DATA_VALID_SIGNATURE)
        ||
      (pstMap->G_iSignature2 != DATA_VALID_SIGNATURE)
        ||
      (pstMap->G_iVersionMajor != DATA_VERSION_MAJOR))
   {
      //
      // Bad MAP file !
      //
      global_InitMemory(RMAP_CLEAR);
      GLOBAL_RestoreDefaults();
      //=======================================================================
      // Init the global mutex
      //=======================================================================
      pthread_mutexattr_init(&tMutexAttr);
      pthread_mutexattr_settype(&tMutexAttr, PTHREAD_MUTEX_ERRORCHECK);
      if( (iCc = pthread_mutex_init(&pstMap->G_tMutex, &tMutexAttr)) != 0) 
      {
         PRINTF2("GLOBAL_Init(): mutex init ERROR %d (%s)", iCc, strerror(iCc));
         exit(EXIT_CC_GEN_ERROR);
      }
      PRINTF("global_CreateMap():Mutex created" CRLF);
      LOG_Report(0, "MAP", "MAP file signature or DB version mismatch, creating new Mapping:");
      LOG_Report(0, "MAP", "Sig-1 = %08x", pstMap->G_iSignature1);
      LOG_Report(0, "MAP", "Sig-2 = %08x", pstMap->G_iSignature2);
      LOG_Report(0, "MAP", "Vrs   = v%d.%d", pstMap->G_iVersionMajor, pstMap->G_iVersionMinor);
      //
      PRINTF("global_CreateMap():MAP file signature or DB version mismatch, creating new Mapping:" CRLF);
      PRINTF1("global_CreateMap():New signature-1 = 0x%08x" CRLF, pstMap->G_iSignature1);
      PRINTF1("global_CreateMap():New signature-2 = 0x%08x" CRLF, pstMap->G_iSignature2);
      PRINTF2("global_CreateMap():New DB version  = v%d.%d" CRLF, pstMap->G_iVersionMajor, pstMap->G_iVersionMinor);
   }
   else
   {
      global_InitMemory(RMAP_RESTART);
      //=======================================================================
      // Init the global mutex
      //=======================================================================
      pthread_mutexattr_init(&tMutexAttr);
      pthread_mutexattr_settype(&tMutexAttr, PTHREAD_MUTEX_ERRORCHECK);
      if( (iCc = pthread_mutex_init(&pstMap->G_tMutex, &tMutexAttr)) != 0) 
      {
         PRINTF2("GLOBAL_Init(): mutex init ERROR %d (%s)", iCc, strerror(iCc));
         exit(EXIT_CC_GEN_ERROR);
      }
      PRINTF("global_CreateMap():Mutex created" CRLF);
      PRINTF2("global_CreateMap():MAP file OKee, v%d.%d" CRLF, pstMap->G_iVersionMajor, pstMap->G_iVersionMinor);
      fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   global_InitMemory
// Purpose:    Init the MAP memory
//
// Parms:      Cmd
// Returns:
// Note:       CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
//
static void global_InitMemory(RMAPSTATE tState)
{
  switch(tState)
  {
     case RMAP_CLEAR:
        GEN_MEMSET(pstMap, 0, sizeof(RPIMAP));
        //
        pstMap->G_iSignature1      = DATA_VALID_SIGNATURE;
        pstMap->G_iSignature2      = DATA_VALID_SIGNATURE;
        pstMap->G_iVersionMajor    = DATA_VERSION_MAJOR;
        pstMap->G_iVersionMinor    = DATA_VERSION_MINOR;
        break;

     case RMAP_SYNC:
     case RMAP_RESTART:
     default:
        break;
  }
}

//
// Function:   global_PidsInit
// Purpose:    Init all threads
//
// Parms:      
// Returns:    
// Note:       
//
static void global_PidsInit(void)
{
   int      ePid;

   if(pstMap)
   {
      //
      // Show PIDs
      //
      for(ePid=0; ePid<NUM_PIDT; ePid++)
      {
         pstMap->G_stPidList[ePid] = stDefaultPidList[ePid];
         PRINTF1("global_PidsInit():Setup %s" CRLF, pstMap->G_stPidList[ePid].pcHelp);
      }
   }
}

//
// Function:   global_Open
// Purpose:    Open the main file for the mapping
//
// Parms:      Filename, size
// Returns:    
// Note:       We do NOT setup the GLOBAL mutex since we do NOT have a valid MAP file !
//             We CANNOT use functions that make use of the mutex here !
//
//
static void global_Open(const char *pcName, int iMapSize)
{
  pstMap      = NULL;
  iGlobalMapSize = 0;

  iFdMap = open(pcName, O_RDWR|O_CREAT, 0640);
  //
  // Make sure the file has the minimum size!
  //
  lseek(iFdMap, iMapSize, SEEK_SET);
  write(iFdMap, "EOF", 4);
  //
  pstMap = (RPIMAP *) mmap(NULL, iMapSize, PROT_READ|PROT_WRITE, MAP_SHARED, iFdMap, 0);
  if(pstMap == MAP_FAILED)
  {
     PRINTF("global_Open():Mapping failed" CRLF);
  }
  else
  {
     iGlobalMapSize = iMapSize;
  }
}

//
// Function:   global_SaveMapfile
// Purpose:    Copy the MMAP file from RAM disk to SD 
//             cp /usr/local/share/rpi/spicam.map /mnt/rpicache/spicam.map
// Parms:     
// Returns:    
// Note:       CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
//
static void global_SaveMapfile()
{
   char  cSrc, cDst;

   global_GetFileInfo((char *)pcDirRestoreSrc, &cSrc);
   global_GetFileInfo((char *)pcDirRestoreDst, &cDst);
   //
   if( (cSrc == 'D') && (cDst == 'D') )
   {
      system(pcShellBackup);
   }
}

//
// Function:   global_RestoreMapfile
// Purpose:    Copy the MMAP file from SD to RAM disk
//             cp /usr/local/share/rpi/rpislim.map /mnt/rpicache/rpislim.map
// Parms:     
// Returns:    
// Note:       CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
//
static int global_RestoreMapfile()
{
   int   iCc=1; 
   char  cSrc=0, cDst=0;

   global_GetFileInfo((char *)pcDirRestoreSrc, &cSrc);
   global_GetFileInfo((char *)pcDirRestoreDst, &cDst);
   //
   if( (cSrc == 'D') && (cDst == 'D') )
   {
      PRINTF1("global_RestoreMapfile(): restore %s.*" CRLF, pcShellRestore);
      iCc = system(pcShellRestore);
      if(iCc) 
      {
         PRINTF2("global_RestoreMapfile():Error-%d: Restore %s.*" CRLF, errno, pcShellRestore);
      }
   }
   else
   {
      if(cSrc != 'D') PRINTF1("global_RestoreMapfile():Dir [%s] not found" CRLF, pcDirRestoreSrc);
      if(cDst != 'D') PRINTF1("global_RestoreMapfile():Dir [%s] not found" CRLF, pcDirRestoreDst);
   }
   return(iCc);
}

// 
// Function    : global_GetFileInfo
// Description : Retrieve info about a file in this dir
// 
// Parameters  : Dir path, Char buffer for result
// Returns     : TRUE if OKee
// Note        : CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
// 
bool global_GetFileInfo(char *pcPath, char *pcChar)
{
   bool        fCc=TRUE;
   struct stat stStat;

   if(stat(pcPath, &stStat) == -1) return(FALSE);

   switch(stStat.st_mode & S_IFMT)
   {
      case S_IFDIR: 
         *pcChar = 'D';
         break;

      case S_IFREG:
         *pcChar = 'F';
         break;

      default:
         *pcChar = '?';
         break;
   }
   return(fCc);
}

