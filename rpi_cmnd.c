/*  (c) Copyright:  2018..2019  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_cmnd.c
 *  Purpose:            Archive command module
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    24 Jul 2018:      Created
 *    30 Jan 2019:      Unify report and log file names
 *    01 Feb 2019:      Send email always at days end     
 *    06 Feb 2019:      Add cl parms check
 *    16 Apr 2021:      Add Verbose LOG level
 *    06 Jun 2021:      Add flags to verbose arg
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "rpi_mail.h"
#include "cmd_mail.h"
#include "rpi_cmnd.h"

//#define USE_PRINTF
//#undef  FEATURE_USE_PRINTF
//#define FEATURE_USE_STDOUT
#include <printf.h>


#if defined(__DATE__) && defined(__TIME__)
   static const char RPI_BUILT[] = __DATE__ " " __TIME__;
#else
   static const char RPI_BUILT[] = "Unknown";
#endif

typedef struct _arg_list_
{
   GLOPAR      ePar;
   PIKARG      eArg;
   const char *pcHelp;
}  ARGL;
//
static ARGL stArgList[] =
{
   // ePar              eArg                 pcHelp
   {  PAR_ARCHIVE,      ARG_PATH_ARCHIVE, "$a Archive Dir:               "    },
   {  PAR_LAST_VID,     ARG_LAST_VLOOP,   "$v Last Saved Path Video:     "    },
   {  PAR_LAST_THB,     ARG_LAST_TLOOP,   "$A Last Saved Path Thumbnail: "    },
   {  PAR_FIFO,         ARG_PATH_FIFO,    "$P FIFO Full Path:            "    },
   {  PAR_MIN_NOW,      ARG_MIN_NOW,      "$D Minute count now:          "    },
   {  PAR_MIN_DAWN,     ARG_MIN_DAWN,     "   Minute count dawn:         "    },
   {  PAR_MIN_SUNRISE,  ARG_MIN_SUNRISE,  "   Minute count sunrise:      "    },
   {  PAR_MIN_SUNSET,   ARG_MIN_SUNSET,   "   Minute count sunset:       "    },
   {  PAR_MIN_DUSK,     ARG_MIN_DUSK,     "   Minute count dusk:         "    },
   {  PAR_MOTION_STATE, ARG_MOTION_STATE, "$o Motion state:              "    },
   {  PAR_VERBOSE,      ARG_VERBOSE,      "   Flags and Verbose level    "    },
   {  PAR_LOG,          ARG_LOG_FILE,     "$G LOG file:                  "    } 
};
static int iNumArgList = sizeof(stArgList)/sizeof(ARGL);
//
static const char *pcInformTimeout  = "inform timeout 4";
static const char *pcPiKrellCamMask = "CCCC_YYYY-MM-DD_hh.mm.ss_T.xxxxxx";
static int         iFlags           = 0;
static int         iVerbose         = 0;
//    
// Local prototypes
//
static char   *cmd_FindFilename           (char *);
static int     cmd_ExtractData            (char, char *, char *, int);
static int     cmd_ExtractParameter       (PIKMSK, char *, char *, int);
static int     cmd_ArchiveMotionFifo      (void);
static int     cmd_ArchiveMotionLocal     (void);
static int     cmd_SkipArchiveMotion      (void);
static int     cmd_EmailArchiveMotion     (void);
static bool    cmd_DirectoryCreate        (char *);
static bool    cmd_DirectoryExists        (char *);
static bool    cmd_FileCopy               (char *, char *);
static int     cmd_InformFifo             (const char *, char *, char *);
//
static bool    cmd_SignalRegister         (sigset_t *);
static void    cmd_ReceiveSignalSegmnt    (int);
static void    cmd_ReceiveSignalInt       (int);
static void    cmd_ReceiveSignalTerm      (int);
static void    cmd_ReceiveSignalUser1     (int);
static void    cmd_ReceiveSignalUser2     (int);
//

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   CMD_Execute
// Purpose:    Run the Archive command thread
// 
// Parameters: Argument list, argument length, number of args
// Returns:    
// Note:       
// 
int CMD_Execute(char cArgList[NUM_ARGS][ARG_LENZ], int iArgLen, int iNrArgs)
{
   int         iSize, x, iCc=EXIT_CC_OKEE;
   sigset_t    tBlockset;
   char       *pcPar;
   char       *pcFile;
   char       *pcArg;
   char       *pcVerbose;
   char        cData[10];
   ARGL       *pstArg;

   if(cmd_SignalRegister(&tBlockset) == FALSE)
   {
      LOG_Report(errno, "CMD", "CMD-Execute():Exit Register ERROR:");
      return(EXIT_CC_GEN_ERROR);
   }
   //
   // The PiKrellCam arguments are being passed through the $x macros in .pikrellcam/pikrellcam.conf
   // stArgList maps these CLI arguments onto the G_pxXxxx variables.
   //
   PRINTF1("CMD-Execute():Setup PAR list with %d CmdLine args." CRLF, iNrArgs);
   if(iNrArgs <= NUM_ARGS)
   {
      for(x=0; x<iNumArgList; x++)
      {
         if(x < NUM_ARGS)
         {
            pstArg = &stArgList[x];
            pcPar  = GLOBAL_GetParameter(pstArg->ePar);
            iSize  = GLOBAL_GetParameterSize(pstArg->ePar);
            pcArg = cArgList[pstArg->eArg];
            GEN_STRNCPY(pcPar, pcArg, iSize);
            PRINTF3("CMD-Execute():%2d[%s] CLI=%s" CRLF, x, pstArg->pcHelp, pcArg);
            PRINTF3("CMD-Execute():%2d[%s] PAR=%s" CRLF, x, pstArg->pcHelp, pcPar);
         }
         else 
         {
            PRINTF1("CMD-Execute():Too many args (%d)" CRLF, iNumArgList);
            LOG_Report(0, "CMD", "CMD-Execute():Too many args (%d)", iNumArgList);
            break;
         }
      }
      //
      // Get flags and verbose level 0...?
      //
      pcVerbose = GLOBAL_GetParameter(PAR_VERBOSE);
      iVerbose  = atoi(pcVerbose);
      iFlags    = iVerbose & ~VERBOSE_LEVEL_MASK;
      iVerbose &= VERBOSE_LEVEL_MASK;
      PRINTF2("CMD-Execute():Flags=0x%X, Verbose Level=%d" CRLF, iFlags, iVerbose);
      //LOG_Report(0, "CMD", "CMD-Execute():Flags=0x%X, Verbose Level=%d", iFlags, iVerbose);
      //
      // LOG Args
      //
      if(iFlags & FLAGS_LOG_ARGS)
      {
         for(x=0; x<iNumArgList; x++)
         {
            pcPar = GLOBAL_GetParameter(stArgList[x].ePar);
            PRINTF3("CMD-Execute():Arg[%2d]=(%s): %s" CRLF, x, stArgList[x].pcHelp, pcPar);
            //LOG_Report(0, "CMD", "CMD-Execute():Arg[%2d]=(%s): %s", x, stArgList[x].pcHelp, pcPar);
         }
      }
      //
      // Archive the files
      //
      // Archive all loop-video files
      // PiKrellCam filenames have the format:
      //
      // loop_2018-07-27_11.15.38_a.xxx where
      //
      //    a = 0 for no motion
      //        m for motion trigger
      //  xxx = mp4    for video
      //        th.jpg for thumbnails
      //
      pcPar  = GLOBAL_GetParameter(PAR_LAST_VID);
      pcFile = cmd_FindFilename(pcPar);
      //
      PRINTF1("CMD-Execute():Video File=%s" CRLF, pcFile);
      //
      x = cmd_ExtractParameter(MASK_TYPE, pcFile, cData, 8);
      //
      switch(cData[0])
      {
         case 't':
            //
            // Test mode: skip archiving
            //
            LOG_Report(0, "CMD", "TEST: Version=%s now EXIT", VERSION);
            break;

         case 'm':
            //
            // Motion trigger: Archive either local or let PiKrellCam do it 
            //
            PRINTF("CMD-Execute():Motion detected" CRLF);
            if(iFlags & FLAGS_LOCAL_ARCH) iCc = cmd_ArchiveMotionLocal();
            else                          iCc = cmd_ArchiveMotionFifo();
            break;

         default:
            iCc = cmd_SkipArchiveMotion();
            break;
      }
   }
   else
   {
      PRINTF2("CMD-Execute():Wrong number of arguments %d i.o. %d)" CRLF, iNrArgs, NUM_ARGS);
      LOG_Report(0, "CMD", "CMD-Execute():Wrong number of arguments %d i.o. %d)", iNrArgs, NUM_ARGS);
   }
   return(iCc);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   cmd_FindFilename
// Purpose:    Find the filename from the pikrellcam file
// 
// Parameters: Ptr to whole path
// Returns:    Ptr to filename
// Note:       f.i: Path -> /mnt/rpipix/loop_2018-07-27_11.15.38_0.mp4
//                  Return -------------^
//
static char *cmd_FindFilename(char *pcPath)
{
   char *pcFile=NULL;

   while(*pcPath)
   {
      if(*pcPath++ == '/') pcFile = pcPath;
   }
   return(pcFile);
}

// 
// Function:   cmd_ExtractParameter
// Purpose:    Extract data from the pikrellcam file
// 
// Parameters: Var type, file, dest, max_size
// Returns:    Dest act_size
// 
static int cmd_ExtractParameter(PIKMSK eMask, char *pcFile, char *pcDest, int iMaxSize)
{
   int   iNr=0;

   switch(eMask)
   {
      case MASK_CAPTION:
         iNr = cmd_ExtractData('C', pcFile, pcDest, iMaxSize);
         break;

      case MASK_YEAR:
         iNr = cmd_ExtractData('Y', pcFile, pcDest, iMaxSize);
         PRINTF1("cmd-ExtractParameter():Year:[%s]" CRLF, pcDest);
         break;

      case MASK_MONTH:
         iNr = cmd_ExtractData('M', pcFile, pcDest, iMaxSize);
         PRINTF1("cmd-ExtractParameter():Month:[%s]" CRLF, pcDest);
         break;

      case MASK_DAY:
         iNr = cmd_ExtractData('D', pcFile, pcDest, iMaxSize);
         PRINTF1("cmd-ExtractParameter():Day:[%s]" CRLF, pcDest);
         break;

      case MASK_HOUR:
         iNr = cmd_ExtractData('h', pcFile, pcDest, iMaxSize);
         break;

      case MASK_MINUTE:
         iNr = cmd_ExtractData('m', pcFile, pcDest, iMaxSize);
         break;

      case MASK_SECOND:
         iNr = cmd_ExtractData('s', pcFile, pcDest, iMaxSize);
         break;

      case MASK_TYPE:
         iNr = cmd_ExtractData('T', pcFile, pcDest, iMaxSize);
         break;

      case MASK_EXT:
         iNr = cmd_ExtractData('x', pcFile, pcDest, iMaxSize);
         break;

      default:
         break;
   }
   return(iNr);
}

// 
// Function:   cmd_ExtractData
// Purpose:    Extract data from the pikrellcam file
// 
// Parameters: Char type, file, dest, max_size
// Returns:    Dest act_size
// 
static int cmd_ExtractData(char cType, char *pcFile, char *pcDest, int iMaxSize)
{
   bool        fFound=FALSE;
   const char *pcMask=pcPiKrellCamMask;
   int         iNr=0;

   if(iMaxSize < 2) return(0);
   else             *pcDest = 0;

   //PRINTF2("cmd-ExtractData():Type=[%c], File=[%s]" CRLF, cType, pcFile);
   //
   while(!fFound && *pcFile && (iNr < iMaxSize))
   {
      if(*pcMask == cType)
      {
         fFound = TRUE;
         pcDest[iNr++] = *pcFile;
         pcDest[iNr]   = 0;
      }
      pcFile++;
      pcMask++;
   }
   //
   while(fFound && *pcFile && (iNr < iMaxSize))
   {
      if(*pcMask == cType)
      {
         pcDest[iNr++] = *pcFile;
         pcDest[iNr]   = 0;
      }
      else fFound = FALSE;
      pcFile++;
      pcMask++;
   }
   return(iNr);
}

// 
// Function:   cmd_ArchiveMotionFifo
// Purpose:    Have pikrellcam archive this video/Thumbnail file
// 
// Parameters: 
// Returns:    Cc
// Note:       Get the files from the G_ area
// 
static int cmd_ArchiveMotionFifo()
{
   int      iNr, iLen, iCc=EXIT_CC_GEN_ERROR;
   int      iFd;
   char     cYear[6];
   char     cMonth[6];
   char     cDay[6];
   char    *pcPar;
   char    *pcFile;
   char    *pcFifo;
   char    *pcData;
   char    *pcMot;
   char    *pcMin;

   pcFifo = GLOBAL_GetParameter(PAR_FIFO);
   pcPar  = GLOBAL_GetParameter(PAR_LAST_VID);
   pcMin  = GLOBAL_GetParameter(PAR_MIN_NOW);
   pcMot  = GLOBAL_GetParameter(PAR_MOTION_STATE);
   pcFile = cmd_FindFilename(pcPar);
   //
   iFd = open(pcFifo, O_WRONLY|O_NONBLOCK);
   if(iFd > 0)
   {
      //
      // Build archive fifo cmd
      //
      iNr  = cmd_ExtractParameter(MASK_YEAR, pcFile, cYear, 5);
      iNr += cmd_ExtractParameter(MASK_MONTH, pcFile, cMonth, 5);
      iNr += cmd_ExtractParameter(MASK_DAY, pcFile, cDay, 5);
      //
      if(iNr == 8)
      {
         //
         // OKee: all data found: send archive cmd to FIFO
         //
         pstMap->G_iArchiveCount++;
         pcData = (char *) safemalloc(MAX_CMDLINE_LEN);
         GEN_SNPRINTF(pcData, MAX_CMDLINE_LEN, "archive_video %s %s-%s-%s\n", pcFile, cYear, cMonth, cDay);
         iLen = GEN_STRLEN(pcData);
         iNr = write(iFd, pcData, iLen);
         //
         // LOG FIFO command (remove LF)
         //
         GEN_RemoveChar(pcData, LF);
         if(iNr != iLen) LOG_Report(0, "CMD", "cmd-ArchiveMotionFifo():%3d:Motion=%s, Minute=%s (FIFO-Cmd=%s) *** SIZE ERROR *** (%d i.o %d)", pstMap->G_iArchiveCount, pcMot, pcMin, pcData, iLen, iNr);
         else            LOG_Report(0, "CMD", "cmd-ArchiveMotionFifo():%3d:Motion=%s, Minute=%s (FIFO-Cmd=%s)", pstMap->G_iArchiveCount, pcMot, pcMin, pcData);
         //
         PRINTF2("cmd-ArchiveMotion():Minute=%s: FIFO <-- %s" CRLF, pcMin, pcData);
         safefree(pcData);
         close(iFd);
         iCc = EXIT_CC_OKEE;
      }
      else
      {
         LOG_Report(0, "CMD", "cmd-ArchiveMotionFifo():Nr=%d:Bad date syntax in %s", iNr, pcPar);
      }
   }
   else
   {
      LOG_Report(errno, "CMD", "cmd-ArchiveMotionFifo():ERROR open FIFO %s", pcFifo);
   }
   return(iCc);
}

// 
// Function:   cmd_ArchiveMotionLocal
// Purpose:    Locally archive this video/Thumbnail file
// 
// Parameters: 
// Returns:    Cc
// Note:       Get the files from the G_ area
// 
static int cmd_ArchiveMotionLocal()
{
   bool     fOkee=TRUE;
   int      iNr, iCc=EXIT_CC_GEN_ERROR;
   char     cYear[6];
   char     cMonth[6];
   char     cDay[6];
   char    *pcVideo;
   char    *pcThumb;
   char    *pcFile;
   char    *pcArchive;
   char    *pcPath;
   char    *pcFifo;

   pcFifo    = GLOBAL_GetParameter(PAR_FIFO);
   pcArchive = GLOBAL_GetParameter(PAR_ARCHIVE);
   pcVideo   = GLOBAL_GetParameter(PAR_LAST_VID);
   pcThumb   = GLOBAL_GetParameter(PAR_LAST_THB);
   //      
   PRINTF1("cmd-ArchiveMotionLocal():Archive [%s]" CRLF, pcArchive);
   //
   pcFile = cmd_FindFilename(pcVideo);
   iNr    = cmd_ExtractParameter(MASK_YEAR,  pcFile, cYear,  5);
   iNr   += cmd_ExtractParameter(MASK_MONTH, pcFile, cMonth, 5);
   iNr   += cmd_ExtractParameter(MASK_DAY,   pcFile, cDay,   5);
   //
   if(iNr == 8)
   {
      pcPath = (char *) safemalloc(MAX_PATH_LENZ);
      if(!cmd_DirectoryExists(pcArchive))  fOkee = cmd_DirectoryCreate(pcArchive);
      if(fOkee)
      {
         GEN_SNPRINTF(pcPath, MAX_PATH_LEN, "%s/%s", pcArchive, cYear);
         if(!cmd_DirectoryExists(pcPath))  fOkee = cmd_DirectoryCreate(pcPath);
      }
      if(fOkee)
      {
         GEN_SNPRINTF(pcPath, MAX_PATH_LEN, "%s/%s/%s", pcArchive, cYear, cMonth);
         if(!cmd_DirectoryExists(pcPath))  fOkee = cmd_DirectoryCreate(pcPath);
      }
      if(fOkee)
      {
         GEN_SNPRINTF(pcPath, MAX_PATH_LEN, "%s/%s/%s/%s", pcArchive, cYear, cMonth, cDay);
         if(!cmd_DirectoryExists(pcPath))  fOkee = cmd_DirectoryCreate(pcPath);
      }
      if(fOkee)
      {
         GEN_SNPRINTF(pcPath, MAX_PATH_LEN, "%s/%s/%s/%s/videos", pcArchive, cYear, cMonth, cDay);
         if(!cmd_DirectoryExists(pcPath))  fOkee = cmd_DirectoryCreate(pcPath);
      }
      if(fOkee)
      {
         GEN_SNPRINTF(pcPath, MAX_PATH_LEN, "%s/%s/%s/%s/thumbs", pcArchive, cYear, cMonth, cDay);
         if(!cmd_DirectoryExists(pcPath))  fOkee = cmd_DirectoryCreate(pcPath);
      }
      if(fOkee)
      {
         //
         // OKee: all directoriesexist, move files to archive
         //
         pstMap->G_iArchiveCount++;
         //
         GEN_SNPRINTF(pcPath, MAX_PATH_LEN, "%s/%s/%s/%s/videos/%s", pcArchive, cYear, cMonth, cDay, pcFile);
         //
         if(cmd_FileCopy(pcVideo, pcPath))
         {
            PRINTF2("cmd-ArchiveMotionLocal():%3d:Archive [%s]" CRLF, pstMap->G_iArchiveCount, pcVideo);
            LOG_Report(0, "CMD", "cmd-ArchiveMotionLocal():%3d:Archive [%s]", pstMap->G_iArchiveCount, pcVideo);
            pcFile = cmd_FindFilename(pcThumb);
            GEN_SNPRINTF(pcPath, MAX_PATH_LEN, "%s/%s/%s/%s/thumbs/%s", pcArchive, cYear, cMonth, cDay, pcFile);
            if(cmd_FileCopy(pcThumb, pcPath))
            {
               GEN_SNPRINTF(pcPath, MAX_PATH_LEN, "Archived %s", pcVideo);
               cmd_InformFifo(pcFifo, (char *)"0 3 0", pcPath);
               iCc = EXIT_CC_OKEE;
            }
            else
            {
               PRINTF1("cmd-ArchiveMotionLocal():Copy Thumb ERROR [%s]" CRLF, strerror(errno));
               LOG_Report(errno, "CMD", "cmd-ArchiveMotionLocal():ERROR copy [%s] to [%s]", pcThumb, pcPath);
            }
         }
         else
         {
            PRINTF1("cmd-ArchiveMotionLocal():Copy Video ERROR [%s]" CRLF, strerror(errno));
            LOG_Report(errno, "CMD", "cmd-ArchiveMotionLocal():ERROR copy [%s] to [%s]", pcVideo, pcPath);
         }
      }
      else
      {
         PRINTF1("cmd-ArchiveMotionLocal():Problems archiving to [%s]" CRLF, pcArchive);
         LOG_Report(0, "CMD", "cmd-ArchiveMotionLocal():Problems archiving to [%s]", pcArchive);
      }
      safefree(pcPath);
   }
   else
   {
      PRINTF2("cmd-ArchiveMotionLocal():Nr=%d:Bad date syntax in %s" CRLF, iNr, pcVideo);
      LOG_Report(0, "CMD", "cmd-ArchiveMotionLocal():Nr=%d:Bad date syntax in %s", iNr, pcVideo);
   }
   return(iCc);
}

// 
// Function:   cmd_SkipArchiveMotion
// Purpose:    Do NOT archive this pikrellcam video/Thumbnail file
// 
// Parameters: 
// Returns:    Cc
// Note:       
// 
static int cmd_SkipArchiveMotion()
{
   int      iMinsNow, iMinsDusk;
   char    *pcPar;
   char    *pcMot;
   char    *pcMin;
   char    *pcFile;

   pcMot     = GLOBAL_GetParameter(PAR_MOTION_STATE);
   pcPar     = GLOBAL_GetParameter(PAR_LAST_VID);
   pcFile    = cmd_FindFilename(pcPar);
   //
   // Get current minute
   // If minute shows end-of-the-day, email the day results
   //
   pcMin     = GLOBAL_GetParameter(PAR_MIN_NOW);
   iMinsNow  = atoi(pcMin);
   //
   pcMin     = GLOBAL_GetParameter(PAR_MIN_DUSK);
   iMinsDusk = atoi(pcMin);
   //
   PRINTF4("cmd-SkipArchiveMotion():%s:Motion=%s, Minute=%d, Dusk=%d" CRLF, pcFile, pcMot, iMinsNow, iMinsDusk);
   LOG_Report(0, "CMD", "cmd-SkipArchiveMotion():%3d-%s:Motion=%s, Minute=%d, Dusk=%d", pstMap->G_iArchiveCount, pcFile, pcMot, iMinsNow, iMinsDusk);
   return(EXIT_CC_OKEE);
}

// 
// Function:   cmd_DirectoryExists
// Purpose:    Check if directory exists
// 
// Parameters: Dir path
// Returns:    TRUE if exists
// Note:       
// 
static bool cmd_DirectoryExists(char *pcDir)
{
   bool        fCc=FALSE;
   int         iRes;
   struct stat stStat;

   iRes = stat(pcDir, &stStat);
   if(iRes == 0)
   {
      if(S_ISDIR(stStat.st_mode)) 
      {
         // Fine: Dir exists
         PRINTF1("cmd_DirectoryExists():Dir [%s] Exists" CRLF, pcDir);
         fCc = TRUE;
      }
      else
      {
         PRINTF1("cmd_DirectoryExists():Dir [%s] does NOT Exists" CRLF, pcDir);
      }
   }
   else
   {
      if(errno == ENOENT)
      {
         PRINTF2("cmd_DirectoryExists():Dir [%s] does not exist, %s" CRLF, pcDir, strerror(errno));
      }
      else 
      {
         PRINTF2("cmd_DirectoryExists():Error reading Dir [%s], %s" CRLF, pcDir, strerror(errno));
      }
   }
   return(fCc);
}

// 
// Function:   cmd_DirectoryCreate
// Purpose:    Create directory
// 
// Parameters: Dir path
// Returns:    TRUE if okee
// Note:       
// 
static bool cmd_DirectoryCreate(char *pcDir)
{
   bool  fCc=TRUE;
   int   iRes;

   if((iRes = mkdir(pcDir, 0755)) != 0)
   {
      PRINTF2("cmd-DirectoryCreate():ERROR creating directory [%s]:%s" CRLF, pcDir, strerror(errno));
      LOG_Report(errno, "CMD", "cmd-DirectoryCreate():ERROR creating directory [%s]:", pcDir);
      fCc = FALSE;
   }
   else PRINTF1("cmd-DirectoryCreate():Dir [%s] created" CRLF, pcDir);
   return(fCc);
}

// 
// Function:   cmd_EmailArchiveMotion
// Purpose:    Email this pikrellcam video/Thumbnail file at the appropriate time
// 
// Parameters: 
// Returns:    Cc
// Note:       Get the files from the G_ area
// 
static int cmd_EmailArchiveMotion()
{
   int      iCc = EXIT_CC_GEN_ERROR;
   int      iMinsNow, iMinsDusk;
   char    *pcPar;
   char    *pcMot;
   char    *pcMin;
   char    *pcFile;

   pcMot     = GLOBAL_GetParameter(PAR_MOTION_STATE);
   pcPar     = GLOBAL_GetParameter(PAR_LAST_VID);
   pcFile    = cmd_FindFilename(pcPar);
   //
   // Get current minute
   // If minute shows end-of-the-day, email the day results
   //
   pcMin     = GLOBAL_GetParameter(PAR_MIN_NOW);
   iMinsNow  = atoi(pcMin);
   //
   pcMin     = GLOBAL_GetParameter(PAR_MIN_DUSK);
   iMinsDusk = atoi(pcMin);
   //
   PRINTF4("cmd-EmailArchiveMotion():%s:Motion=%s, Minute=%d, Dusk=%d" CRLF, pcFile, pcMot, iMinsNow, iMinsDusk);
   LOG_Report(0, "CMD", "cmd-EmailArchiveMotion():%3d-%s:Motion=%s, Minute=%d, Dusk=%d", pstMap->G_iArchiveCount, pcFile, pcMot, iMinsNow, iMinsDusk);
   //
   if(iMinsNow >= iMinsDusk)
   {
      //
      // We have passed the dusk: email if not yet done this day
      //
      pstMap->G_ulMailSecsNow = RTC_GetSystemSecs();
      if(pstMap->G_ulMailSecsNow > pstMap->G_ulMailSecsMail)
      {
         //
         // Email not yet done this day, do it and setup again for tomorrow
         //
         pstMap->G_ulMailSecsMail  = RTC_GetMidnight(pstMap->G_ulMailSecsNow);
         pstMap->G_ulMailSecsMail += (24 * 3600);
         //
         if(RPI_InitMail())
         {
            PRINTF2("cmd-EmailArchiveMotion():Email results at Dusk: %02d:%02d" CRLF, iMinsDusk/60, iMinsDusk%60);
            LOG_Report(0, "CMD", "cmd-EmailArchiveMotion():Email results after Dusk: %02d:%02d", iMinsDusk/60, iMinsDusk%60);
            CMD_MailMotion();
            iCc = EXIT_CC_OKEE;
         }
      }
   }
   return(iCc);
}

// 
// Function:   cmd_InformFifo
// Purpose:    Send inform command to the pikrellcam FIFO
// 
// Parameters: FIFO (or NULL), Format, Message
// Returns:    Cc
// Note:       Build fifo cmd, f.i:
//             "inform timeout x
//             "inform "AapNootMies" a b c d e"
//                      pcMsg        pcFrmt
//                                   a: (0) Row within region
//                                   b: (3) Justify (3=center)
//                                   c: (0) Font (0=small, 1=big)
//                                   d: (0) Xs
//                                   e: (0) Ys
// 
static int cmd_InformFifo(const char *pcFifo, char *pcFrmt, char *pcMsg)
{
   int         iLen, iCc=EXIT_FAILURE;
   int         iFd;
   char       *pcData;

   if(pcFifo == NULL) pcFifo = GLOBAL_GetParameter(PAR_FIFO);
   //
   iLen = GEN_STRLEN(pcInformTimeout) + GEN_STRLEN(pcMsg) + GEN_STRLEN(pcFrmt) + 64;
   pcData = (char *) safemalloc(iLen);
   //
	iFd = open(pcFifo, O_WRONLY|O_NONBLOCK);
   if(iFd > 0)
   {
      //
      // First setup timeout
      // Send inform msg
      //
      GEN_SNPRINTF(pcData, iLen, "%s\ninform \"%s\" %s\n", pcInformTimeout, pcMsg, pcFrmt);
      write(iFd, pcData, GEN_STRLEN(pcData));
      LOG_Report(0, "RPI", "rpi-InformFifo():[Msg=%s, Frm=%s]", pcMsg, pcFrmt);
      close(iFd);
      iCc = EXIT_SUCCESS;
   }
   else
   {
      LOG_Report(errno, "CMD", "cmd-InformFifo():ERROR open FIFO %s", pcFifo);
   }
   safefree(pcData);
   return(iCc);
}

//
//  Function:   cmd_FileCopy
//  Purpose:    Copy file
//              
//  Parms:      Src, Dest
//  Returns:    TRUE if OKee
//
static bool cmd_FileCopy(char *pcSrc, char *pcDst)
{
   bool     fCc=FALSE;
   char    *pcBuffer;
   size_t  tRd, tWr;
   FILE    *ptSrc = safefopen(pcSrc, "r");
   FILE    *ptDst = safefopen(pcDst, "w");

   if(ptSrc && ptDst)
   {
      PRINTF1("cmd-FileCopy():Copy from %s" CRLF, pcSrc);
      PRINTF1("cmd-FileCopy():Copy to   %s" CRLF, pcDst);
      //
      pcBuffer = (char *) safemalloc(CMD_BUFFER_SIZE);
      //
      while(!safefeof(ptSrc)) 
      {
         tRd = safefread(pcBuffer, 1, CMD_BUFFER_SIZE, ptSrc);
         if(tRd) 
         {
            //PRINTF1("cmd-FileCopy():Read    %d bytes" CRLF, tRd);
            tWr = safefwrite(pcBuffer, 1, tRd, ptDst);
            //PRINTF1("cmd-FileCopy():Written %d bytes" CRLF, tWr);
         }
      }
      safefclose(ptSrc);
      safefclose(ptDst);
      safefree(pcBuffer);
      fCc = TRUE;
   }
   else
   {
      if(!ptSrc) PRINTF1("cmd-FileCopy():Error opening %s" CRLF, pcSrc);
      if(!ptDst) PRINTF1("cmd-FileCopy():Error opening %s" CRLF, pcDst);
   }
   return(fCc);
}


/*------  Local functions separator ------------------------------------
__SIGNAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
// Function:   cmd_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool cmd_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &cmd_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "CMD", "cmd-SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &cmd_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "CMD", "cmd-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &cmd_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "CMD", "cmd-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &cmd_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "CMD", "cmd-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &cmd_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "CMD", "cmd-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   cmd_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void cmd_ReceiveSignalSegmnt(int iSignal)
{
   GLOBAL_SegmentationFault(__FILE__, __LINE__);
   LOG_SegmentationFault(__FILE__, __LINE__);
   exit(EXIT_CC_GEN_ERROR);
}

// 
// Function:   cmd_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void cmd_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "CMD", "cmd-ReceiveSignalTerm()");
}

//
// Function:   cmd_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void cmd_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "CMD", "cmd-ReceiveSignalInt()");
}

//
// Function:   cmd_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       The camera thread will send us a SIGUSR1 to
//             signal a new command
//
static void cmd_ReceiveSignalUser1(int iSignal)
{
}

//
// Function:   cmd_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       Not used
//
static void cmd_ReceiveSignalUser2(int iSignal)
{
}


/*------  Local functions separator ------------------------------------
__COMMENT_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/
#ifdef COMMENT

static int     cmd_SaveMotionFiles        (const char *);

// 
// Function:   cmd_SaveMotionFiles
// Purpose:    Save motion files from the RAMFS cache to persistent archive filesystem
// 
// Parameters: Source dir
// Context:    PID_CMND
// Returns:    system call return code (-1 is error)
// Note:       PiKrellCam Archive structure:
//             /...../mdeia/archive/
//             2018/
//               07/
//                24/
//                 video/loop_2018
//                 thumbs/
//
static int cmd_SaveMotionFiles(const char *pcSrcDir)
{
   int      iSize, iCc;
   char    *pcTst;
   char    *pcDst;
   char    *pcCmd;

   iSize = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS);
   //
   pcTst = safemalloc(iSize+1);
   pcDst = safemalloc(MAX_PATH_LENZ);
   pcCmd = safemalloc(MAX_PATH_LENZ);
   //
   // Build input   image filename  f.i. /mnt/rpipix/*
   // Create output image directory f.i. /all/...../video/20180706_154525/
   //
   RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcTst, pstMv->ulSecsMotion);
   GEN_SNPRINTF(pcDst, MAX_PATH_LEN, "%s%s%s/", pstMap->G_pcWwwDir, pcSrcDir, pcTst);
   //
   iCc = mkdir(pcDst, 0755);
   if(iCc == EXIT_CC_OKEE)
   {
      GEN_SNPRINTF(pcCmd, MAX_PATH_LEN, "mv %s* %s", pstMap->G_pcRamDir, pcDst);
      PRINTF1("hlp-SaveRamfiles():Save motion files [%s]" CRLF, pcCmd);
      //
      iCc = system(pcCmd);
      if(iCc) LOG_Report(errno, "HLP", "hlp-SaveRamfiles():ERROR calling ", pcCmd);
   }
   else
   {
      LOG_Report(errno, "HLP", "hlp-SaveRamfiles():Cannot create directory %s", pcDst);
   }
   safefree(pcCmd);
   safefree(pcDst);
   safefree(pcTst);
   return(iCc);
}

//
// Function:   cmd_DumpVars
// Purpose:    
//
// Parms:
// Returns:    
// Note:       Not used
//
static void cmd_DumpVars(const char *pcCaption)
{
   char       *pcVar;

   PRINTF1("CMD-DumpVars():%s" CRLF, pcCaption);
   //
   pcVar = GLOBAL_GetParameter(PAR_POP3_USER);
   if(pcVar) PRINTF1("CMD-DumpVars():PAR_POP3_USER=%s" CRLF, pcVar);
   pcVar = GLOBAL_GetParameter(PAR_POP3_PASS);
   if(pcVar) PRINTF1("CMD-DumpVars():PAR_POP3_PASS=%s" CRLF, pcVar);
   pcVar = GLOBAL_GetParameter(PAR_POP3_SRVR);
   if(pcVar) PRINTF1("CMD-DumpVars():PAR_POP3_SRVR=%s" CRLF, pcVar);
   pcVar = GLOBAL_GetParameter(PAR_POP3_PORT);
   if(pcVar) PRINTF1("CMD-DumpVars():PAR_POP3_PORT=%s" CRLF, pcVar);
   //
   pcVar = GLOBAL_GetParameter(PAR_SMTP_USER);
   if(pcVar) PRINTF1("CMD-DumpVars():PAR_SMTP_USER=%s" CRLF, pcVar);
   pcVar = GLOBAL_GetParameter(PAR_SMTP_PASS);
   if(pcVar) PRINTF1("CMD-DumpVars():PAR_SMTP_PASS=%s" CRLF, pcVar);
   pcVar = GLOBAL_GetParameter(PAR_SMTP_SRVR);
   if(pcVar) PRINTF1("CMD-DumpVars():PAR_SMTP_SRVR=%s" CRLF, pcVar);
   pcVar = GLOBAL_GetParameter(PAR_SMTP_PORT);
   if(pcVar) PRINTF1("CMD-DumpVars():PAR_SMTP_PORT=%s" CRLF, pcVar);
}

#endif   //COMMENT
