/*  (c) Copyright:  2018  Patrn, Confidential Data
 *
 *  Workfile:           par_pids.h
 *  Revision:
 *  Modtime:
 *
 *  Purpose:            Extract headerfile for the RPI thread PIDs
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    24 Jul 2018:      Created
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

//          ePid           pcPid          pcHelp
EXTRACT_PID(PID_HOST,      "Host",        "Host")
EXTRACT_PID(PID_CMND,      "Cmnd",        "Command")
EXTRACT_PID(PID_SPARE1,    "Spr1",        "Spare 1")
EXTRACT_PID(PID_SPARE2,    "Spr2",        "Spare 2")
EXTRACT_PID(PID_SPARE3,    "Spr3",        "Spare 3")
EXTRACT_PID(PID_SPARE4,    "Spr4",        "Spare 4")
EXTRACT_PID(PID_SPARE5,    "Spr5",        "Spare 5")
EXTRACT_PID(PID_SPARE6,    "Spr6",        "Spare 6")

