/*  (c) Copyright:  2018  Patrn, Confidential Data
 *
 *  Workfile:           cmd_mail.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for command cmd_mail.c
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    14 Aug 2018:      Copied from spicam
 *
 *
 *
 *
 *
 *
**/

#ifndef _CMD_MAIL_H_
#define _CMD_MAIL_H_

bool CMD_MailMotion           (void);

#endif /* _CMD_MAIL_H_ */
