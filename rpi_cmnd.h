/*  (c) Copyright:  2018  Patrn, Confidential Data
 *
 *  Workfile:           rpi_cmnd.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for main thread
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    24 Jul 2018:      Created
 *    16 Apr 2021:      Add Verbose LOG level
 *    06 Jun 2021:      Add flags to verbose arg
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_CMND_H_
#define _RPI_CMND_H_

#define  ARG_LEN              255
#define  ARG_LENZ             ARG_LEN+1
//
#define CMD_BUFFER_SIZE       32768

// =========================================================================================
// FIFO Commands
//    example: 
//    echo "motion_enable [on|off|toggle]" > ~/pikrellcam/www/FIFO
// or
//    pcFifo = GLOBAL_GetParameter(PAR_FIFO);
//    GEN_SNPRINTF(pcShell, MAX_CMDLINE_LEN, "echo \"archive_video %s %s-%s-%s\" >%s", pcFile, cYear, cMonth, cDay, pcFifo);
//    iCc = system(pcShell);
//
// =========================================================================================
//    audio mic_open
//    audio mic_close
//    audio mic_toggle
//    audio gain [up|down|N]		# N: 0 - 30
//    audio stream_open
//    audio stream_close
//    audio_trigger_video [on|off]
//    audio_trigger_level N		# N: 2 - 100
//    box_MP3_only [on|off]
//    record on
//    record on pre_capture_time
//    record on pre_capture_time time_limit
//    record pause
//    record off
//    loop [on|off|toggle]
//    still
//    tl_start period
//    tl_end
//    tl_hold [on|off|toggle]
//    tl_show_status [on|off|toggle]
//    motion_enable [on|off|toggle]
//    motion limits magnitude count
//    motion burst count frames
//    motion trigger code    # code is digit N or N:ID    N is 0 or 1 and ID is a string (see Examples)
//    motion trigger code pre_capture time_limit
//    motion load_regions name
//    motion save_regions name
//    motion list_regions
//    motion show_regions [on|off|toggle]
//    motion show_vectors [on|off|toggle]
//    motion [command] - other commands sent by the web page to edit motion regions not
//    	intented for script or command line use.
//    
//    preset prev_position
//    preset next_position
//    preset prev_settings
//    preset next_settings
//    preset goto position settings
//    
//    display [command] - commands sent by the web page to display OSD menus. Not intended for
//    	script or command line use.
//    
//    tl_inform_convert
//    video_fps fps
//    video_bitrate bitrate
//    still_quality quality
//    video_mp4box_fps fps
//    inform "some string" row justify font xs ys
//    	echo inform \"Have a nice day.\" 3 3 1 > FIFO
//    	echo inform timeout 3
//    archive_video [day|day_loop|video.mp4] [today|yesterday|yyyy-mm-dd]
//    archive_still [day|video.mp4] [today|yesterday]yyyy-mm-dd]
//    annotate_text_background_color [none|rrggbb]   # rrggbb is hex color value 000000 - ffffff
//    annotate_text_brightness value   # value is integer 0 - 255, 255 default
//    annotate_text_size  value        # value is integer 6 - 160, 32 default
//    annotate_string [prepend|append] id string
//    annotate_string remove id
//    annotate_string spacechar c
//    fix_thumbs [fix|test]
//    delete_log
//    upgrade
//    quit
//    

// =========================================================================================
// Arg  CMD   Function
// =========================================================================================
// (0)      - Executable /usr/bin/pikarch
//       $C - user scripts directory full path
//       $c - distribution scripts directory full path
//       $I - the PiKrellCam install directory
// (2)   $a - archive directory full path
//       $m - media directory full path
//       $M - mjpeg file full path
// (5)   $P - command FIFO full path
// (1)   $G - log file full path
//       $H - hostname
//       $h - multicast from hostname
//       $E - effective user running PiKrellCam
//       $V - video files directory full path
//       $t - thumb files directory full path
//       $z - loop files directory full path
// (3)   $v - last video saved full path filename
// (4)   $A - last thumb saved full path filename
//            If motion_preview_save_mode is first and there are
//            only audio or external detects, this name will be,
//            renamed after the video ends.
//       $S - still files directory full path
//       $s - last still saved full path filename
//       $L - timelapse files directory full path
//       $l - timelapse current series filename format: tl_sssss_%05d.jpg
//            in timelapse sub directory.  If used in any script
//            arg list, $l must be the last argument.
//       $N - timelapse sequence last number
//       $n - timelapse series
//       $T - timelapse video full path filename in video sub directory
//  (7)  $o - motion enable state
//       $e - current motion video type: motion, audio, FIFO
//            (or FIFO code).  Video can start as audio or FIFO
//            type and change to motion if motion is detected.
//       $p - current preset number pair: position setting
//  (6)  $D - current_minute dawn sunrise sunset dusk
//       $X - motion video sequence number
//       $Z - pikrellcam version
//
typedef enum _pikrellcam_args_
{
   ARG_NAME    = 0,           // Utility name
   ARG_LOG_FILE,              // $G LOG file
   ARG_PATH_ARCHIVE,          // $a Archive Dir
   ARG_LAST_VLOOP,            // $v Last Saved Path Video 
   ARG_LAST_TLOOP,            // $A Last Saved Path Thumbnail
   ARG_PATH_FIFO,             // $P FIFO Full Path
   ARG_MIN_NOW,               // $D Minute Now
   ARG_MIN_DAWN,              //    Minute Dawn
   ARG_MIN_SUNRISE,           //    Minute Sunrise
   ARG_MIN_SUNSET,            //    Minute Sunset
   ARG_MIN_DUSK,              //    Minute Dusk
   ARG_MOTION_STATE,          // $o Motion state
   ARG_VERBOSE,               // Flags and Verbose level
   //
   // End of args from PiKrellCam scripts
   // See /home/pi/.pikrellcam/pikrellcam.conf
   //
   // 27 jul 2018:$s in addition to the previous does NOT seem to work.
   //             Both on_motion_end AND on_motion_enable do NOT call pikarch anymore.
   //
   NUM_ARGS
}  PIKARG;

//
// PiKrellCam file mask:
//
// No motion
// ===================================
// loop_2018-07-27_15.34.55_0.mp4
// loop_2018-07-27_15.34.55_0.th.jpg
//
// Motion
// ===================================
// loop_2018-07-27_15.34.55_m.mp4
// loop_2018-07-27_15.34.55_m.th.jpg
//
//    C:    Caption     (loop, manual, ..)
//    Y:    Year        (2018,..)
//    M:    Month       (01...12)
//    D:    Day         (01...31)
//    h:    Hour        (00...23)
//    m:    Minute      (00...59)
//    s:    Second      (00...59)
//    T:    Type        (0, m, ...)
//    x:    Extension   (.mp4, .th.jpg, ...)
//    _:    Separator
//
typedef enum _pikrellcam_mask_
{
   MASK_CAPTION = 0,
   MASK_YEAR,
   MASK_MONTH,
   MASK_DAY,
   MASK_HOUR,
   MASK_MINUTE,
   MASK_SECOND,
   MASK_TYPE,
   MASK_EXT,
   //
   NUM_MASK
}  PIKMSK;

//
int   CMD_Execute       (char [NUM_ARGS][ARG_LENZ], int, int);

#endif /* _RPI_CMND_H_ */
