/*  (c) Copyright:  2018 Patrn, Confidential Data
 *
 *  Workfile:           cmd_mail.c
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            Mail data 
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    14 Aug 2018:      Initial commit
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_mail.h"
#include "mail/mail_func.h"
#include "mail/mail_client.h"
//
#include "cmd_mail.h"

//#define USE_PRINTF
#include <printf.h>


//
// Variables
//
static const char *pcMailListTo[] =
{
   "peter.hillen@ziggo.nl",
   NULL
};
//
static const char *pcMailListCc[] =
{
   "peter.hillen@gmail.com",
   "peter@patrn.nl",
   NULL
};
//
static const char *pcMailListBcc[] =
{
   NULL
};
//
static const char *pcMailMotionSymmary    =  RPI_PUBLIC_LOG_PATH;
static const char *pcMailListSubjectMdet  =  "Patrn Motion Detect summary";

//
// Local prototypes
//
static MAILCT  cmd_MailMotionData      (MAILDT, int, char **, int *);

//
//  Function:   CMD_MailMotion
//  Purpose:    Handle Mail motion data
//
//  Parms:      
//  Returns:    TRUE
//
bool CMD_MailMotion()
{
   PRINTF("CMD-MailMotion()" CRLF);
   RPI_SendMail(cmd_MailMotionData);
   return(TRUE);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   cmd_MailMotionData
// Purpose:    Send mail with Motion Detect summary
// 
// Parameters: Datatype, index, mail data, mail data size
// Returns:    Content type (MAIL_CT_HTML, ..., MAIL_CT_NONE
// Note:       This is a callback from the mail collector, and it has to free
//             the allocated memory later !
// 
static MAILCT cmd_MailMotionData(MAILDT tDt, int iIdx, char **ppcData, int *piSize)
{
   MAILCT   tNewCt, tRetCt=MAIL_CT_NONE;
   char    *pcData=NULL;
   
   PRINTF2("cmd-MailMotionData():Idx=%d, DT=%d" CRLF, iIdx, tDt);
   //
   switch(tDt)
   {
      default:
         tNewCt = MAIL_CT_NONE;
         break;

      case MAIL_DT_FROM:
      case MAIL_DT_REPLY:
         if(iIdx == 0) pcData = pstMap->G_pcSmtpUser;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_TO:
         pcData = (char *)pcMailListTo[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_CC:
         pcData = (char *)pcMailListCc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_BCC:
         pcData = (char *)pcMailListBcc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_SUBJECT:
         if(iIdx == 0) pcData = (char *)pcMailListSubjectMdet;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_ATTM:
         tNewCt = MAIL_CT_NONE;
         break;

      case MAIL_DT_BODY:
         if(iIdx == 0) pcData = (char *)pcMailMotionSymmary;
         tNewCt = MAIL_CT_FILE;
         break;
   }
   if(pcData)
   {
      if(RPI_MailCopyBody(ppcData, pcData, piSize)) tRetCt = tNewCt;
   }
   return(tRetCt);
}

