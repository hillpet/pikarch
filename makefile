#################################################################################
# 
# makefile:
#	$(TARGET) - Raspberry Pi PiKrellCam archiving utility
#
#	Copyright (c) 2018 Peter Hillen
#################################################################################
BOARD=$(shell cat ../../RPIBOARD)

ifeq ($(BOARD),8)
#
# RPi#8 (Rev-2): PiKrellCam motion detect
#
	BOARD_OK=yes
	TARGET=pikarch
	COMMON=../common
	OBJM=rpi_mail.o cmd_mail.o mail/mail_func.o mail/mail_ssl.o mail/mail_smtp.o mail/mail_pop3.o mail/mail_client.o mail/blowbox.o mail/blowfish.o mail/encode.o mail/crypto.o
	OBJX=
	DEFS=-D FEATURE_IO_NONE
	LIBM=-lssl -lcrypto
endif

ifeq ($(BOARD),10)
#
# RPi#10 (Rev-3): SpiCam HTTP Server
#
	BOARD_OK=yes
	TARGET=pikarch
	COMMON=../common
	OBJM=rpi_mail.o cmd_mail.o mail/mail_func.o mail/mail_ssl.o mail/mail_smtp.o mail/mail_pop3.o mail/mail_client.o mail/blowbox.o mail/blowfish.o mail/encode.o mail/crypto.o
	OBJX=
	DEFS=-D FEATURE_IO_NONE
	LIBM=-lssl -lcrypto
endif

ifndef BOARD_OK
#
# All other RPi's: 
#
	BOARD_OK=yes
	TARGET=pikarch
	COMMON=../common
	OBJM=
	OBJX=
	DEFS=-D FEATURE_IO_NONE
	LIBM=
endif

CC		= gcc
CFLAGS	= -O2 -Wall -g
DEFS	+= -D BOARD_RPI$(BOARD)
INCDIR	+= -I/opt/vc/include
INCDIR	+= -I/opt/vc/include/interface/vmcs_host/linux
INCDIR	+= -I/opt/vc/include/interface/vcos/pthreads -DRPI=1
INCDIR	+= -I$(COMMON)
LDFLGS	= -pthread
DESTDIR	= ./bin
LIBFLGS	= -L/usr/local/lib -L/usr/lib -L/opt/vc/lib -L$(COMMON)
LIBS		= -lrt
LIBS		+= $(LIBM) 
LIBCOM	= -lcommon
DEPS		= config.h
OBJ		= rpi_main.o rpi_cmnd.o globals.o
OBJ		+= $(OBJM)
OBJ		+= $(OBJX)

debug: LIBCOM = -lcommondebug
debug: DEFS += -D FEATURE_USE_PRINTF -D DEBUG_ASSERT
debug: all

all: modules $(DESTDIR)/$(TARGET)

modules:
$(DESTDIR)/$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(DEFS) -o $@ $(OBJ) $(LIBFLGS) $(LIBS) $(LIBCOM) $(LDFLGS)
	strip $(DESTDIR)/$(TARGET)

sync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp -u /mnt/rpi/softw/$(TARGET)/*.c ./
	cp -u /mnt/rpi/softw/$(TARGET)/*.h ./
	cp -u /mnt/rpi/softw/$(TARGET)/mail/*.c mail/
	cp -u /mnt/rpi/softw/$(TARGET)/mail/*.h mail/
	cp -u /mnt/rpi/softw/$(TARGET)/makefile ./
	cp -u /mnt/rpi/softw/$(TARGET)/piktest ./
	cp -u /mnt/rpi/softw/$(TARGET)/pikfifo ./

forcesync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp /mnt/rpi/softw/$(TARGET)/*.c ./
	cp /mnt/rpi/softw/$(TARGET)/*.h ./
	cp /mnt/rpi/softw/$(TARGET)/mail/*.c mail/
	cp /mnt/rpi/softw/$(TARGET)/mail/*.h mail/
	cp /mnt/rpi/softw/$(TARGET)/makefile ./
	cp /mnt/rpi/softw/$(TARGET)/piktest ./
	cp /mnt/rpi/softw/$(TARGET)/pikfifo ./

ziplog:
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo gzip -c /usr/local/share/rpi/$(TARGET).log >>/usr/local/share/rpi/$(TARGET)logs.gz
	sudo rm /usr/local/share/rpi/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET).log

copylog:
	@echo "Copy logfile to PC: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo cp /mnt/rpicache/$(TARGET).log /mnt/rpi/softw/$(TARGET)/$(TARGET).log

sortlog:
	@echo "Sort and copy logfile to PC: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo cat /mnt/rpicache/$(TARGET).log | sort -n >/mnt/rpicache/$(TARGET)sort.log
	sudo cp /mnt/rpicache/$(TARGET)sort.log /mnt/rpi/softw/$(TARGET)/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET)sort.log

dellog:
	@echo "Delete logfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm /usr/local/share/rpi/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET).log

delmap:
	@echo "Delete mapfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm /usr/local/share/rpi/$(TARGET).map
	sudo rm /mnt/rpicache/$(TARGET).map

clean:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	rm -rf *.o
	rm -rf mail/*.o
	rm -rf $(DESTDIR)/$(TARGET)

setup:
	sudo cp /usr/local/share/rpi/$(TARGET).map /mnt/rpicache/$(TARGET).map
	sudo cp /usr/local/share/rpi/$(TARGET).log /mnt/rpicache/$(TARGET).log

install:
	sudo cp bin/$(TARGET) /usr/bin/$(TARGET)

mail/%.o: mail/%.c  $(DEPS)
	$(CC) -o $@ -c $(CFLAGS) $(DEFS) $(INCDIR) $< 

.c.o: $(DEPS)
	$(CC) -c $(CFLAGS) $(DEFS) $(INCDIR) $< 
