/*  (c) Copyright:  2018  Patrn, Confidential Data
 *
 *  Workfile:           rpi_mail.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for command cmd_mail.c
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    14 Aug 2018:      Copied from spicam
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_MAIL_H_
#define _RPI_MAIL_H_

#include "mail/mail_func.h"

bool RPI_InitMail             (void);
void RPI_SendMail             (MAILSCB);
void RPI_ReceiveMail          (void);
bool RPI_ParameterChanged     (int);
bool RPI_MailCopyBody         (char **, char *, int *);

#endif /* _RPI_MAIL_H_ */
