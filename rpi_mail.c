/*  (c) Copyright:  2018 Patrn, Confidential Data
 *
 *  Workfile:           rpi_mail.c
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            Mail send/receive functions
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Revisions:
 *    14 Aug 2018:      Copied from spicam
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "mail/mail_func.h"
#include "mail/mail_client.h"
//
#include "rpi_mail.h"

//#define USE_PRINTF
#include <printf.h>

//
// Local prototypes
//
static void    cmd_GetMailParameters   (MAILO *, MAILSCB, MAILDT);
static bool    cmd_StoreMail           (int, char *, int, void *);

//#define  FEATURE_DUMP_MAILO
#ifdef   FEATURE_DUMP_MAILO
   static void cmd_DumpMailo           (MAILO *, const char *);
   #define DUMP_MAILO(a,b)             cmd_DumpMailo(a,b)
#else    //FEATURE_DUMP_MAILO
   #define DUMP_MAILO(a,b)
#endif   //FEATURE_DUMP_MAILO

//
//  Function:   RPI_ParameterChanged
//  Purpose:    Handle HTTP parameter changes
//
//  Parms:      HTTP page ID
//  Returns:    TRUE if OKee
//
bool RPI_ParameterChanged(int iId)
{
   return(CLIENT_ParameterChanged(iId));
}

//
//  Function:   RPI_InitMail
//  Purpose:    Init mail
//
//  Parms:      
//  Returns:    TRUE if proper Usernames found
//
bool RPI_InitMail()
{
   bool  fCc=TRUE;
   //
   // Setup e-mail
   //
   if(!CLIENT_Init() )
   {
      PRINTF("RPI-mail():Email Client ERROR: exit" CRLF);
      LOG_Report(0, "RPI", "Email Client ERROR: exit");
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:   RPI_ReceiveMail
//  Purpose:    Receive an email
//
//  Parms:      
//  Returns:    void
//
void RPI_ReceiveMail()
{
   int      i, iPort, iNr;
   ATYPE    tAuth;
   char    *pcSrvr, *pcUser, *pcPass, *pcMail;
   MAILO   *pstOpt;

#ifdef   FEATURE_TEST_MIME
   {
      const char *pcTest=  "==1==2==3==4==5==6==7==8==9==0==1==2==3==4==5===" CRLF
                           "**the quick brown fox jumps over the lazy dog **" CRLF
                           "**The quick brown fox jumps over the lazy dog **" CRLF
                           "**THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG **" CRLF
                           "==1==2==3==4==5==6==7==8==9==0==1==2==3==4==5===" CRLF;
      //
      char *pcDecoded;
      char *pcEncoded;
      int   iLen2, iLen3;
      
      pcEncoded = MIME_Encode64((u_int8 *)pcTest, GEN_STRLEN(pcTest), NULL, &iLen2);
      PRINTF1("RPI-ReceiveMail():base64_encode() L=%d" CRLF, iLen2);
      PRINTF1("RPI-ReceiveMail():base64_encode():\n%s" CRLF, pcEncoded);
      //
      pcDecoded = (char *)MIME_Decode64(pcEncoded, iLen2, NULL, &iLen3);
      PRINTF1("RPI-ReceiveMail():base64_decode() L=%d" CRLF, iLen3);
      PRINTF1("RPI-ReceiveMail():base64_decode():\n%s" CRLF, pcDecoded);
      //
      safefree(pcEncoded);
      safefree(pcDecoded);
   }
#endif   //FEATURE_TEST_MIME

   //
   // Setup client changes if needed
   // Retrieve mail server settings
   //
   tAuth = CLIENT_Setup(CL_COMM_MAIL_IN);
   switch(tAuth) 
   {
      case MAIL_SETUP_NO_AUTH:
         // No authentication credentials found: attempt anonymous login
         break;

      case MAIL_SETUP_PLAIN_AUTH:
         // Plaintext authentication is forced. Use with caution, 
         // everybody can read the base64 codes Username/password.
         // Fallthrough !
      default:
      case MAIL_SETUP_SSL_AUTH:
         // Authentication credentials found: attempt authenticated login
         pcUser = CLIENT_GetUser(CL_COMM_MAIL_IN);
         pcSrvr = CLIENT_GetServer(CL_COMM_MAIL_IN);
         iPort  = CLIENT_GetPort(CL_COMM_MAIL_IN);
         pcMail = NULL;
         //
         pstOpt = MAIL_Init(tAuth);
         //
         pcPass = safemalloc(BUF_MISC);
         if(CLIENT_GetPassword(CL_COMM_MAIL_IN, pcPass, BUF_MISC))
         {
            //
            // Password in plaintext now
            //
            MAIL_SetMailServer(pstOpt, MAIL_POP3, pcSrvr, iPort, pcUser, pcPass);
            if( MAIL_Login(pstOpt) )
            {
               iNr = MAIL_RetrieveAccount(pstOpt);
               PRINTF2("RPI-ReceiveMail():%d new emails for %s" CRLF, iNr, pcUser);
               for(i=1; i<=iNr; i++)
               {
                  if(MAIL_RetrieveHeader(pstOpt, i, 0, cmd_StoreMail, pcMail))
                  {
                     PRINTF1("RPI-ReceiveMail():mail header nr-%d OK" CRLF, iNr);
                  }
                  if(MAIL_ReceiveMail(pstOpt, i, cmd_StoreMail, pcMail))
                  {
                     PRINTF1("RPI-ReceiveMail():mail nr-%d OK" CRLF, iNr);
                  }
               }
               MAIL_Logout(pstOpt);
            }
            else PRINTF("RPI-ReceiveMail():ERROR: Login failed !" CRLF);
            MAIL_Cleanup(pstOpt);
         }
         else PRINTF("RPI-ReceiveMail():ERROR: Get password" CRLF);
         safefree(pcPass);
         break;
   }
}

//
//  Function:   RPI_SendMail
//  Purpose:    Send an email
//
//  Parms:      Callback func
//  Returns:    void
//
void RPI_SendMail(MAILSCB pfunMail)
{
   int      iPort;
   ATYPE    tAuth;
   char     cLog[DATE_TIME_SIZE];
   char    *pcSrvr, *pcUser=NULL, *pcPass=NULL;
   MAILO   *pstOpt;

   PRINTF1("RPI-SendMail():(A)Safe memory allocation=%d" CRLF, GLOBAL_GetMallocs());
   //
   RTC_GetDateTimeSecs(cLog);
   PRINTF1("RPI-SendMail():Timestamp=%s" CRLF, cLog);
   //
   // MAIL_SETUP_SSL_AUTH is chosen if Username/Password are supplied as well. If not, 
   // MAIL_SETUP_NO_AUTH is used. 
   // The option MAIL_SETUP_PLAIN_AUTH can be forced here al well.
   // Setup client changes if needed, retrieve mail server settings
   //
   tAuth  = CLIENT_Setup(CL_COMM_MAIL_OUT);
   pcSrvr = CLIENT_GetServer(CL_COMM_MAIL_OUT);
   iPort  = CLIENT_GetPort(CL_COMM_MAIL_OUT);
   //
   switch(tAuth) 
   {
      default:
      case MAIL_SETUP_NO_AUTH:
         // No authentication credentials found: attempt anonymous login
         PRINTF("RPI-SendMail():No Auth" CRLF);
         break;

      case MAIL_SETUP_SSL_AUTH:
         //
         // Authentication credentials found: attempt authenticated login.
         // SSL/TLS is assumed since we have a User/Psw combo. If the port
         // is set to MAIL_SMTP_PORT_PLAIN, swap to plain login
         //
         if(iPort == MAIL_SMTP_PORT_PLAIN) tAuth = MAIL_SETUP_PLAIN_AUTH;
         // Fallthrough !
      case MAIL_SETUP_PLAIN_AUTH:
         //
         // If plaintext authentication is forced, use with caution, 
         // everybody can read the base64 codes Username/password.
         //
         pcPass = safemalloc(BUF_MISC);
         if(CLIENT_GetPassword(CL_COMM_MAIL_OUT, pcPass, BUF_MISC))
         {
            pcUser = CLIENT_GetUser(CL_COMM_MAIL_OUT);
            PRINTF1("RPI-SendMail():Auth user %s" CRLF, pcUser);
         }
         else
         {
            // No proper credentials: try to login anonymous
            PRINTF("RPI-SendMail():No Pass: try No Auth" CRLF);
            tAuth = MAIL_SETUP_NO_AUTH;
         }
         break;
   }
   pstOpt = MAIL_Init(tAuth);
   //=======================================================================
   // IF WE NEED A PASSWORD, IT'S IN PLAINTEXT NOW
   //
   MAIL_SetMailServer(pstOpt, MAIL_SMTP, pcSrvr, iPort, pcUser, pcPass);
   if(pcPass) 
   {
      GEN_MEMSET(pcPass, 0x00, BUF_MISC);
      pcPass = safefree(pcPass);
   }
   // DESTROYED PASSWORD IN MEMORY
   //=======================================================================
   if( MAIL_Login(pstOpt) )
   {
      DUMP_MAILO(pstOpt, "A:MAIL_Login ()");
      //
      // Collect all mail parameters and data from the mail client by calling
      // the callback function in the function argument: 
      //    tCt = pfunMail() 
      // until tCt == MAIL_CT_NONE. 
      // Add all responses to the MAILO struct linked-list (pstOpt->pstList).
      //
      PRINTF1("RPI-SendMail():(B)Safe memory allocation=%d" CRLF, GLOBAL_GetMallocs());
      cmd_GetMailParameters(pstOpt, pfunMail, MAIL_DT_FROM);
      cmd_GetMailParameters(pstOpt, pfunMail, MAIL_DT_REPLY);
      cmd_GetMailParameters(pstOpt, pfunMail, MAIL_DT_TO);
      cmd_GetMailParameters(pstOpt, pfunMail, MAIL_DT_CC);
      cmd_GetMailParameters(pstOpt, pfunMail, MAIL_DT_BCC);
      cmd_GetMailParameters(pstOpt, pfunMail, MAIL_DT_SUBJECT);
      cmd_GetMailParameters(pstOpt, pfunMail, MAIL_DT_DATE);
      cmd_GetMailParameters(pstOpt, pfunMail, MAIL_DT_ID);
      cmd_GetMailParameters(pstOpt, pfunMail, MAIL_DT_BODY);
      cmd_GetMailParameters(pstOpt, pfunMail, MAIL_DT_ATTM);
      PRINTF1("RPI-SendMail():(C)Safe memory allocation=%d" CRLF, GLOBAL_GetMallocs());
      DUMP_MAILO(pstOpt, "A:GetMailParm()");
      PRINTF1("RPI-SendMail():(D)Safe memory allocation=%d" CRLF, GLOBAL_GetMallocs());
      MAIL_SendMail(pstOpt);
      PRINTF1("RPI-SendMail():(E)Safe memory allocation=%d" CRLF, GLOBAL_GetMallocs());
      MAIL_Logout(pstOpt);
      DUMP_MAILO(pstOpt, "A:MAIL_LogOut()");
   }
   else
   {
      PRINTF("RPI-SendMail():ERROR:Login failed!" CRLF);
   }
   PRINTF1("RPI-SendMail():(F)Safe memory allocation=%d" CRLF, GLOBAL_GetMallocs());
   pstOpt = MAIL_Cleanup(pstOpt);
   PRINTF1("RPI-SendMail():(G)Safe memory allocation=%d" CRLF, GLOBAL_GetMallocs());
   PRINTF("RPI-SendMail():Done!" CRLF);
}

//
// Function:   RPI_MailCopyBody
// Purpose:    Copy mail body to mail handler
//
// Parms:      Data ptr, size ptr
// Returns:    TRUE if OKee
// Note:       
//
bool RPI_MailCopyBody(char **ppcData, char *pcData, int *piSize)
{
   bool  fCc=FALSE;
   int   iSize;

   if(pcData && ppcData && piSize)
   {
      iSize    = GEN_STRLEN(pcData);
      *ppcData = safemalloc(iSize+1);
      *piSize  = iSize;
      //
      // Copy data to destination
      //
      GEN_STRCPY(*ppcData, pcData);
      fCc = TRUE;
   }
   return(fCc);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   cmd_GetMailParameters
// Purpose:    Retrieve the send-mail parameters
//
// Parms:      Mail option struct, Data type
// Returns:    
//
static void cmd_GetMailParameters(MAILO *pstOpt, MAILSCB pfunMail, MAILDT tDataType)
{
   MAILCT   tContType;
   int      iIdx=0, iDataSize;
   char    *pcData;
   MAILL   *pstList;
   MAILL   *pstNew;

   do
   {
      pcData    = NULL;
      iDataSize = 0;
      //
      // The client callback function MUST allocate the necessary memory for the info
      // and copy the info into this allocated location, passing its length also !
      // If the client has no more data to pass along, it MUST return MAIL_CT_NONE
      // on successive calls.
      //
      tContType = pfunMail(tDataType, iIdx, &pcData, &iDataSize);
      if(tContType != MAIL_CT_NONE)
      {
         PRINTF1("cmd_GetMailParameters():Add entry %d" CRLF, iIdx);
         //
         // Add this part of the mail to the MAILL mail list struct
         //
         pstNew = safemalloc(sizeof(MAILL));
         pstNew->tContType = tContType;
         pstNew->tDataType = tDataType;
         pstNew->pcData    = pcData;
         pstNew->iDataSize = iDataSize;
         pstNew->pstNext   = NULL;
         //
         if(pstOpt->pstList == NULL) pstOpt->pstList = pstNew;
         else
         {
            pstList = pstOpt->pstList;
            while(pstList->pstNext) pstList = pstList->pstNext;
            pstList->pstNext = pstNew;
         }
         iIdx++;
      }
   }
   while(tContType != MAIL_CT_NONE);
}

//
//  Function:   cmd_StoreMail
//  Purpose:    Callback for receive mail
//
//  Parms:      iMessageNr, Buffer, Length, MyDataPtr
//  Returns:    TRUE if OKee
//
static bool cmd_StoreMail(int iNr, char *pcMail, int iSize, void *pvData)
{
   PRINTF3("cmd_StoreMail(%d):Size=%d : %s" CRLF, iNr, iSize, pcMail);
   return(TRUE);
}

#ifdef FEATURE_DUMP_MAILO
//
// Function:   cmd_DumpMailo
// Purpose:    Dump MAILO struct
//
// Parms:      Id
// Returns:    
//
static void cmd_DumpMailo(MAILO *pstOpt, const char *pcCaption)
{
   int      x=0;
   MAILL   *pstList=pstOpt->pstList;
   
                        LOG_printf("MAILO: %s MAILT   tType    : %d" CRLF, pcCaption, pstOpt->tType);
                        LOG_printf("MAILO: %s ATYPE   tAuth    : %d" CRLF, pcCaption, pstOpt->tAuth);
                        LOG_printf("MAILO: %s int     iSocket  : %d" CRLF, pcCaption, pstOpt->iSocket);
                        LOG_printf("MAILO: %s int     iPort    : %d" CRLF, pcCaption, pstOpt->iPort);
                        LOG_printf("MAILO: %s bool    fSsl     : %d" CRLF, pcCaption, pstOpt->fSsl);
   //
                        LOG_printf("MAILO: %s char *  pcHost   : %p" CRLF, pcCaption, pstOpt->pcHost);
   if(pstOpt->pcHost)   LOG_printf("MAILO: %s char *  pcHost   : %s" CRLF, pcCaption, pstOpt->pcHost);
                        LOG_printf("MAILO: %s char *  pcUser   : %p" CRLF, pcCaption, pstOpt->pcUser);
   if(pstOpt->pcUser)   LOG_printf("MAILO: %s char *  pcUser   : %s" CRLF, pcCaption, pstOpt->pcUser);
                        LOG_printf("MAILO: %s char *  pcPass   : %p" CRLF, pcCaption, pstOpt->pcPass);
   if(pstOpt->pcPass)   LOG_printf("MAILO: %s char *  pcPass   : %s" CRLF, pcCaption, pstOpt->pcPass);

   while(pstList)
   {
                        LOG_printf("MAILO: %s char *  List     : %2d %p" CRLF, pcCaption, x, pstList->pcData);
      if(pstList->pcData) 
                        LOG_printf("MAILO: %s char *           : %s" CRLF, pcCaption, pstList->pcData);
      x++;
      pstList = pstList->pstNext;
   }
   //
#ifdef   FEATURE_USE_SSL
                        LOG_printf("MAILO: %s SSL_CTX * pstCtx : %p" CRLF, pcCaption, pstOpt->pstCtx);
                        LOG_printf("MAILO: %s SSL     * pstSsl : %p" CRLF, pcCaption, pstOpt->pstSsl);
#endif   //FEATURE_USE_SSL

   sleep(1);
}
#endif   //FEATURE_DUMP_MAILO
